
<?php get_header(); ?>	
	

	<?php
	// Get values for employee
	$name = get_the_title();
	$title = get_field('job_title');
	$phone = get_field('phone_number');
	$email = get_field('email');
	$social_array = get_field('social_media_links');
	$bio = get_field('bio');
	$post_image = get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'b-employee__image', 'alt' => $name." Photograph" ) );
	
	?>
	
		<section class="b-empoyee-header">
			<div class="wrap b-section__wrap-outer b-section__wrap-outer_empoyee-header">
				<div class="b-section__wrap-inner b-section__wrap-inner_empoyee-header">
					<a href="../" class="b-empoyee-header__backlink">Back to MEDI Coaches</a>
					<div class="empoyee-header__profile b-columns">
						<div class="b-resp-image b-employee__photo">
							<?php echo $post_image; ?>
						</div>
						<div class="b-employee-details">
							<h1 class="b-employee-details__name"><?php echo $name; ?></h1>
							<?php if($title): ?><p class="b-employee-details__title"><?php echo $title; ?></p><?php endif; ?>
							<?php if($phone): ?><p class="b-employee-details__phone"><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p><?php endif; ?>
							<?php if($email): ?><p class="b-employee-details__email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p><?php endif; ?>
							<?php if($social_array): ?>
								<ul class="b-employee-social-sites nav">
								<?php while( have_rows('social_media_links') ): the_row(); ?>
								
								<?php
								$site_name = get_sub_field('site_name');
								$site_link = get_sub_field('site_link');
								?>
								<li class="b-employee-social-site">
									<a class="b-employee-social-site__link" href="<?php echo $site_link; ?>" title="<?php echo $site_name; ?>"><?php echo $site_name; ?></a>
								</li>
							<?php endwhile; ?>
							</ul>
							<?php endif; ?>
						</div>
						
					</div>
					
				</div>
			</div>
		</section>
		<?php //END EMPLOYEE HEADER ?>

		<section class="b-empoyee-body">
			<div class="wrap b-section__wrap-outer b-section__wrap-outer_empoyee-body">
				<div class="b-section__wrap-inner b-section__wrap-inner_empoyee-body">
					<div class="b-wysiwyg b-wysiwyg_employee-bio">
						<?php echo $bio; ?>
					</div>
				</div>
			</div>
		</section>

	


<?php get_footer(); ?>



