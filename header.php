<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>
		
		<?php // drop Google Analytics Here ?>
			<?php if( get_field("google_tag_manager_ID", "options") ): ?>
			<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','<?php the_field("google_tag_manager_ID", "options"); ?>');</script>
			<!-- End Google Tag Manager -->
			<?php endif; ?>
		<?php // end analytics ?>
		
		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico?v=1">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
		<?php if( get_field("google_tag_manager_ID", "options") ): ?>
			<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php the_field("google_tag_manager_ID", "options"); ?>"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->
		<?php endif; ?>
		
		<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
		<?php
		/* IF HOME PAGE */
		if(is_page('homepage')) {
			get_template_part('library/custom-loops/section_page-cover-intro');
		}
		?>
		<?php //look for Full Panel Scrolling Design Option
			$full_panel_class = "";
			$full_panel_option = get_field('full_panel_scrolling');
			if($full_panel_option) {
				$full_panel_class = "full_panel_scrolling";
			}
		?>
		<div id="container" class="<?php echo $full_panel_class; ?>">
		
			<header class="b-header js-fixed-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">

				<div id="inner-header" class="b-header__inner wrap b-section__wrap-outer">
					
					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<?php if( get_field('header_logo', 'option') ) : ?>
						<p id="logo" class="b-header__logo h1"><a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php the_field('header_logo', 'option'); ?>" data-pin-nopin="true" /></a></p>
					<?php else : ?>
						<p id="logo" class="b-header__logo h1"><a href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></p>
					<?php endif; ?>
					<?php // if you'd like to use the site description you can un-comment it below ?>
					<?php // bloginfo('description'); ?>

					<?php if ( has_nav_menu( 'main-nav' ) ) { ?>
						<?php
						$menu_text = "";
						if(get_field('menu_text', 'option') ):
							$menu_text = get_field('menu_text', 'option');
						endif;
						?>
						<div class="b-mobile-nav-switch last-col js-reveal-nav"><a class="b-mobile-nav-switch__link" href="#"><?php echo $menu_text; ?></a></div>
                	<?php } ?>
                	
                	<div class="b-header__navigation js-header-nav">
	                	
                	
						<nav role="navigation" class="b-menu b-menu_main">	
							<?php wp_nav_menu(array(
	    					         'container' => false,                           // remove nav container
	    					         'container_class' => 'menu',                 // class of container (should you choose to use it)
	    					         'menu' => __( 'The Main Menu', 'mediLeadershipTheme' ),  // nav name
	    					         'menu_class' => 'b-menu__list b-menu__list_main nav top-nav',               // adding custom nav class
	    					         'theme_location' => 'main-nav',                 // where it's located in the theme
	    					         'before' => '',                                 // before the menu
	        			               'after' => '',                                  // after the menu
	        			               'link_before' => '',                            // before each link
	        			               'link_after' => '',                             // after each link
	        			               'depth' => 2,                                   // limit the depth of the nav
	    					         'fallback_cb' => '',                             // fallback function (if there is one)
	    					         
							)); ?>
						</nav>	
							
						<?php get_search_form() ?>
						<img class="b-search-button_launch" src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/icon-search.png" />
			
					</div>

				</div>
				<div class="b-submenu">
					<nav role="navigation" class="b-menu b-menu_submenu">
						<?php wp_nav_menu(array(
						'container' => false,                           // remove nav container
						'container_class' => 'menu cf',                 // class of container (should you choose to use it)
						'menu' => __( 'The SubMenu', 'bonestheme' ),  // nav name
						'menu_class' => 'b-menu__list b-menu__list_submenu nav',               // adding custom nav class
						'theme_location' => 'main-nav',                 // where it's located in the theme
						'before' => '',                                 // before the menu
	        			'after' => '',                                  // after the menu
	        			'link_before' => '',                            // before each link
	        			'link_after' => '',                             // after each link
	        			'depth' => 1,                                   // limit the depth of the nav
						'fallback_cb' => '',							// fallback function (if there is one)
						'sub_menu' => true                          
						)); ?>
					</nav>
					<nav role="navigation" class="b-menu b-menu_submenu">
						<?php wp_nav_menu(array(
						'container' => false,                           // remove nav container
						'container_class' => 'menu cf',                 // class of container (should you choose to use it)
						'menu' => __( 'The SubMenu', 'bonestheme' ),  // nav name
						'menu_class' => 'b-menu__list b-menu__list_submenu nav',               // adding custom nav class
						'theme_location' => 'utility-nav',                 // where it's located in the theme
						'before' => '',                                 // before the menu
	        			'after' => '',                                  // after the menu
	        			'link_before' => '',                            // before each link
	        			'link_after' => '',                             // after each link
	        			'depth' => 1,                                   // limit the depth of the nav
						'fallback_cb' => '',							// fallback function (if there is one)
						'sub_menu' => true                          
						)); ?>
					</nav>
				</div>
			</header>
			<a id="top" class="b-anchor-tag"></a>
			<span  class="b-header-buffer js-header-buffer"></span>