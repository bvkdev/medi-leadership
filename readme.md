# Regional Office - A Wordpress Parent Theme

Designed by BVK (code: Benjamin Gray)

## Theme Dependencies:

* This theme relies on Advance Custom Fields plugin, minimum version 5.

## Installation Instructions:

* In the theme folder is an export of the Custom Fields JSON. You must import this file to recreate all the custom fields for creating content with this theme.


## Must Do:

* If you are going to edit ANY custom fields, you must first import the JSON file from this repo and, when finished locally, export ALL custom fields to a file with the same name in order to keep this up to date.


### credits
Based on Bones, designed by Eddie Machado
http://themble.com/bones
License: WTFPL
License URI: http://sam.zoy.org/wtfpl/