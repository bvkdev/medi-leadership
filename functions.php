<?php
if ( !session_id() )
{
	session_start();
}
include __DIR__ . '/library/php/helpers.php';

if( !defined('WP_ENV') ) {
	define( 'WP_ENV', 'production' );
}
$bb_minset = "";
if('WP_ENV' == 'production'){
	$bb_minset = ".min";
} elseif ('WP_ENV' == 'staging'){
	$bb_minset = ".min";
} elseif ('WP_ENV' == 'local'){
	$bb_minset = ".min";
}

/*
 * GET THE SHARED FUNCTIONS THAT BLOCKS USE FOR COMMON ACF REQUESTS
 * 
*/
require_once('library/common-functions.php');

/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

//add PHP Mobile Detection
require_once( 'library/mobile-detect.php' );
/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style();

  // let's get language support going, if you need it
  load_theme_textdomain( 'mediLeadershipTheme', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  require_once( 'library/employee-type.php' );
  

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 300, true );
add_image_size( 'bones-thumb-600s', 600, 600, true );
add_image_size( 'bones-thumb-300', 300, 150, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*-------------------------------------------
 POST THUMBNAIL FILTERS
--------------------------------------------*/
add_filter( 'post_thumbnail_html', 'remove_width_height', 10, 5 );
add_filter( 'image_send_to_editor', 'remove_width_height', 10, 5 );
 
function remove_width_height( $html, $post_id, $post_thumbnail_id, $size, $attr ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	//
	$html = preg_replace( '/(sizes)=\".*\"/', "", $html );
	
    return $html;
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* THEME CUSTOMIZE *********************/

/* 
  A good tutorial for creating your own Sections, Controls and Settings:
  http://code.tutsplus.com/series/a-guide-to-the-wordpress-theme-customizer--wp-33722
  
  Good articles on modifying the default options:
  http://natko.com/changing-default-wordpress-theme-customization-api-sections/
  http://code.tutsplus.com/tutorials/digging-into-the-theme-customizer-components--wp-27162
  
  To do:
  - Create a js for the postmessage transport method
  - Create some sanitize functions to sanitize inputs
  - Create some boilerplate Sections, Controls and Settings
*/

function bones_theme_customizer($wp_customize) {
  // $wp_customize calls go here.
  //
  // Uncomment the below lines to remove the default customize sections 

  // $wp_customize->remove_section('title_tagline');
  // $wp_customize->remove_section('colors');
  // $wp_customize->remove_section('background_image');
  // $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');
  
  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  // $wp_customize->get_section('background_image')->title = __( 'Images' );
}

add_action( 'customize_register', 'bones_theme_customizer' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'mediLeadershipTheme' ),
		'description' => __( 'The first (primary) sidebar.', 'mediLeadershipTheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'footer_widgets_1',
		'name' => __( 'Footer Widgets Column 1', 'ccbtheme' ),
		'description' => __( 'This is the first of the two widget bars across the bottom.', 'mediLeadershipTheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="b-widget__title">',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'footer_widgets_2',
		'name' => __( 'Footer Widgets Column 2', 'ccbtheme' ),
		'description' => __( 'This is the second of the two widget bars across the bottom.', 'mediLeadershipTheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="b-widget__title">',
		'after_title' => '</h3>',
	));
	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'mediLeadershipTheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'mediLeadershipTheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'mediLeadershipTheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'mediLeadershipTheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'mediLeadershipTheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'mediLeadershipTheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!


/*
This is a modification of a function found in the
twentythirteen theme where we can declare some
external fonts. If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/
function regional_office_fonts() {
  wp_enqueue_style('googleFonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Playfair+Display:400i,700');
}
add_action('wp_enqueue_scripts', 'regional_office_fonts');

/*
The following are additions made to the Bones Skeletal theme by Benjamin Gray.
*/

/********************************************************/
// Adding Dashicons in WordPress Front-end
/********************************************************/
//add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}

/********************************************************************
 * MAKE USE OF WORDPRESS OPTIONAL ITEMS AND REGISTER THINGS
********************************************************************/

// Registering additional wp3+ menus
register_nav_menus(
	array(
		'utility-nav' => __( 'The Utility Menu', 'mediLeadershipTheme' ),	// utility nav in header
		'header-highlight-nav' => __( 'Header Highlight Menu', 'mediLeadershipTheme' ),	// utility nav in header
		'sitemap-menu' => __( 'Sitemap', 'mediLeadershipTheme')			// using the built-in menu structure for creating a sitemap
	)
);

// Add a theme options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

/********************************************************************
 * ALTER WORDPRESS DEFAULTS
********************************************************************/
function custom_mime_types( $post_mime_types ) {
        //$post_mime_types['application/msword'] = array( __( 'DOCs' ), __( 'Manage DOCs' ), _n_noop( 'DOC <span class="count">(%s)</span>', 'DOC <span class="count">(%s)</span>' ) );
        //$post_mime_types['application/vnd.ms-excel'] = array( __( 'XLSs' ), __( 'Manage XLSs' ), _n_noop( 'XLS <span class="count">(%s)</span>', 'XLSs <span class="count">(%s)</span>' ) );
        $post_mime_types['application/pdf'] = array( __( 'PDFs' ), __( 'Manage PDFs' ), _n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' ) );
        $post_mime_types['application/zip'] = array( __( 'ZIPs' ), __( 'Manage ZIPs' ), _n_noop( 'ZIP <span class="count">(%s)</span>', 'ZIPs <span class="count">(%s)</span>' ) );
		
        return $post_mime_types;
}
add_filter( 'post_mime_types', 'custom_mime_types' );


//add excerpts to pages
add_post_type_support( 'page', 'excerpt' );

//add ability to shorten excerpts
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
//add ability to shorten anything
function shorten_content($content, $limit) {
	$text = $content;
  $excerpt = explode(' ', $text, $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

// Remove Open Sans that WP adds from frontend
if (!function_exists('remove_wp_open_sans')) :
  function remove_wp_open_sans() {
    //wp_deregister_style( 'open-sans' );
    
  }
  //add_action('wp_enqueue_scripts', 'remove_wp_open_sans');
  // Uncomment below to remove from admin
  // add_action('admin_enqueue_scripts', 'remove_wp_open_sans');
endif;


// ADD A BODY TAG HOOK
function after_body(){
	do_action('after_body');
}

// Alter the default "more" for excerpts
function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

// Alter the default youTube embed code
// adds rel=0 to remove ads from the end screen

add_filter('embed_oembed_html','oembed_result', 10, 3);
function oembed_result($html, $url, $args) {
    // $args includes custom argument
    // modify $html as you need
    /* Modify video parameters. */
	if ( strstr( $html,'youtube.com/embed/' ) ) {
		$html = str_replace( '?feature=oembed', '?feature=oembed&rel=0', $html );
	}
	
    return $html;
}

// Add a div around oEmbeds to allow for responsive embedding.
add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;
function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="embed-container">'.$html.'</div>';
    return $return;
}

// Create a Walker to echo a submenu.
//// The header file outputs the menu regularly, with the 'sub_menu' argument as true.
//// This filter runs through all the menu items and only displays the children of the currently active parent menu item
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );
// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
	if ( isset( $args->sub_menu ) ) {
		$root_id = 0;
		// find the current menu item
		foreach ( $sorted_menu_items as $menu_item ) {
			if ( $menu_item->current ) {
				// set the root id based on whether the current menu item has a parent or not
				$root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
				break;
			}
		}
	
		// find the top level parent
		if ( ! isset( $args->direct_parent ) ) {
			$prev_root_id = $root_id;
			while ( $prev_root_id != 0 ) {
				foreach ( $sorted_menu_items as $menu_item ) {
					if ( $menu_item->ID == $prev_root_id ) {
						$prev_root_id = $menu_item->menu_item_parent;
						// don't set the root_id to 0 if we've reached the top of the menu
						if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
						break;
					}
				}
			}
		}
	
		$menu_item_parents = array();
		//echo sizeof($sorted_menu_items);
		//echo $root_id."  --  ";
		foreach ( $sorted_menu_items as $key => $item ) {
			// init menu_item_parents
			//echo $item->menu_item_parent." ";
			if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;
			
				if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
					// part of sub-tree: keep!
					$menu_item_parents[] = $item->ID;
				} else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
					// not part of sub-tree: away with it!
					unset( $sorted_menu_items[$key] );
				}
			if( $item->menu_item_parent != $root_id ) :
				unset( $sorted_menu_items[$key]);
			endif;
		}
		//echo sizeof($sorted_menu_items);
		//  add class for sizing
		foreach ( $sorted_menu_items as $key => $item ) {
			//$menu_total = sizeof($sorted_menu_items);
			//$item->classes[] = 'p-1of2 m-1of2 t-1of'.$menu_total.' d-1of'.$menu_total;
			//$item->classes[] = 'js_grid_item';
		}
		return $sorted_menu_items;
	} else {
		return $sorted_menu_items;
	}
}

// CREATE WALKER TO ADD CUSTOM CLASSES TO MENU ITEMS
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_any_menu', 10, 2 );
// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_any_menu( $sorted_menu_items, $args ) {
 
    // Add "js_grid_item" class to the menu items
    foreach ( $sorted_menu_items as $key => $item ) {
		$item->classes[] = 'b-menu__list-item';
	}
 
    // Return the menu objects
    return $sorted_menu_items;
}

// CREATE WALKER TO ADD CUSTOM CLASSES TO THE FOOTER MENU ITEMS
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_footer_menu', 10, 2 );
// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_footer_menu( $sorted_menu_items, $args ) {
	// Only apply the classes to the footer navigation menu
    if ( isset( $args->theme_location ) )
        if ( 'footer-links' !== $args->theme_location )
            return $sorted_menu_items;
 
    // Add "js_grid_item" class to the menu items
    foreach ( $sorted_menu_items as $key => $item ) {
		$item->classes[] = 'js_grid_item';
	}
 
    // Return the menu objects
    return $sorted_menu_items;
}

/*-------------------------------------------
 OFFSET THE POSTS CALCULATIONS BECAUSE THE
 BLOG PAGE DISPLAYS THE FIRST FOUR IN A SPECIAL LAYOUT
--------------------------------------------*/

// offset the main query on the home page 
function xtutsplus_offset_main_query ( $query ) {
    if ( !is_front_page() && is_home() ) {
    	if ( $query->is_home() && $query->is_main_query() && !$query->is_paged() ) {
        	$query->set( 'offset', '4' );
    	}
    }
 }
//add_action( 'pre_get_posts', 'tutsplus_offset_main_query' );
function tutsplus_offset_main_query ( $query ) {
	if ( $query->is_home() && $query->is_main_query() ) {
		$query->set('post__not_in', get_option('sticky_posts'));
	}
	if ( $query->is_home() && $query->is_main_query() && !$query->is_paged() ) {
        //$query->set( 'offset', '1' );
	}
 }
add_action( 'pre_get_posts', 'tutsplus_offset_main_query' );

/*-------------------------------------------
 CHECK FOR THEME OPTIONS TO SHUT OFF THE COMMENTS COMPLETELY.
 IF TRUE, FILTER THE COMMENTS_TEMPLATE() FUNCTION TO RETURN NOTHING.
--------------------------------------------*/
add_filter( "comments_template", "hide_all_commenting" );
function hide_all_commenting( $comment_template ) {
	$comments_status = get_field('turn_off_comments', 'options');
     global $post;
     //if ( !( is_singular() && ( have_comments() || 'open' == $post->comment_status ) ) ) {
     if($comments_status) {
     	$template = tempnam( trailingslashit( untrailingslashit( sys_get_temp_dir() ) ), 'comments.php' );
		
        return $template;
	 }
     //}

}

/********************************************************************
 * ADD SCRIPTS AND STYLES
********************************************************************/

// Add Masonry Plugin
if (! function_exists('slug_scripts_masonry') ) :
	if ( ! is_admin() ) :
		function slug_scripts_masonry() {
		    wp_enqueue_script('masonry');
		    //wp_enqueue_style('masonry’, get_template_directory_uri().'/css/’);
		}
		//add_action( 'wp_enqueue_scripts', 'slug_scripts_masonry' );
	endif; //! is_admin()
endif; //! slug_scripts_masonry exists 



// Add jQuery Pluginss
add_action( 'wp_enqueue_scripts', 'add_jquery_plugins' );
function add_jquery_plugins() {
//BXSLIDER
wp_register_script('bxslider', get_template_directory_uri().'/library/js/libs/bxslider/jquery.bxslider.min.js', array('jquery'), '4', true);
wp_enqueue_script('bxslider');
wp_register_script('bxslider_easing', get_template_directory_uri().'/library/js/libs/bxslider/plugins/jquery.easing.1.3.min.js', array('jquery'), '1.3', true);
wp_enqueue_script('bxslider_easing');
//OBJECT FIT VIDEOS POLYFILL
wp_register_script('objectFitVideos', get_template_directory_uri().'/library/js/libs/object-fit-videos.min.js', array('jquery'), '1.0.2', true);
wp_enqueue_script('objectFitVideos');
//CHARTS
//wp_register_script('charts', get_template_directory_uri().'/library/js/libs/chart.min.js', array('jquery'), '1.0.1', true);
//wp_enqueue_script('charts');
//FLIPCLOCK COUNTDOWN
//wp_register_script('flipclock', get_template_directory_uri().'/library/js/libs/flipclock/flipclock.min.js', array('jquery'), '0.7.4', true);
//wp_enqueue_script('flipclock');
//get GOOGLE MAPS API KEY from options settings
$apiKey = get_field('google_maps_api_key', 'options');
if($apiKey) {
	wp_register_script('google_maps_api', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key='.$apiKey, array('jquery'), '3', true);
	wp_enqueue_script('google_maps_api');
	wp_register_script('google_maps_js', get_template_directory_uri().'/library/js/libs/jquery.acf.googleMaps.js', array('jquery'), '1.0.1', true);
	wp_enqueue_script('google_maps_js');
	wp_register_script('google_maps_style', get_template_directory_uri().'/library/js/libs/jquery.acf.googleMaps.style.min.js', array('jquery'), '1', true);
	wp_enqueue_script('google_maps_style');
}
//iFRAME RESIZER (REQUIRES SECOND SCRIPT TO BE ON THE IFRAMED CONTENT SOURCE)
wp_register_script('iframe_resizer', get_template_directory_uri().'/library/js/libs/iframeResizer.min.js', array('jquery'), '3.5.14', true);
wp_enqueue_script('iframe_resizer');
//MAP ESCAPE
//wp_register_script('map_escape', get_template_directory_uri().'/library/js/libs/mapescape.min.js', array('jquery'), '1', true);
//wp_enqueue_script('map_escape');
//MATCH HEIGHTS
wp_register_script('match_heights', get_template_directory_uri().'/library/js/libs/jquery.matchHeight-min.js', array('jquery'), '0.7.0', true);
wp_enqueue_script('match_heights');
//NWRAPPER
wp_register_script('nwrapper', get_template_directory_uri().'/library/js/libs/jquery.nwrapper.min.js', array('jquery'), '1', true);
wp_enqueue_script('nwrapper');
//RESPONSIVE IMAGE MAPS
//wp_register_script('rwd_image_maps', get_template_directory_uri().'/library/js/libs/imageMapResizer.min.js', array('jquery'), '1.5', true);
//wp_enqueue_script('rwd_image_maps');
//MOBILE CHECKING
wp_register_script('ismobile', get_template_directory_uri().'/library/js/libs/isMobile.min.js', '0.4.1', true);
wp_enqueue_script('ismobile');
//add_facebook_sdk script and localize it to insert the App ID
$appID = get_field('facebook_sdk_id', 'options');
if($appID):
	wp_register_script('add_facebook_sdk', get_template_directory_uri().'/library/js/libs/add_facebook_sdk.js', array('jquery'), '1', true);
	wp_enqueue_script('add_facebook_sdk');
	$facebookVariables = array( 'appID' => $appID );
	wp_localize_script( 'add_facebook_sdk', 'fb_variables', $facebookVariables );
endif;
}
//FULL PAGE SECTION SCROLLING: FULLPAGE.JS
//wp_register_script('jqueryui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js', array('jquery'), '1.9.1', false);
//wp_enqueue_script('jqueryui');
wp_register_script('fullpageExtensions', get_template_directory_uri().'/library/js/libs/fullpage/jquery.fullpage.extensions.min.js', array('jquery'), '2.9.3', true);
//wp_enqueue_script('fullpageExtensions');
wp_register_script('fullpageOverflow', get_template_directory_uri().'/library/js/libs/fullpage/scrolloverflow.min.js', array('jquery'), '0.0.2', false);
wp_enqueue_script('fullpageOverflow');
wp_register_script('fullpage', get_template_directory_uri().'/library/js/libs/fullpage/jquery.fullpage.js', array('jquery', 'fullpageOverflow'), '2.7.4', true);
wp_enqueue_script('fullpage');
//FULL PAGE SECTION SCROLLING: SMART SCROLL
//wp_register_script('fullpage', get_template_directory_uri().'/library/js/libs/smartscroll/smartscroll.js', array('jquery'), '1', true);
//wp_enqueue_script('fullpage');
//FULL PAGE SECTION SCROLLING: ALTON SCROLL
//wp_register_script('altonscroll', get_template_directory_uri().'/library/js/libs/altonscroll/jquery.alton.min.js', array('jquery'), '1.2.0', true);
//wp_enqueue_script('altonscroll');

//Enqueue CSS files for specific jQuery plugins
add_action( 'wp_enqueue_scripts', 'add_jquery_styles' );
function add_jquery_styles() {
wp_enqueue_style('bxslider_css', get_template_directory_uri().'/library/js/libs/bxslider/jquery.bxslider.min.css');
wp_enqueue_style('fullpage_css', get_template_directory_uri().'/library/js/libs/fullpage/jquery.fullpage.css');
//wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' );
//wp_enqueue_style('smartscroll_css', get_template_directory_uri().'/library/js/libs/smartscroll/smartscroll.css');
//wp_enqueue_style('flipclock_css', get_template_directory_uri().'/library/js/libs/flipclock/flipclock.min.css');
}


//Enqueue an emergency CSS file that lives outside of the SCSS library
add_action( 'wp_enqueue_scripts', 'add_non_scss_styles' );
function add_non_scss_styles() {
wp_enqueue_style('non_scss_styles', get_template_directory_uri().'/library/css/non_scss.css');
}
/*---------------------------------------------------
	SOME HELPFUL FUNCTIONS
------------------------------------------------*/
//Register the Google API Key for the back end
function my_acf_init() {
	//get GOOGLE MAPS API KEY from options settings
	$apiKey = get_field('google_maps_api_key', 'options');
	acf_update_setting('google_api_key', $apiKey);
}
add_action('acf/init', 'my_acf_init');

//GET THE CATEGORY SLUG FROM THE ID
function get_cat_slug($cat_id) {
	$cat_id = (int) $cat_id;
	$category = get_category($cat_id);
	return $category->slug;
}

//MOVE WORDPRESS'S JQUERY TO FOOTER
add_action( 'wp_default_scripts', 'move_jquery_into_footer' );
function move_jquery_into_footer( $wp_scripts ) {

    if( is_admin() ) {
        return;
    }

    $wp_scripts->add_data( 'jquery', 'group', 1 );
    $wp_scripts->add_data( 'jquery-core', 'group', 1 );
    $wp_scripts->add_data( 'jquery-migrate', 'group', 1 );
}

/* DON'T DELETE THIS CLOSING TAG */ ?>
