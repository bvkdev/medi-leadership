<?php
/*
 Template Name: Pillar/Focus
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
	<?php /* THIS IS WHERE CUSTOM FIELD CONTENT WILL GO */ ?>
	<?php get_template_part('library/custom-loops/loop-flexible-content'); ?>
	
	<?php /* THIS IS WHERE THE PHOTO GRID WILL GO */ ?>
	<?php get_template_part('library/custom-loops/section_employee_grid'); ?>
	
	<?php /* THIS IS WHERE THE BIZ OPS LINK WILL GO */ ?>
	
	
	
	


<?php get_footer(); ?>



