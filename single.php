<?php get_header(); ?>

			<div id="content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php
				$show_sidebar ="";
				$show_sidebar = get_field('include_sidebar');
				if($show_sidebar):
					$col_grid = "m-all t-2of3 d-4of7";
				else:
					$col_grid = "m-all p-all t-all d-all";
				endif;
				?>
				<?php
				$post_image = get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'b-featured-post__thumbnail', 'alt' => get_the_title() ) );
				$image_url = get_the_post_thumbnail_url(get_the_ID(), 'full' );
				$background_style = "style='background: url(".$image_url.") center center no-repeat; background-size: cover;'";
				
				?>
				<?php if($post_image) : ?>
				<div class="b-single-post-header" <?php echo $background_style; ?>>
					<?php //echo $post_image; ?>
				</div>
				<?php endif; ?>
				<div id="inner-content" class="wrap b-section__wrap-outer cf b-columns">
					
						<!-- AddThis Button BEGIN -->
						<div class="b-share-buttons b-share-buttons_top p-all m-all t-1of8 d-1of8">
							<p class="b-share-button b-share-button_label">Share</p>
							<a class="b-share-button addthis_button_linkedin at300b" target="_blank" title="LinkedIn" href=""><i class="fa  fa-linkedin linkedin"></i></a>
							<a class="b-share-button addthis_button_facebook at300b" title="Facebook" href=""><i class="fa fa-facebook facebook"></i></a>
							<a class="b-share-button addthis_button_twitter at300b" title="Twitter" href=""><i class="fa fa-twitter twitter"></i></a>
							<a class="b-share-button addthis_button_email at300b" title="Email" href=""><i class="fa fa-envelope envelope"></i></a>
							<a class="b-share-button addthis_button_google_plusone_share at300b" target="_blank" title="Google+" href=""><i class="fa fa-google-plus google-plus"></i></a>
						</div>
						<!--<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>-->
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51de2d2b7e09a8f5"></script>
						<!-- AddThis Button END -->

						<main id="main" class="b-single-post p-all m-all t-6of7 d-6of7" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<?php
								/*
								 * Ah, post formats. Nature's greatest mystery (aside from the sloth).
								 *
								 * So this function will bring in the needed template file depending on what the post
								 * format is. The different post formats are located in the post-formats folder.
								 *
								 *
								 * REMEMBER TO ALWAYS HAVE A DEFAULT ONE NAMED "format.php" FOR POSTS THAT AREN'T
								 * A SPECIFIC POST FORMAT.
								 *
								 * If you want to remove post formats, just delete the post-formats folder and
								 * replace the function below with the contents of the "format.php" file.
								*/
								get_template_part( 'post-formats/format', get_post_format() );
							?>
							<?php
							// CHECK FOR AND DISPLAY THE AUTHOR BIO FOOTER
							$author = get_field('post_author');
							if($author) :								
							?>
								<?php foreach( $author as $post): // variable must be called $post (IMPORTANT) ?>
	        						<?php setup_postdata($post); ?>
	        						<?php
	        						//get post values
									$author_photo = get_the_post_thumbnail_url(get_the_ID(), 'full' );
									$author_name = get_the_title();
									$author_title = get_field('job_title');
									$author_link = get_the_permalink();
									$author_slug = basename(get_permalink());
									$author_posts_link = "";
									$term = term_exists($author_slug, 'post_tag');
									if ($term !== 0 && $term !== null) {
										$author_posts_link = get_term_link($author_slug, 'post_tag');
									}
									
									
									?>
									<div class="b-author-box">
										<div class="b-author-box__wrap b-columns">
											<div class="b-resp-image b-author-box__photo">
												<img src="<?php echo $author_photo; ?>" class="b-author-box__image" alt="Photo of <?php echo $author_name; ?>" />
											</div>
											<div class="b-author-box__content">
												<p class="b-author-box__meta">written by:</p>
												<h4 class="b-author-box__name"><a class="b-author-box__link" title="<?php echo $author_name; ?>" href="<?php echo $author_link; ?>"><?php echo $author_name; ?></a></h4>
												<?php if($author_title) : ?><p class="b-author-box__title"><?php echo $author_title; ?></p><?php endif; ?>
												<?php if($author_posts_link) : ?><a class="b-author-box__link" title="More posts by <?php echo $author_name; ?>" href="<?php echo $author_posts_link; ?>">More Posts by <?php echo $author_name; ?></a><?php endif; ?>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
								<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							<?php endif; ?>
							
							
						<?php endwhile; ?>
						</main>

						<?php if($show_sidebar): ?>
							<?php get_sidebar(); ?>
						<?php endif; ?>

				</div>
				<?php endif; ?>
			</div>

<?php get_footer(); ?>
