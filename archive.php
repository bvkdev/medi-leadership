<?php get_header(); ?>
<?php 
//Create exclusion list of IDs
$exclude = 	get_terms( array(
			'slug' => Array( '' ), 
			'taxonomy' => "category",
			'fields'   => 'ids',
			) );
// Create list of all available categories
$post_terms = 	get_terms( array(
					'taxonomy' => 'category',
					'hide_empty' => true,
					'exclude' => $exclude
				) );
?>
			<div id="content" class="b-posts-page">
				<?php // SORTING BAR ?>
					<div id="post-sort-bar" class="b-sortbar">
						<div class="wrap b-section__wrap-outer cf">
							
						<?php
							$blogURL = get_permalink( get_option( 'page_for_posts' ));
							if ( $post_terms && ! is_wp_error( $post_terms ) ) : 
								$post_categories = array();
								echo "<ul class='nav b-post-sort-options'>";
								echo "<label class='b-sortbar_label'>Jump To:</label>";
								echo "<li class='menu-item b-post-sort-option b-post-sort-option_all'><a href='".$blogURL."' >All</a></li>";
								foreach ( $post_terms as $term ) :
									$post_categories[] = $term->name;
									$term_url = get_term_link($term->slug, $term->taxonomy);
									//var_dump($term_url);
									echo "<li class='menu-item b-post-sort-option' data-category='".$term->slug."'><a href='".$term_url."' >".$term->name."</a></li>";
								endforeach;
								echo "</ul>";
								$post_categories_list = join( ", ", $post_categories );
							endif;
						?>
							
						</div>
					</div>
					<?php // END SORTING BAR ?>
				<div id="inner-content" class="wrap b-section__wrap-outer cf">
							<?php if (is_category()) { ?>
								<h1 class="b-headline b-headline_underscore archive-title h2">
									<span><?php _e( '', 'mediLeadershipTheme' ); ?></span> <?php single_cat_title(); ?>
								</h1>

							<?php } elseif (is_tag()) { ?>
								<h1 class="b-headline b-headline_underscore archive-title h2">
									<span><?php _e( '', 'mediLeadershipTheme' ); ?></span> <?php single_tag_title(); ?>
								</h1>

							<?php } elseif (is_author()) {
								global $post;
								if($post) {
								$author_id = $post->post_author;
								} else {
									$author_id= "";
								}
							?>
								<h1 class="b-headline b-headline_underscore archive-title h2">

									<span><?php _e( 'Posts By:', 'mediLeadershipTheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>

								</h1>
							<?php } elseif (is_day()) { ?>
								<h1 class="b-headline b-headline_underscore archive-title h2">
									<span><?php _e( 'Daily Archives:', 'mediLeadershipTheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
								</h1>

							<?php } elseif (is_month()) { ?>
									<h1 class="b-headline b-headline_underscore archive-title h2">
										<span><?php _e( 'Monthly Archives:', 'mediLeadershipTheme' ); ?></span> <?php the_time('F Y'); ?>
									</h1>

							<?php } elseif (is_year()) { ?>
									<h1 class="b-headline b-headline_underscore archive-title h2">
										<span><?php _e( 'Yearly Archives:', 'mediLeadershipTheme' ); ?></span> <?php the_time('Y'); ?>
									</h1>
							<?php } ?>
							
							<?php $main_col_grid = "m-all t-all d-all" ?>
							<main id="main" class="b-posts" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							

							

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<?php
							$post_date = get_the_date('m-j-y');
							$post_title = get_the_title();
							$post_excerpt = excerpt(30);
							$post_id = get_the_ID();
							$post_url = get_permalink( $post_id );
							$post_image = get_the_post_thumbnail( $post_id, "medium", array( 'class' => 'b-featured-post__thumbnail', 'alt' => $post_title ) );
							$read_more_label = "Read More";
							?>
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'b-regular-post b-columns js-sortable-object' ); ?> role="article">
									<?php if($post_image) : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="b-regular-post__image-link p-all m-all t-1of3 d-1of4">
										
										<div class="b-resp-image">
											<?php echo $post_image; ?>
										</div>
										
										<div class="b-regular-post__image_hover">
											<p>Read Story</p>
										</div>
									</a>
									<?php endif; ?>
									<div class="b-regular-post__content p-all m-all t-2of3 d-3of4">
										<p class="b-regular-post__meta"><?php echo $post_date; ?></p>
										<h2 class="h2 entry-title b-regular-post__title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
										<?php if($post_excerpt): ?> <p class="b-wysiwyg b-regular-post__text"><?php echo $post_excerpt; ?></p><?php endif; ?>
										<a class="b-regular-post__button" href="<?php echo $post_url; ?>"><?php echo $read_more_label; ?></a>
									</div>
								</article>

							<?php endwhile; ?>

									

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'mediLeadershipTheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'mediLeadershipTheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( '', 'mediLeadershipTheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>


						</main>

				</div>

			</div>

<?php get_footer(); ?>
