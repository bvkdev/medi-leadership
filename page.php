<?php get_header(); ?>
		
	<?php get_template_part('library/custom-loops/section_page-header'); ?>
	
	<?php /* THIS IS WHERE CONDITIONAL STATEMENTS FOR SPECIFIC PAGES' CONTENT WILL GO */ ?>
	
	<?php
	 
	/* DONATION PAGE */
	if(is_page('donate')) {
		get_template_part('library/custom-loops/section_donate-form');
	}
	/* WHAT WE DO PAGE */
	if(is_page('what-we-do')) {
		get_template_part('library/custom-loops/section_community-goals');
		get_template_part('library/custom-loops/section_money-people');
	}
	
	/* VOLUNTEER PAGE */
	if(is_page('volunteer')) {
		//get_template_part('library/custom-loops/section_event-feed');
	}
	
	/* EVENTS PAGE */
	if(is_page('events')) {
		get_template_part('library/custom-loops/section_event-feed');
	}
	
	?>
	<?php get_template_part('library/custom-loops/loop-flexible-content'); ?>
	
	<?php /* PROBABLY DELETE EVERYTHING AFTER THIS */ ?>
	
	<?php
		global $post;
		$content = $post->post_content;
		
		if($content != "") {
		wp_reset_postdata( $post );
	?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div id="content">

				<div id="inner-content" class="wrap b-section__wrap-outer cf">
					
						<?php
						$show_sidebar ="";
						$show_sidebar = get_field('include_sidebar');
						if($show_sidebar):
							$col_grid = "";
						else:
							$col_grid = "";
						endif;
						?>
						<main id="main" class="cf <?php echo $col_grid; ?>" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<?php /*<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<?php
									$show_meta = "";
									$show_meta = get_field('show_meta_byline');
									if($show_meta):
									?>
									<p class="byline vcard">
										<?php printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> by <span class="author">%3$s</span>', 'mediLeadershipTheme' ), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), get_the_author_link( get_the_author_meta( 'ID' ) )); ?>
									</p>
									<?php
									endif;
									?>

								</header> <?php // end article header ?>
								 * 
								*/?>

								<section class="entry-content cf" itemprop="articleBody">
									<?php
										// the content (pretty self explanatory huh)
										the_content();

										/*
										 * Link Pages is used in case you have posts that are set to break into
										 * multiple pages. You can remove this if you don't plan on doing that.
										 *
										 * Also, breaking content up into multiple pages is a horrible experience,
										 * so don't do it. While there are SOME edge cases where this is useful, it's
										 * mostly used for people to get more ad views. It's up to you but if you want
										 * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
										 *
										 * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
										 *
										*/
										wp_link_pages( array(
											'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'mediLeadershipTheme' ) . '</span>',
											'after'       => '</div>',
											'link_before' => '<span>',
											'link_after'  => '</span>',
										) );
									?>
								</section> <?php // end article section ?>

								<footer class="article-footer cf">

								</footer>

								<?php comments_template(); ?>

							</article>
							</main>
							<?php
							if($show_sidebar):
							?>
								<?php //get_sidebar(); ?>
							<?php
							endif;
							?>
						</div>

					</div>
							<?php endwhile; else : ?>
							<div id="content">

							<div id="inner-content" class="wrap b-section__wrap-outer cf">
								<main id="main" class="cf <?php echo $col_grid; ?>" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'mediLeadershipTheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'mediLeadershipTheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'mediLeadershipTheme' ); ?></p>
										</footer>
									</article>
								</main>
								<?php
								if($show_sidebar):
								?>
									<?php //get_sidebar(); ?>
								<?php
								endif;
								?>
								</div>

							</div>
							<?php endif; ?>

						
						
				</div>

			</div>
	<?php
		} //end if the content check;
	?>

<?php get_footer(); ?>
