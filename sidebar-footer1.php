<?php if ( is_active_sidebar( 'footer_widgets_1' ) ) : ?>
	<div id="footer-widgets-1" class="b-footer-widget b-footer-widget_one sidebar p-all m-all t-1of2 d-1of2 cf" role="complementary">
		<?php dynamic_sidebar( 'footer_widgets_1' ); ?>
	</div>		
<?php else : ?>
	<?php
		/*
		 * This content shows up if there are no widgets defined in the backend.
		*/
	?>
<?php endif; ?>

				
