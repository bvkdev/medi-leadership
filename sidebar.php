				<div id="sidebar1" class="sidebar m-all t-1of3 d-3of7 last-col cf" role="complementary">
					<div class="b-resp-image b-resp-image_sidebar">
					<?php
					if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					the_post_thumbnail( 'full' );
					}
					?></div>
					<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

						<?php dynamic_sidebar( 'sidebar1' ); ?>

					<?php else : ?>

						<?php
							/*
							 * This content shows up if there are no widgets defined in the backend.
							*/
						?>

						<div class="no-widgets">
						</div>

					<?php endif; ?>

				</div>
