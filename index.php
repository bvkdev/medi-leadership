<?php get_header(); ?>

<?php 
//Create exclusion list of IDs
$exclude = 	get_terms( array(
			'slug' => Array( '' ), 
			'taxonomy' => "category",
			'fields'   => 'ids',
			) );
// Create list of all available categories
$post_terms = 	get_terms( array(
					'taxonomy' => 'category',
					'hide_empty' => true,
					'exclude' => $exclude
				) );
?>
	
			<div id="content" class="b-posts-page">
					<?php // SORTING BAR ?>
					<div id="post-sort-bar" class="b-sortbar">
						<div class="wrap b-section__wrap-outer cf">
							
						<?php
							$blogURL = get_permalink( get_option( 'page_for_posts' ));
							if ( $post_terms && ! is_wp_error( $post_terms ) ) : 
								$post_categories = array();
								echo "<ul class='nav b-post-sort-options'>";
								echo "<label class='b-sortbar_label'>Jump To:</label>";
								echo "<li class='menu-item b-post-sort-option b-post-sort-option_all'><a href='".$blogURL."' >All</a></li>";
								foreach ( $post_terms as $term ) :
									$post_categories[] = $term->name;
									$term_url = get_term_link($term->slug, $term->taxonomy);
									//var_dump($term_url);
									echo "<li class='menu-item b-post-sort-option' data-category='".$term->slug."'><a href='".$term_url."' >".$term->name."</a></li>";
								endforeach;
								echo "</ul>";
								$post_categories_list = join( ", ", $post_categories );
							endif;
						?>
							
						</div>
					</div>
					<?php // END SORTING BAR ?>
				<div id="inner-content" class="wrap b-section__wrap-outer cf">
					<?php if ( !is_paged() ) : //CHECKING IF WE'RE ON PAGE 1, SO WE DON'T HAVE STICKY/FEATURED POSTS' DESIGN ON OTHER PAGES ?>
					
					<?php // FIRST STICKY POST ?>
					<?php
					/* Get all sticky posts */
    				$sticky = get_option( 'sticky_posts' );
				    /* Sort the stickies with the newest ones at the top */
				    rsort( $sticky );
					//print_r($sticky);
					/* Get the 2 newest stickies (change 2 for a different number) */
				    $sticky = array_slice( $sticky, 0, 1 );
					//print_r($sticky);
					$sticky_args = array(
						'post__in' => $sticky,
						'posts_per_page' => '1',
						'ignore_sticky_posts' => true,
						'post_status' => 'publish',
						//'post__in'  => get_option( 'sticky_posts' ),
						'order' =>'DSC',
					);
					$query = new WP_query ( $sticky_args );
					?>
					<?php
					if ( $query->have_posts() ) :
					?>
						
						<?php while ( $query->have_posts() ) : $query->the_post(); /* start the loop */ ?>
						<?php
						$post_date = get_the_date('m-j-y');
						$post_title = get_the_title();
						$post_excerpt = excerpt(30);
						$post_id = get_the_ID();
						$post_url = get_the_permalink();
						$post_image = get_the_post_thumbnail( $post_id, 'bones-thumb-600s', array( 'class' => 'b-featured-post__thumbnail', 'alt' => $post_title ) );
						$read_more_label = "Read More";
						?>
						<div class="b-featured-post b-featured-post_sticky">
							<div class="b-featured-post__inner">
								<?php if($post_image) : ?>
									<div class="b-resp-image b-resp-image_featured-post">
										<?php echo $post_image; ?>
									</div>
								<?php endif; ?>
								
								<div class="b-featured-post__content">
									<p class="b-featured-post__meta"><?php echo $post_date; ?></p>
									<?php if($post_title): ?><h3 class="b-featured-post__headline"><?php echo $post_title; ?></h3><?php endif; ?>
									<?php if($post_excerpt): ?> <p class="b-wysiwyg b-featured-post__text"><?php echo $post_excerpt; ?></p><?php endif; ?>
									<a class="b-button_flat b-button_small b-featured-post__button" href="<?php echo $post_url; ?>"><?php echo $read_more_label; ?></a>
								</div>
							</div>
						</div>
					<?php
						endwhile;
					endif;
					rewind_posts();
					?>
					<?php // END FIRST STICKY POST ?>
					
					
					
					<?php // REMAINING POSTS, WITH THE FIRST FOUR HAVING A UNIQUE STYLE ?>
					
					<?php // FIRST FOUR, NON-STICKY, FEATURED POSTS ?>
					<?php
					$featured_args = array(
						//'posts_per_page' => '1',
						//'post_status' => 'publish',
						//'post__not_in' => get_option( 'sticky_posts' ),
						//'order' =>'DSC',
					);
					//$featured_query = new WP_query ( $featured_args );
					//query_posts( $featured_args );
					?>
					<?php
					if ( have_posts() ) :
						$post_counter = 0;
						$total_posts = $wp_the_query->post_count + 1;
						$featured_offset = 4;
					?>
					<?php while ( have_posts() ) : the_post(); /* start the loop */ ?>
						<?php
						$post_date = get_the_date('m-j-y');
						$post_title = get_the_title();
						$post_excerpt = excerpt(30);
						$post_id = get_the_ID();
						$post_url = get_permalink( $post_id );
						$post_image = get_the_post_thumbnail( $post_id, "bones-thumb-600", array( 'class' => 'b-featured-post__thumbnail', 'alt' => $post_title ) );
						$read_more_label = "Read More";
						?>
						<?php // Get a list of all categories for the current post
						$original_post = $post->ID;
						$terms = get_the_terms( $post->ID, 'category' );
						$post_cats_list = "";
						//print_r($terms);
						if ( $terms && ! is_wp_error( $terms ) ) : 
							$post_cats = array();
							foreach ( $terms as $term ) :
								$post_cats[] = $term->slug;
							endforeach;
							$post_cats_list = join( " ", $post_cats );
						endif;
						?>
						<?php if( $post_counter < $featured_offset ) : //DO SOMETHING DIFFERENT WITH THE FIRST FOUR POSTS?>
						
							<?php if( 0 === $post_counter ) : //CREATE AN OPENING HOLDER DIV ON THE FIRST PASS THROUGH?>
							<div class="b-featured-posts b-columns">
							<?php endif; ?>
							
							<div class="b-featured-post js-sortable-object <?php echo $post_cats_list; ?> p-all m-all t-1of2 d-1of2">
								<div class="b-featured-post__inner">
									<?php if($post_image) : ?>
										<div class="b-resp-image b-resp-image_featured-post">
											<?php echo $post_image; ?>
										</div>
									<?php endif; ?>
									<div class="b-featured-post__content js-equal-heights_rows">
										<p class="b-featured-post__meta"><?php echo $post_date; ?></p>
										<?php if($post_title): ?><h3 class="b-featured-post__headline"><?php echo $post_title; ?></h3><?php endif; ?>
										<?php if($post_excerpt): ?> <p class="b-wysiwyg b-featured-post__text"><?php echo $post_excerpt; ?></p><?php endif; ?>
										<a class="b-button_flat b-button_small b-featured-post__button" href="<?php echo $post_url; ?>"><?php echo $read_more_label; ?></a>
									</div>
								</div>
							</div>
							
							<?php if( ($featured_offset -1) === $post_counter ) : ?>
							</div> <?php // CLOSE FEATURED POSTS CONTAINER ?>
							<?php endif; ?>
							
						<?php else : // END FEATURE STYLE OUTPUT; START THE REGULAR OUTPUT?>
							<?php
							$post_image = get_the_post_thumbnail( $post_id, "medium", array( 'class' => 'b-featured-post__thumbnail', 'alt' => $post_title ) );
							?>
							<?php if( $featured_offset === $post_counter ) : ?>
								
							<h2 class="b-headline b-headline_underscore">More Stories</h2>
							<main id="main" class="b-posts" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<?php endif; ?>
							
								<article id="post-<?php the_ID(); ?>" <?php post_class( 'b-regular-post b-columns js-sortable-object '.$post_cats_list ); ?> role="article">
									<?php if($post_image) : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="b-regular-post__image-link p-all m-all t-1of3 d-1of4">
										
										<div class="b-resp-image">
											<?php echo $post_image; ?>
										</div>
										
										<div class="b-regular-post__image_hover">
											<p>Read Story</p>
										</div>
									</a>
									<?php endif; ?>
									<div class="b-regular-post__content p-all m-all t-2of3 d-3of4">
										<p class="b-regular-post__meta"><?php echo $post_date; ?></p>
										<h2 class="h2 entry-title b-regular-post__title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
										<?php if($post_excerpt): ?> <p class="b-wysiwyg b-regular-post__text"><?php echo $post_excerpt; ?></p><?php endif; ?>
										<a class="b-regular-post__button" href="<?php echo $post_url; ?>"><?php echo $read_more_label; ?></a>
									</div>
								</article>
							
							<?php if( (($wp_the_query->post_count) + 1) === $post_counter ) : ?>
							</main> <?php // END REGULAR POSTS CONTAINER ?>
							<?php endif; ?>
						<?php endif; ?>
					<?php
						$post_counter++; // INCREASE POST COUNT BY ONE
						endwhile; // END HAVING POSTS
					?>
					
					<?php
					endif; // END POSTS LOOP
					?>

					
					
					<?php
					else : // END CHECK FOR PAGE 1 STATUS
					?>
					
					
					<?php if ( have_posts() ) :?>
					<h2 class="b-headline b-headline_underscore">Stories Page <?php echo $paged; ?></h2>
					<main id="main" class="b-posts" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
						<?php while ( have_posts() ) : the_post(); /* start the loop */ ?>
						

								<?php
								$post_date = get_the_date('m-j-y');
								$post_title = get_the_title();
								$post_excerpt = excerpt(30);
								$post_id = get_the_ID();
								$post_url = get_permalink( $post_id );
								$post_image = get_the_post_thumbnail( $post_id, array(150, 150), array( 'class' => 'b-featured-post__thumbnail', 'alt' => $post_title ) );
								$read_more_label = "Read More";
								?>
								<?php // Get a list of all categories for the current post
								$original_post = $post->ID;
								$terms = get_the_terms( $post->ID, 'category' );
								$post_cats_list = "";
								//print_r($terms);
								if ( $terms && ! is_wp_error( $terms ) ) : 
									$post_cats = array();
									foreach ( $terms as $term ) :
										$post_cats[] = $term->slug;
									endforeach;
									$post_cats_list = join( " ", $post_cats );
								endif;
								?>
								<article id="post-<?php the_ID(); ?>" <?php post_class( 'b-regular-post b-columns js-sortable-object '.$post_cats_list ); ?> role="article">
									<?php if($post_image) : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="b-regular-post__image-link p-all m-all t-1of3 d-1of4">
										
										<div class="b-resp-image">
											<?php echo $post_image; ?>
										</div>
										
										<div class="b-regular-post__image_hover">
											<p>Read Story</p>
										</div>
										
									</a>
									<?php endif; ?>
									<div class="b-regular-post__content p-all m-all t-2of3 d-3of4">
										<p class="b-regular-post__meta"><?php echo $post_date; ?></p>
										<h2 class="h2 entry-title b-regular-post__title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
										<?php if($post_excerpt): ?> <p class="b-wysiwyg b-regular-post__text"><?php echo $post_excerpt; ?></p><?php endif; ?>
										<a class="b-regular-post__button" href="<?php echo $post_url; ?>"><?php echo $read_more_label; ?></a>
									</div>
								</article>
					
						
						<?php
						endwhile; // END WHILE LOOP FOR PAGES 2 AND BEYOND
						?>
						</main>
					<?php
					endif; // END IF have_posts() FOR PAGES 2 AND BEYOND
					?>
					
					<?php
					endif; // END IF/ELSE FOR PAGED STATUS
					?>
		<?php bones_page_navi(); ?>			
	</div>

</div>


<?php //get_template_part('library/custom-loops/loop-flexible-content'); ?>

<?php get_footer(); ?>
