			<footer id="footer" class="b-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<?php if ( is_active_sidebar( 'footer_widgets_1' ) || is_active_sidebar( 'footer_widgets_2' ) ) : ?>
					<div class="b-footer__top">
						<div class="b-footer__inner b-footer__inner_top wrap">
							<?php  get_sidebar('footer1');  ?>
							<?php  get_sidebar('footer2');  ?>
						</div>
					</div>
				<?php endif; ?>	
					<div class="b-footer__bottom">
						<div class="b-footer__inner b-footer__inner_bottom wrap">
							<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> <?php bloginfo( 'description' ); ?></p>
							
							<?php get_template_part("/library/custom-loops/loop-social-options"); ?>
							
							<?php wp_nav_menu(array(
		    					'container' => 'nav',                              // remove nav container
		    					'container_class' => 'b-footer__nav',         // class of container (should you choose to use it)
		    					'menu' => __( 'Footer Links', 'regionalOfficeTheme' ),   // nav name
		    					'menu_class' => 'nav b-footer__links cf',            // adding custom nav class
		    					'theme_location' => 'footer-links',             // where it's located in the theme
		    					'before' => '',                                 // before the menu
			        			'after' => '',                                  // after the menu
			        			'link_before' => '',                            // before each link
			        			'link_after' => '',                             // after each link
			        			'depth' => 0,                                   // limit the depth of the nav
		    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
								)); ?>
							
							
						</div>
					</div>
				
				

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
