(function($) {

// Variables
$search_by_zip = false;

function set_form(status) {
	console.log('error status: '+status);
	$error_message_field =  $('.b-map-error-message');
	$zip_input = $('.b-input__zipcode-search');
	$massage_no_results = "Sorry, but there are no results for your zipcode. This is the closest location.";
	if(status == "no-results") {
		$error_message_field.html($massage_no_results);
		$error_message_field.css ({
			'display' : 'block'
		});
	} else if( status == "clear" ) {
		$error_message_field.html("");
		$error_message_field.css ({
			'display' : 'none'
		});
		$zip_input.val("");
	} else if( status =="no-geocode-results" ) {
		//hide the whole search apparatus
	}
}
/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function render_map( $el ) {

	// var
	var $markers = $el.find('.marker');
	var $useSearch = $el.hasClass('js-use-search');
	// vars
	var args = {
		zoom		: 11,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
	};

	// create map	        	
	var map = new google.maps.Map( $el[0], args);

	// add a markers reference
	map.markers = [];

	// add markers
	$markers.each(function(){

    	add_marker( $(this), map, $useSearch );

	});

// Only called when there are no results.
function rad(x) {return x*Math.PI/180;}
function find_closest_marker( event ) {
	
    var lat = event.lat();
    var lng = event.lng();
    var R = 6371; // radius of earth in km
    var distances = [];
    var closest = -1;
    for( i=0;i<map.markers.length; i++ ) {
        var mlat = map.markers[i].position.lat();
        var mlng = map.markers[i].position.lng();
        var dLat  = rad(mlat - lat);
        var dLong = rad(mlng - lng);
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        distances[i] = d;
        if ( closest == -1 || d < distances[closest] ) {
            closest = i;
        }
    }
	map.markers[closest].setVisible(true);
	center_map( map );
    set_form("no-results");
}


	// center map
	center_map( map );
	
	// style map
	style_map( map );
	
	// Turn on scroll zoom only after clicking on map. Turn it off after leaving map area.
	google.maps.event.addListener(map, 'click', function(event){
          if( this.scrollwheel != true ) {
          	this.setOptions({scrollwheel:true});
          } else {
          	this.setOptions({scrollwheel:false});
          }
			
    });
	google.maps.event.addListener(map, 'mouseout', function(event){
		this.setOptions({scrollwheel:false});  
	});
	
	//You can umcomment this to have it find closet pin to click point. there is a slight bug (hierarchy problem?) in the find_closeest marker that causes this to fail. check out the console.
	//google.maps.event.addListener(map, 'click', find_closest_marker);
	
	/*** Map Search Bar Functionality ***/
	function do_zip_search() {
		$newQuery = $('.b-input__zipcode-search').val();
		//console.log($newQuery);
		var any_visible_markers = false;
		// This flag lets the infow window know that the bounds change was done via the search box and not user-dragging.
		$search_by_zip = true;
		// Cycle through markers to find the ones that have a zipcode that matches the user input.
		$.each( map.markers, function( i, marker ){
			if(marker.zipcode !== $newQuery && $newQuery != "" ){
				marker.setVisible(false);
			} else {
				marker.setVisible(true);
				// Set a flag to show that at least one marker is visible.
				any_visible_markers = true;
				// Clear the Error Message
				set_form("clear");
			}
		});
		//don't center map if no markers are visible. It puts you in the ocean.
		if( any_visible_markers == true ){
			center_map( map );
		} else {
			//console.log('zip query is '+$newQuery);
			$.ajax({
		       url : "http://maps.googleapis.com/maps/api/geocode/json?components=postal_code:"+$newQuery+"",
		       method: "POST",
		       success:function(data){
		           latitude = data.results[0].geometry.location.lat;
		           longitude= data.results[0].geometry.location.lng;
		           //console.log("Lat = "+latitude+"- Long = "+longitude);
		           zipMarker = new google.maps.LatLng( latitude, longitude );
		           find_closest_marker( zipMarker );
		       }
		    });
			
		}
	}
	// Pressing Enter/Return while in the zipcode input field
	$('.js-search-map_input').keypress(function (e) {
	    if (e.keyCode == 13) {
	        do_zip_search();
	    }
	 });
	
	// Search Button
	$('.js-search-map').click(function(){
		do_zip_search();
	});
	// The reset button sets all markers to visible, resets the boundaries to show all, and clears the input box.
	$('.js-reset-map').click(function(){
		$.each( map.markers, function( i, marker ){
			//console.log(map.markers[i]);
			map.markers[i].setVisible(true);
		});
		
		center_map( map );
		// Clear the Error Message
		set_form("clear");
	});
}

/*
* Reverse Geocoding for details
* 
* these functions are used by the map marker initializer.
*/
//NOT Implemented at this point.///////////
function ajaxGeoCode(theURL) {
	$.ajax({
		url : theURL,
		method: "POST",
		success:function(data){
			//console.log(data);
		}
	});
}
////////////////////////////////////////////
function httpGet(theUrl) {
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
}
// returns the full, proper street address with city, state.
function getAddressFromGoogle(response) {
	if( response != undefined ) {
		var obj = jQuery.parseJSON(response);
		if( response.status !== "ZERO_RESULTS" ){
			return obj.results[0].formatted_address;
		}
	}
}
// returns the zip code from a Google Maps Geocoding Request.
function getZipFromGoogle(response) {
	if( response != undefined ) {
		//console.log(response);
		var obj = jQuery.parseJSON(response);
		//console.log(obj.status);
		if( obj.status == "OK" ){
			//console.log(response.status);
			var addressComponents = obj.results[0].address_components;
			addressComponents.forEach(function(element) {
			  	if ( $.inArray('postal_code', element['types']) != -1 ) {
			  		zip = element['long_name'];
			  	}
	  		});
	  		console.log("Google API Response: "+obj.status);
	  		return zip;
		} else {
			console.log("Google API Response: "+obj.status);
		}
		
	}
}
// returns the zip code from the database


// Checks to see if the marker in the argument is in the map bounds/viewport
function check_is_in_or_out(map, marker){
  return map.getBounds().contains(marker.getPosition());
}
// Cycles all markers to find if there is only one marker in bounds/viewport. Uses the function above.
function contains_only_one(map, marker){
	var number_of = 0;
	$.each( map.markers, function( i, element ){
		if( check_is_in_or_out(map, element) == true ) {
			number_of++;
		}
	});
	return number_of;
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map, $useSearch ) {
	//console.log('useSearch is '+ $useSearch);
	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	//console.log(latlng);
	// Applies the marker's title as a data point called, category.
	var category = $marker.find('.b-marker__title').html();
	//Only do the zip search stuff if there's a search bar with the map
	if( true === $useSearch ) {
		// Find out if the zip code is already set on the marker
		var zipcode = $marker.attr('data-zip');
		// If it's not, use Google Maps API on the front-end
		if ( !zipcode ) {
			// Call to the Google Maps Geocode API, using the current marker's lat and lng.
			var geocode = httpGet("https://maps.googleapis.com/maps/api/geocode/json?latlng="+$marker.attr('data-lat')+","+$marker.attr('data-lng')+"");
			// Uses lat and lng reverse geocode to get the formatted zip code for attachment as a data point on the marker.
			var zipcode = getZipFromGoogle(geocode);
			//console.log(zipcode);
		}
	} else {
		zipcode = ""
	}
	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon	 : "http://maps.google.com/mapfiles/ms/icons/orange-dot.png",
		category: category,
		zipcode : zipcode
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content	: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open( map, marker );
		});
		
		// show info window if only one in bounds and if the bounds_changed event was called by the zip code search, rather than user dragging/zooming.
		google.maps.event.addListener(map, 'bounds_changed', function() {
			// Is there only one marker?
			if (contains_only_one(map, marker) == 1) {
				// Was the movement caused by searching?
				if( $search_by_zip == true ) {
					// Is this the marker that's in bounds?
					if( check_is_in_or_out(map, marker) ){
						infowindow.open( map, marker );
						// Reset the search flag so that dragging/zooming doesn't reopen window.
						$search_by_zip = false;
					}
				}
			}
		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();
	var visible_markers = 0;
	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
		if( marker.visible == true ) {
			bounds.extend( latlng );
			visible_markers++;
		}
	});

	// no marker?
	if( map.markers.length == 0 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 4 );
	}
	// only 1 marker?
	else if ( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 10 );
	}
	else
	{
		// fit to bounds
		if(visible_markers > 1){
			map.fitBounds( bounds );
		} else {
			map.setCenter( bounds.getCenter() );
			map.setZoom( 13 );
			//console.log(map.markers);
		}
	}
}



/*
*  style_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	3/17/2015
*  @since	0
*
*  @param	$mapStyle (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function style_map( map ) {
	// Create an array of styles.
	
	map.setOptions({styles: styles});
	  // Create a new StyledMapType object, passing it the array of styles,
	  // as well as the name to be displayed on the map type control.
	  	var styledMap = new google.maps.StyledMapType(styles,
	    {name: "Styled Map"});
	    
	   //Associate the styled map with the MapTypeId and set it to display.
  		map.mapTypes.set('map_style', styledMap);
  		map.setMapTypeId('map_style');

}
/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/

$(document).ready(function(){

	$('.acf-map').each(function(){

		render_map( $(this) );

	});
	
	

});

})(jQuery);