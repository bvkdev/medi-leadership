// Get localized variables
var facebookAppID = fb_variables.appID;
//console.log("facebookAppID is "+facebookAppID);

window.fbAsyncInit = function() {
    FB.init({
      appId      : facebookAppID,
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
