
/*
 * Bones Scripts File
 * Authors: Eddie Machado and Benjamin Gray
 *
*/
// Get localized variables
var templateUrl = directories.themeURI;
var homepageUrl = directories.homepage;
//console.log("templateUrl is "+templateUrl);
/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();
// match variables to breakpoints (you may need to update these if you change your CSS)
var screenMobile = 480;
var screenTablet = 768;
var screenDesktop = 1030;

/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;

//INDEX OF POLYFILL FOR IE11
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, "includes", {
    enumerable: false,
    value: function(obj) {
        var newArr = this.filter(function(el) {
          return el == obj;
        });
        return newArr.length > 0;
      }
  });
}

/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/
//POLYFILL FOR OBJECT FIT VIDEOS	
var objectFitVideos=function(t){"use strict";function e(t){for(var e=getComputedStyle(t).fontFamily,o=null,i={};null!==(o=l.exec(e));)i[o[1]]=o[2];return i["object-position"]?n(i):i}function o(t){var o=-1;t?"length"in t||(t=[t]):t=document.querySelectorAll("video");for(;t[++o];){var n=e(t[o]);(n["object-fit"]||n["object-position"])&&(n["object-fit"]=n["object-fit"]||"fill",i(t[o],n))}}function i(t,e){function o(){var o=t.videoWidth,n=t.videoHeight,d=o/n,a=r.clientWidth,c=r.clientHeight,p=a/c,l=0,s=0;i.marginLeft=i.marginTop=0,(d<p?"contain"===e["object-fit"]:"cover"===e["object-fit"])?(l=c*d,s=a/d,i.width=Math.round(l)+"px",i.height=c+"px","left"===e["object-position-x"]?i.marginLeft=0:"right"===e["object-position-x"]?i.marginLeft=Math.round(a-l)+"px":i.marginLeft=Math.round((a-l)/2)+"px"):(s=a/d,i.width=a+"px",i.height=Math.round(s)+"px","top"===e["object-position-y"]?i.marginTop=0:"bottom"===e["object-position-y"]?i.marginTop=Math.round(c-s)+"px":i.marginTop=Math.round((c-s)/2)+"px"),t.autoplay&&t.play()}if("fill"!==e["object-fit"]){var i=t.style,n=window.getComputedStyle(t),r=document.createElement("object-fit");r.appendChild(t.parentNode.replaceChild(r,t));var d={height:"100%",width:"100%",boxSizing:"content-box",display:"inline-block",overflow:"hidden"};"backgroundColor backgroundImage borderColor borderStyle borderWidth bottom fontSize lineHeight left opacity margin position right top visibility".replace(/\w+/g,function(t){d[t]=n[t]});for(var a in d)r.style[a]=d[a];i.border=i.margin=i.padding=0,i.display="block",i.opacity=1,t.addEventListener("loadedmetadata",o),window.addEventListener("optimizedResize",o),t.readyState>=1&&(t.removeEventListener("loadedmetadata",o),o())}}function n(t){return~t["object-position"].indexOf("left")?t["object-position-x"]="left":~t["object-position"].indexOf("right")?t["object-position-x"]="right":t["object-position-x"]="center",~t["object-position"].indexOf("top")?t["object-position-y"]="top":~t["object-position"].indexOf("bottom")?t["object-position-y"]="bottom":t["object-position-y"]="center",t}function r(t,e,o){o=o||window;var i=!1,n=null;try{n=new CustomEvent(e)}catch(t){n=document.createEvent("Event"),n.initEvent(e,!0,!0)}var r=function(){i||(i=!0,requestAnimationFrame(function(){o.dispatchEvent(n),i=!1}))};o.addEventListener(t,r)}var d=navigator.userAgent.indexOf("Edge/")>=0,a=new Image,c="object-fit"in a.style&&!d,p="object-position"in a.style&&!d,l=/(object-fit|object-position)\s*:\s*([-\w\s%]+)/g;c&&p||(o(t),r("resize","optimizedResize"))};"undefined"!=typeof module&&"undefined"!=typeof module.exports&&(module.exports=objectFitVideos);


function ro_lock_header(ro_class_name) {
	viewport = updateViewportDimensions();
	if (viewport.width >= 768) {
		ro_header_height = jQuery(ro_class_name).height();
		
		jQuery(ro_class_name).css({
			'position' : 'fixed',
			'z-index' : '10000',
			//'top'	: 	ro_header_height
		});
		
		jQuery('.js-header-buffer').css({
			'height' : ro_header_height,
			'display' : 'block'
		});
	} else {
		jQuery(ro_class_name).css({
			'position' : 'relative'
		});
		jQuery('.js-header-buffer').css({
			'height' : 0
		});
	}
}

// MOBILE NAV SWITCH
function ro_mobileNavSwitch(switchElement) {
	jQuery(switchElement).toggleClass("on");
	jQuery('body').toggleClass("menu_open");
	jQuery('.js-header-nav').slideToggle();
}

function ro_menuOnResize(switchElement) {
	var viewport = updateViewportDimensions();
	if( viewport.width >= 768 ) {
		jQuery(switchElement).removeClass("on");
		jQuery('body').removeClass("menu_open");
		jQuery('.js-header-nav').show().css('display', 'flex');
	} else {
		if (jQuery(switchElement).hasClass("on") ) {
			jQuery('body').addClass("menu_open");
			jQuery('.js-header-nav').show().css('display', 'flex');
		} else {
			jQuery('.js-header-nav').hide();
		}
	}
};
//
function ro_full_height(section_name) {
	var viewport = updateViewportDimensions();
	$(section_name).css({
		"height": viewport.height
	});
}

//
function smooth_scroll(menu_locked) {
	// Smooth Scroll to Anchors
	var page_root = jQuery('html, body');
	var headerOffset = 0;
	
	// Adjust the offset of the main menu for whether or not the brand menu is visible. Called in the click function below.
	isScrolled = function() {
		//console.log($(window).scrollTop());
		if (jQuery(window).scrollTop() == 0){
			headerOffset = jQuery('header').height() * 2;
		} else {
			headerOffset = jQuery('header').height();	
		}
	};
	
	jQuery('a[href*="#"]').not('.b-mobile-nav-switch__link').click(function() {
		if( menu_locked == true ) { isScrolled(); }
		href = jQuery.attr(this, 'href');
		console.log(href);
		page_root.animate({
			scrollTop: jQuery(href).offset().top-headerOffset
		}, 1000, function() {
			window.location.hash = href;
		});
		return false;
	});
};

function liGrid(organizer, column_number) {
	if (jQuery.fn.nwrapper !== undefined) {
		//Defaults
		organizer = organizer || 'ul';
		column_number = column_number || 2;
		//
		//console.log(column_number);
		jQuery('.js_li_grid').each(function () {
			var data_organizer_term = jQuery(this).data("organizer");
			var data_organizer_class = "";
			if(['p','div','a'].includes(data_organizer_term)) {
				//console.log("does contain");
				var data_organizer_class = jQuery(this).data("organizer");
			} else {
				//console.log("does not contain");
				var data_organizer_class = "."+jQuery(this).data("organizer");
			}
			if(data_organizer_term != undefined) {
				//console.log("not undefined");
				var list = jQuery(this).find(data_organizer_class);
				var list_amount = list.length;
				//console.log(list_amount);
				var two_column_amount = Math.ceil(list_amount/2);
				jQuery(this).nwrapper({ wrapEvery : two_column_amount, defaultClasses : false, extraClasses : ['p-1of2', 'm-1of2', 't-1of2', 'd-1of2'], htmlStructure : 'span' });

			} else {
				(jQuery('ul li',this).length > 0) ? list_style = 'ul' : list_style = 'ol';
				//console.log(list_style);
				if(list_style) {
					var list_amount = jQuery(list_style+' li',this).length;
					var four_column_amount = Math.ceil(list_amount/4);
					var two_column_amount = Math.ceil(list_amount/2);
					jQuery(list_style,this).nwrapper({ wrapEvery : two_column_amount, defaultClasses : false, extraClasses : ['p-all', 'm-1of2', 't-1of2', 'd-1of2'], htmlStructure : 'span' });
				}
			}
		});
	} //END nWRAPPER FUNCITON CHECK
} // end function

function pGrid() {
	if (jQuery.fn.nwrapper !== undefined) {
		
		jQuery('.js_p_grid').each(function () {
			var data_organizer_term = jQuery(this).data("organizer");
			data_organizer_term = "p";
			var data_organizer_class = "";
			if(['p','div','a'].includes(data_organizer_term)) {
				//console.log("does contain");
				var data_organizer_class = data_organizer_term;
			} else {
				//console.log("does not contain");
				var data_organizer_class = "."+data_organizer_term;
			}
			if(data_organizer_term != undefined) {
				//console.log("not undefined");
				var list = jQuery(this).find(data_organizer_class);
				var list_amount = list.length;
				//console.log(list_amount);
				if(list_amount > 1) {
					var two_column_amount = Math.ceil(list_amount/2);
					jQuery(this).nwrapper({ wrapEvery : two_column_amount, defaultClasses : false, extraClasses : ['b-text-columns-span p-all', 'm-all', 't-1of2', 'd-1of2'], htmlStructure : 'span' });
				}
			}
		});
	} //END nWRAPPER FUNCTION CHECK
} // end function


// ICON CAROUSEL
function icon_carousel() {
	if(	jQuery('.js_icon_carousel') ){
		jQuery('.js_icon_carousel').each(function () {
			var grid_width = jQuery(this).attr('data-grid');
			if ($.fn.nwrapper !== undefined) {
				jQuery(this).nwrapper({ wrapEvery : grid_width, defaultClasses : false, extraClasses : ['slide', 'b-icon-carousel-slide'], htmlStructure : 'span' });
			}
			});
	}
} // end function


setUpRecipeSocial = function() {
	
	// FACEBOOK RECIPE SHARE BUTTON
	if( jQuery('.b-share-link_facebook').length > 0 ) {
		jQuery('.b-share-link_facebook').click(function(e){
			e.preventDefault();
			var href = jQuery('a', this).attr('href');
			href = encodeURI(href);
			FB.ui({
			    method: 'share',
			    display: 'popup',
			    href: href,
			}, function(response){});
		});
	}
	
	
};



jQuery(document).ready(function($) {
	var viewport = updateViewportDimensions();
	
	//EDGE object fit videos
	objectFitVideos();
	//Find email addresses and make them links
	$('.xb-board__members').each(function(){
        // Get the content
        var str = $(this).html();
        // Set the regex string
        var regex = /(([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z]))/ig;
        // Replace plain text links by hyperlinks
        var replaced_text = str.replace(regex, "<a href=mailto:$1 target='_blank'>$1</a>");
        // Echo link
        $(this).html(replaced_text);
    });
    //Find URLs and make them links
	$('.xb-board__members').each(function(){
        // Get the content
        var str = $(this).html();
        // Set the regex string
        var regex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        // Replace plain text links by hyperlinks
        var replaced_text = str.replace(regex, "<a href='$1' target='_blank'>$1</a>");
        // Echo link
        $(this).html(replaced_text);
    });
	
	
	//make home hero full height
	//ro_full_height(".b-section_hero");
	
	$('.js-reveal-nav').click(function(){
		ro_mobileNavSwitch(this);
	});
	
	// MOBILE NAV SWITCH
    // Reveal and Hide the Nav on Mobile Devices
	ro_menuOnResize();
	
	//LOCK HEADER IN PLACE DURING SCROLL
	ro_header_lock = ".js-fixed-header";
	if( $(ro_header_lock).length > 0 ) {
		ro_lock_header(ro_header_lock);
	}
	
	/*
	 * search header launch
	 */
	$('.b-search-button_launch').click(function(){
		$('.b-header form#searchform').toggleClass('vis');
	});


	//INCREASE THE FONT-SIZE ON THE FIRST TEXT AREA UNDER THE PAGE HERO
	if( $('.b-section_textedit').length > 0 ) {
		$('.b-section_textedit').first().find(".b-textedit__headline").css({
			'font-size' : '2.375em'
		});
	}
	
	//HOMEPAGE FULL HEIGHT PICTURE
	//if( ! $('.full_panel_scrolling').length > 0) {
	if( $('.home').length > 0) {
		$header_buffer = $('.js-fixed-header').height();
		$footer_buffer = 50;
		$full_height = viewport.height - $header_buffer - $footer_buffer;
		$proportional_height = viewport.width * .68;
		$final_height = Math.min($full_height, $proportional_height);
		$('.b-section_hero').css({
			'height' : $final_height,
			'min-height' : 0
		});
	}
	
	// FULL PAGE SECTION SCROLLING
	if( $('.full_panel_scrolling').length > 0) {
		if(viewport.width >= screenDesktop) {
			$header_buffer = $('.js-fixed-header').height();
			$footer = $('.b-footer');
			$footer.addClass('b-section fp-auto-height');
			
			console.log($header_buffer);
			if( $('#container').length >0 ) {
				$('#container').fullpage({
					//anchors: ['firstPage', 'secondPage', '3rdPage', '4thPage'],
					//sectionsColor: ['#4A6FB1', '#939FAA', '#323539'],
					hybrid:true,
					scrollOverflow: true,
					//scrollBar: true,
					//normalScrollElements: '.b-footer',
					//autoScrolling:true, 
					//
					//verticalCentered: true,
					paddingTop: $header_buffer,
					//paddingBottom: "150",
					//responsiveHeight: 1,
					//fixedElements: '.b-footer',
					//
					sectionSelector: '.b-section',
					navigation: true,
					navigationPosition: 'left',
				});
			}
			$('#fp-nav.left').css({
				'padding-top' : $header_buffer
			});
		}
	}
	
	
	
	// FAQs ACCORDION
	if( $('.js-reveal-sibling').length > 0 ) {
		$('.js-reveal-sibling').on('click', function(e) {
			$this = $(this);
			$this.find('.b-faq__question_sprite').toggleClass('open');
			$this.next('.b-faq__answer').slideToggle();
		});
	}
	
	// POST SORTING BAR
	if( $('#xpost-sort-bar').length > 0 ) {
		$sortbar = $('#post-sort-bar');
		$('li', $sortbar).click(function(){
			var my_cats = $(this).data('category');
			
			//SHOW EVERYTHING, BUT THEN IMMEDIATELY HIDE EVERYTHING THAT ISN'T MY CATEGORY
			$(".js-sortable-object").show().not('.'+my_cats).hide();
			
			//change class on sortbar buttons for highlighting effect
			$('li', $sortbar).addClass('b-post-sort-option_active').not($(this)).removeClass('b-post-sort-option_active');
			
			//SHOW ALL BUTTON
			if($(this).hasClass('b-post-sort-option_all')) {
				$(".js-sortable-object").show();
				$(this).removeClass('b-post-sort-option_active');
			}
			
		});
		
	}
	
	//GRID/COLUMNS
	if ($.fn.nwrapper !== undefined) {
		
		if( $('.js_grid_2').length > 0 ) {
			$('.js_grid_2').each(function () {
				var list_amount = $('.js_grid_item',this).length;
				var two_column_amount = Math.ceil(list_amount/2);
				$(this).nwrapper({ wrapEvery : two_column_amount, defaultClasses : false, extraClasses : ['p-1of2', 'm-1of2', 't-1of2', 'd-1of2'], htmlStructure : 'span' });
			});
		}
	
		if( $('.js_li_grid').length > 0 ) {
			//console.log("js-li-grid amount is "+$('.js_li_grid').length);
			liGrid();
		}
		
		if( $('.js_p_grid').length > 0 ) {
			//console.log("js-li-grid amount is "+$('.js_li_grid').length);
			pGrid();
		}
	}
	
	
	// HERO SLIDE SHOW
	// Fire off bxSlider
	if($('.js-hero-slideshow').length > 0 ){
		
		//HERO SLIDESHOWS
		if ( $('.b-hero-slide').length != 0 ) {
		$('.b-hero-slide').matchHeight({
				byRow: true,
			});
		}
		window.heroSliders = new Array();
			$('.js-hero-slideshow').each(function(i, slider) {
				//console.log(this);
				// fire off the bxSlider code for each showcase
			    heroSliders[i] = $(slider).bxSlider({
					autodelay: 2000,
				  	autohover: true,
				  	infiniteLoop: true,
				  	pause: 3000,
				  	adaptiveHeight: false,
				  	pager: false,
				  	controls: true,
				  	easing: 'easeOutElastic',
				  	speed: 800,
					onSlideAfter: function ($slideElement, oldIndex, newIndex) {
			    	//remove the acitve-slide class from any that have it, in this particular slideshow
				    $('.active-slide', $slideElement.parent()).removeClass('active-slide');
				    $slideElement.addClass('active-slide');
					},
					onSliderLoad: function (currentIndex) {
						//console.log(this);
					    $('.js-slideshow__showcase .slide').eq(1).addClass('active-slide');
					},
				  	
				});
				//$('.b-bx-pager').css("display", "none");
			});
	};
			
			
	// ICON CAROUSELS
    if($('.js_icon_carousel').length != 0 ) {
    	$('.js_icon_carousel').each(function(){
    		var list_amount = jQuery('li',this).length;
    		//console.log('list amount is '+list_amount);
    		var grid_width = parseInt( jQuery(this).attr('data-grid') );
    		//add wrappers to list of icons
    		if ($.fn.nwrapper !== undefined) {
    			jQuery(this).nwrapper({ wrapEvery : grid_width, defaultClasses : false, extraClasses : ['slide', 'b-columns', 'b-icon-carousel-slide'], htmlStructure : 'span' });
    		}
    		// Fire off bxSlider for the carousel
    		if ($.fn.bxSlider !== undefined) {
	    		if(list_amount > grid_width) {
	    		  	$(this).bxSlider({
	    		  		auto: true,
	    			  	autodelay: 2000,
	    			  	autohover: true,
	    			  	pause: 4000,
	    			  	adaptiveHeight: true,
	    			  	pager: true,
	    			  	controls: false,
	    			  	easing: 'easeOutElastic',
	    			  	speed: 1500
	    		  	});
	    	  	};
    		}
    	});

  	}
    
    
	// MOBILE DETECTION
    if( $.fn.isMobile !== undefined ) {
		if (isMobile.apple.device || isMobile.android.device || isMobile.seven_inch) {
			$('body').addClass("mobileDevice");
		}
	}
    
	//SMOOTH SCROLLING
	if($.isFunction(smooth_scroll)){
		smooth_scroll(false);
	}
	
	//Jump to select change
	$('.js-jumpto-select').change(function(){
    	location.hash = $(this).val();
    });
	
	//MATCH HEIGHTS BETWEEN ELEMENTS
	if ($.fn.matchHeight !== undefined) { 
		//console.log("match height");
		var matchSingleRows = false;
		var matchAllRows = true;
		if( $('.js-equal-heights').length != 0 ) {
			//console.log("match height: no row");
			$('.js-equal-heights').matchHeight({
				byRow: matchSingleRows,
				
			});
		}
		
		
		if( $('.js-equal-heights_rows').length != 0 ) {
			//console.log("match height: with rows");
			if(viewport.width >= screenMobile) {
				$('.js-equal-heights_rows').matchHeight({
					byRow: matchAllRows,
				});
			}
		}
		// FEATURED POSTS
		if( $('.js-equal-heights_rows.b-featured-post__content').length != 0 ) {
			$('.js-equal-heights_rows.b-featured-post__content').matchHeight({
				byRow: true,
			});
		}
		// ICON CAROUSELS
		if( $('.js-equal-heights_rows.b-icon__inner').length != 0 ) {
			$('.js-equal-heights_rows.b-icon__inner').matchHeight({
				byRow: true,
			});
		}
	}
	
	//sliders
	if ($.fn.bxSlider !== undefined) {
		
		if($('.js-featured-posts_carousel').length > 0 ) {
			window.featuredPostSliders = new Array();
			
			update_featured_posts_carousel_settings = function(){
				if( viewport.width < 481 ) {
					var slide_width = $('.b-featured-post').width();
					var slide_min = 1;
					var slide_max = 1;
					var control_vis = false;
				} else if( viewport.width >= 481 && viewport.width < 768 ) {
					var slide_width = $('.b-featured-post').width();
					var slide_min = 1;
					var slide_max = 1;
					var control_vis = true;
				}
				else if( viewport.width >= 768 ) {
					var slide_width = $('.b-featured-post').width();
					var slide_min = 1;
					var slide_max = 1;
					var control_vis = true;
				}
				var featured_posts_carousel_settings = {
					auto: true,
				  	autodelay: 2000,
				  	autohover: true,
				  	pause: 8000,
				  	adaptiveHeight: false,
				  	pager: true,
				  	controls: control_vis,
				  	easing: 'easeOutElastic',
				  	speed: 1500,
					onSliderLoad: function(currentIndex) {
						if(viewport.width >= screenMobile) {
							/*$('.b-featured-post').matchHeight({
								byRow: true,
							});*/
						}
					},
				  	minSlides: slide_min,
				  	maxSlides: slide_max,
				  	slideWidth: slide_width,
				};
				return featured_posts_carousel_settings;
			};
			
		  	$('.js-featured-posts_carousel').each(function(i, slider) {
		  		if(this.children.length > 1){
		  		// fire off the bxSlider code for each set of Featured Posts, using the settings creating by the above function.
	  			featuredPostSliders[i] = $(slider).bxSlider(update_featured_posts_carousel_settings());
	  			}
		  	});

		} //END FEATURED POSTS CAROUSELS
		
		if($('.js-carousel_quote').length > 0 ) {
		  	$('.js-carousel_quote').each(function(){
		  		if(this.children.length > 1){
		  			$(this).bxSlider({
				  		auto: true,
					  	autodelay: 2000,
					  	autohover: true,
					  	pause: 8000,
					  	adaptiveHeight: false,
					  	pager: true,
					  	controls: false,
					  	easing: 'easeOutElastic',
					  	speed: 1500,
					 
				  	});
		  		}
		  	});
		  }
	} //END BX SLIDER FUNCTION CHECK
	

	// Fire off video colorBoxes
	if ($.fn.colorbox !== undefined) {
		if($('a.cboxElement_video').length > 0){
			$('a.cboxElement_video').colorbox({
				iframe:true,
				width:'85%',
				height:'80%',
				href:function(){
					var videoId = new RegExp('[\\?&]v=([^&#]*)').exec(this.href);
					//console.log( videoId[1] );
					if (videoId && videoId[1]) {
	    				return 'http://youtube.com/embed/'+videoId[1]+'?rel=0&wmode=transparent&autoplay=1';
			        }
			   }
		  	});
		}
	}
	
	//OBJECT FIT VIDEOS POLYFILL
	objectFitVideos();
	
}); /* end of as page load scripts */

jQuery(window).load(function() {
	//RESPONSIVE IMAGE MAPS
	if (jQuery.fn.imageMapResize !== undefined) { 
		jQuery('map').imageMapResize();
	};

});
/* END OF WINDOW LOAD FUNCTIONS */

jQuery(window).resize(function () {
	if( viewport.width != jQuery(document).width() ) {
		var timeToWaitForLast = 100;
		// if featured Posts Carousel exists, we wait the set amount (in function above) then fire the function
		if ( jQuery('.b-featured-posts__wrap_carousel').length != 0 ) {
			waitForFinalEvent( function() {
				//update_featured_posts_carousel_settings();
				viewport = updateViewportDimensions();
				jQuery.each(featuredPostSliders, function(i, slider) {
			  		// fire off the bxSlider code for each set of Featured Posts
			  		//console.log(update_featured_posts_carousel_settings());
		  			slider.reloadSlider(update_featured_posts_carousel_settings());
				});
			}, timeToWaitForLast, "featured_posts_carousel");
		}
		
		
		
		//MAKE SURE THE MENU IS VISIBLE ON RESIZE
		waitForFinalEvent( function() {
			ro_menuOnResize();
		}, timeToWaitForLast, "ro_menuOnResize");
		
		//LOCK HEADER IN PLACE DURING SCROLL
		waitForFinalEvent( function() {
			ro_header_lock = ".js-fixed-header";
			if( jQuery(ro_header_lock).length > 0 ) {
				ro_lock_header(ro_header_lock);
			}
		}, timeToWaitForLast, "ro_header_lock");
	};
	
});
