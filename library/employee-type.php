<?php
/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'employee_categories', 
		array('type_employee'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Employee Categories', 'mediLeadershipTheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Employee Category', 'mediLeadershipTheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Employee Categories', 'mediLeadershipTheme' ), /* search title for taxomony */
				'all_items' => __( 'All Employee Categories', 'mediLeadershipTheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Employee Category', 'mediLeadershipTheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Employee Category:', 'mediLeadershipTheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Employee Category', 'mediLeadershipTheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Employee Category', 'mediLeadershipTheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Employee Category', 'mediLeadershipTheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Custom Employee Name', 'mediLeadershipTheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'employees' ),
		)
	);
add_rewrite_tag('%employee_categories%', '([^&/]+)');
// let's create the function for the custom type
function custom_post_employee() { 
	// creating (registering) the custom type 
	register_post_type( 'type_employee', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Employees', 'mediLeadershipTheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Employee', 'mediLeadershipTheme' ), /* This is the individual type */
			'all_items' => __( 'All Employees', 'mediLeadershipTheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'mediLeadershipTheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add A New Employee', 'mediLeadershipTheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'mediLeadershipTheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Employee', 'mediLeadershipTheme' ), /* Edit Display Title */
			'new_item' => __( 'New Employee', 'mediLeadershipTheme' ), /* New Display Title */
			'view_item' => __( 'View This Employee', 'mediLeadershipTheme' ), /* View Display Title */
			'search_items' => __( 'Search Employees', 'mediLeadershipTheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'mediLeadershipTheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'mediLeadershipTheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the custom post type for employees.', 'mediLeadershipTheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-list-view', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'coaches', 'with_front' => false ), /* you can specify its url slug */
			'taxonomies' => array( 'employee_categories' ),
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'page',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'page-attributes', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	/*register_taxonomy_for_object_type( 'category', 'type_employee' );
	/* this adds your post tags to your custom post type */
	/*register_taxonomy_for_object_type( 'post_tag', 'custom_type' );*/
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_employee');
	
	add_filter('post_type_link', 'employees_permalink_structure', 10, 4);
	function employees_permalink_structure($post_link, $post, $leavename, $sample)
	{
	    if ( false !== strpos( $post_link, '%employee_categories%' ) ) {
	        $employee_type_term = get_the_terms( $post->ID, 'employee_categories' );
	        $post_link = str_replace( '%employee_categories%', array_pop( $employee_type_term )->slug, $post_link );
	    }
	    return $post_link;
	}
	
	// now let's add custom tags (these act like categories)
	//register_taxonomy( 'custom_tag', 
		/*array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		/*array('hierarchical' => false,    /* if this is false, it acts like tags */
		/*	'labels' => array(
		/*		'name' => __( 'Custom Tags', 'mediLeadershipTheme' ), /* name of the custom taxonomy */
		/*		'singular_name' => __( 'Custom Tag', 'mediLeadershipTheme' ), /* single taxonomy name */
		/*		'search_items' =>  __( 'Search Custom Tags', 'mediLeadershipTheme' ), /* search title for taxomony */
		/*		'all_items' => __( 'All Custom Tags', 'mediLeadershipTheme' ), /* all title for taxonomies */
		/*		'parent_item' => __( 'Parent Custom Tag', 'mediLeadershipTheme' ), /* parent title for taxonomy */
		/*		'parent_item_colon' => __( 'Parent Custom Tag:', 'mediLeadershipTheme' ), /* parent taxonomy title */
		/*		'edit_item' => __( 'Edit Custom Tag', 'mediLeadershipTheme' ), /* edit custom taxonomy title */
		/*		'update_item' => __( 'Update Custom Tag', 'mediLeadershipTheme' ), /* update title for taxonomy */
		/*		'add_new_item' => __( 'Add New Custom Tag', 'mediLeadershipTheme' ), /* add new title for taxonomy */
		/*		'new_item_name' => __( 'New Custom Tag Name', 'mediLeadershipTheme' ) /* name title for taxonomy */
		/*	),
		/*	'show_admin_column' => true,
		/*	'show_ui' => true,
		/*	'query_var' => true,
		)
	);
	
	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/
	

?>
