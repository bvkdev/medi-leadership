<?php
$featured_program_title = get_the_title();
$featured_program_excerpt = excerpt(10);
$featured_program_id = get_the_ID();
$featured_program_url = get_permalink( $featured_program_id );
$featured_program_image = get_the_post_thumbnail( $featured_program_id, 'full', array( 'class' => 'b-featured-program__thumbnail', 'alt' => $featured_program_title ) );
?>



<li class="b-feature-card b-feature-card_programs b-feature-card_programs p-all m-1of2 t-1of2 d-1of2">
<div class="b-feature-card__inner b-feature-card__inner_programs js-equal-heights_rows">
		<a href="<?php echo $featured_program_url; ?>" title="<?php echo $featured_program_title; ?>" class="b-feature-card__image-link">
		<div class="b-feature-card__image b-feature-card__image_programs b-resp-image b-resp-image_feature-card b-resp-image_feature-card_programs">
			<?php if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it. ?>
				<?php echo $featured_program_image; ?>
			<?php endif; ?>
		</div>
		<div class="b-feature-card__image_hover b-feature-card__image_hover_programs">
		<p>View Program</p>
		</div>
															
		</a>
		<?php if($featured_program_title): ?><h3 class="b-feature-card__headline b-feature-card__headline_default"><?php echo $featured_program_title; ?></h3><?php endif; ?>																
</div>
</li>