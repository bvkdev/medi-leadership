<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
// Get values for page intro
$headline = get_field('headline');
$text = get_field('subheadline');
$dollars_chart = get_field('donation_values');
$donation_form = get_field('donation_form');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_donate-panels b-donate-panels <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_donate-panels cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_donate-panels cf b-columns">
			<div class="b-half-panel b-half-panel_left b-half-panel_donate-list p-all m-all t-1of2 d-1of2">
				<?php if($headline): ?> <h3 class="b-half-panel__headline"><?php echo $headline; ?></h3><?php endif; ?>
				<?php if($text): ?> <p class="b-half-panel__text"><?php echo $text; ?></p><?php endif; ?>
				<?php if($dollars_chart): ?>
					<?php if( have_rows('donation_values') ): ?>
						<ul class="b-dollars-chart">
	    			<?php while( have_rows('donation_values') ): the_row(); ?>
	    					<li class="b-dollars_chart__item"><span class="b-dollars_chart__item_amount"><?php the_sub_field('dollar_amount'); ?></span> <span class="b-dollars_chart__item_value"><?php the_sub_field('equivalent'); ?></span></li>
	    			<?php endwhile; ?>
	    				</ul>
	    			<?php endif; ?>
				<?php endif; ?>
			</div>
			<div class="b-half-panel b-half-panel_right  b-half-panel_donate-form p-all m-all t-1of2 d-1of2">
				<div class="b-wysiwyg b-donation-form">
					<?php echo $donation_form; ?>
				</div>
			</div>
			
		</div>
	</div>
</section>