<?php // CURRENT ROW LAYOUT = hover-panels  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);
$section_title = get_sub_field('section_title');
?>

<?php
$hover_panel_grid = "p-all m-1of2 t-1of2 d-1of2";
$hover_panel_array = get_sub_field('Panel');

$hover_panel_count = count( $hover_panel_array );
switch ($hover_panel_count) {
    case 1:
        $hover_panel_grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $hover_panel_grid = "p-all m-1of2 t-1of2 d-1of2";
        break;
    case 3:
        $hover_panel_grid = "p-all m-1of3 t-1of3 d-1of3";
        break;
	case 4:
        $hover_panel_grid = "p-all m-1of2 t-1of4 d-1of4";
        break;
}
?>
<section <?php echo $id_tag; ?> class="b-section b-section_outer-pad <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> <?php echo $inverse_class; ?> b-section_hover-panels" <?php echo $section_bkg_style; ?>>
	
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_hover-panels cf">
		
		<div class="b-section__wrap-inner b-section__wrap-inner_hover-panels cf ">
		<?php the_sub_field('panels'); ?>
			
		<?php if( have_rows('Panel') ): ?>
		
	    <ul class="b-hover-panels b-columns">
	    <?php while( have_rows('Panel') ): the_row(); ?>
	    	<?php
			$image = get_sub_field('image');
			$headline = get_sub_field('headline');
			$text = get_sub_field('text');
			$button_label = "";
			$button_label = ro_get_button_label();
			$button_link = ro_button_link();
			$background_video_url = get_sub_field('video');
			?>
			<li class="b-hover-panel <?php echo $hover_panel_grid; ?>" onclick="void(0)">
				<div class="b-hover-panel__inner js-equal-heights_rows">
					
					<div class="b-hover-panel__cover-image" style="background: url(<?php echo $image; ?>) center center no-repeat; background-size: cover;">
						
					</div>
					
					<?php if($headline): ?> <h3 class="b-headline b-hover-panel__headline"><?php echo $headline; ?></h3><?php endif; ?>
					<div class="b-hover-panel__hidden">
						<?php if($text): ?><div class="b-wysiwyg b-hover-panel__text"><?php echo $text; ?></div><?php endif; ?>
						<?php if($button_label && $button_link): ?>
							<a class="b-hover-panel__button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
						<?php elseif( !$button_label && $button_link): ?>
							<a class="b-hover-panel__button_icon" href="<?php echo $button_link; ?>"> > </a>
						<?php endif; ?>
					</div>
				</div>
				<?php if($background_video_url != "") : ?>
					<video style="object-fit: cover;" autoplay="" loop="" preload="" poster="" class="b-hover-panel__video b-video-background"><source src="<?php echo $background_video_url; ?>" type="video/mp4"></video>
				<?php endif; ?>
				
			</li>
		<?php endwhile; ?>
	    </ul>
		<?php endif; ?>
	</div></div>
</section>
