<?php // CURRENT ROW LAYOUT = faqs  ?>
<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_faqs <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?>  <?php echo $custom_classes; ?> b-section__faqs" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_faq cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_faq cf " >
		<?php if($section_title): ?>
			<h2 class="b-headline h2 b-feature-cards__title b-section__title"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>	
		<?php if( have_rows('questions') ): ?>
		<ul class="b-faqs">
			<?php while( have_rows('questions') ) : the_row(); ?>
				<li class="b-faq">
					<div class="b-faq__question js-reveal-sibling" data-faq="<?php the_sub_field('question'); ?>">
						<span class="b-faq__question_sprite" data-faq="<?php the_sub_field('question'); ?>"></span>
						<?php the_sub_field('question'); ?>
					</div>
					<div class="b-faq__answer js-hide-load">
						<?php the_sub_field('answer'); ?>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>
		
		</div>
	</div>
</section>