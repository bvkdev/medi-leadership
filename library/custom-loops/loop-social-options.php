<?php
// check if the flexible content field has rows of data
if( have_rows('social_icons', 'options') ):
?>
	<div class="b-social-promo">
		
		<ul class="b-menu__list b-social-links">
	<?php
	 	// loop through the rows of data
	    while ( have_rows('social_icons', 'options') ) : the_row();
			$title = get_sub_field('title');
			$icon = get_sub_field('icon');
			$url = get_sub_field('url');
			?>
			<li class='b-social-link b-menu__list-item'>
				<a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
					<img class='b-social-link__image' src="<?php echo $icon; ?>" alt="<?php echo $title; ?>" />
				</a>
			</li>
			<?php
		endwhile;
	?>
		</ul>
	</div>
<?php				
	else :
	
	    // no layouts found
	
	endif;
?>