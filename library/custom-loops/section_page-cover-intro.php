<?php
// GET VALUES FOR TEXT
$should_show = get_field('include_homepage_cover_intro', 'options');
$page_cover_headline = "UNITED WAY OF THE NATIONAL CAPITAL AREA FIGHTS FOR THE HEALTH, EDUCATION AND FINANCIAL STABILITY OF EVERY PERSON IN OUR COMMUNITY.";
$page_cover_headlines = array();
if($should_show) :
	if( have_rows('page_cover_headlines', 'option') ) :
		while( have_rows('page_cover_headlines', 'option')) : the_row();
			$headline = get_sub_field('page_cover_headline');
			array_push($page_cover_headlines, $headline);
		endwhile;
		$page_cover_headline = $page_cover_headlines[array_rand($page_cover_headlines)];
	endif;
	?>
	<?php if($page_cover_headline): ?>
	<div class="b-page-cover js-page-cover">
		<div class="b-page-cover__close js-page-cover-close">X</div>
		<div class="b-section__wrap-outer_page-cover">
			<h1 class="b-headline b-headline_page-cover b-hero__headline js-page-cover_headline"><span><?php echo $page_cover_headline; ?></span></h1>
		</div>
	</div>
	<?php endif; ?>
<?php endif; ?>