<?php // CURRENT ROW LAYOUT = panel_slide_show  ?>
<?php
$content_bkg_style = bb_set_content_bkg_style();
$section_bkg_style = set_background_style();
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = inverse_text();
//$anchor_tag = anchors_away();
$id_tag = add_section_id();
$custom_classes = bb_add_section_classes();
?>
<?php
$post_slides = get_sub_field('featured_posts');
$headline = get_sub_field('headline');
?>
<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_panel-slideshow"  <?php echo $section_bkg_style; ?>>
	<div class="wrap wrap_relative cf"><div class="b-section__wrap-inner cf " >
		<?php if($headline): ?>
			<div class="b-panel-slideshow__title">
				<h2 class="b-headline b-headline_panel-slideshow"><?php the_sub_field('headline'); ?></h2>
			</div>
		<?php endif; ?>
		<?php if($post_slides) : ?>
			<ul class="b-panel-slideshow__slides js-panel-slideshow">
		    <?php foreach( $post_slides as $post): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($post); ?>
		        <li class="b-slide b-slide_panel-slideshow" data-name="<?php the_title(); ?>" data-location="<?php the_field('city_state'); ?>" data-uri="<?php echo get_permalink(); ?>">
		        	<?php 
					if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it. ?>
						<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'slide-500', array( 'class' => 'b-slide__image', 'title' => get_the_title() ) ); ?>
						</a>
					<?php
					endif;
					?>
		        </li>
		    <?php endforeach; ?>
		    </ul>
		    <?php $first_slide = $post_slides[0]->ID; ?>
		    <?php /*  
			<a class="b-slide__caption b-slide__caption_panel-slideshow" href="<?php echo get_permalink($first_slide); ?>"><?php print_r($post_slides[1]->post_title); ?> &ndash; <?php the_field('city_state', $first_slide); ?></a>
			*/ ?>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
	</div></div>
</section>