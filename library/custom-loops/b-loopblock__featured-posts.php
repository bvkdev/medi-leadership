<?php // CURRENT ROW LAYOUT = featured-post  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);

$collapse_class = ro_collapse_padding($ro_prefix);
?>


<?php
$section_layout = "";
$layout_choice = get_sub_field('layout');
if($layout_choice == 'default'):
	$section_layout = '_default';
elseif($layout_choice == 'masonry'):
	$section_layout = '_masonry';
elseif($layout_choice == 'carousel'):
	$section_layout = '_carousel';
else :
	$section_layout = '_default';
endif;
//

//
$section_title = get_sub_field('section_title');
// OPTION TO FORCE EXCLUSION OF THE EXCERPT
$include_excerpt = get_sub_field('include_excerpt');
$include_excerpt = true;
// SOMETIMES THERE'S AN OPTION TO FORCE EQUAL HEIGHTS OR TO LET THEM VARY
$equal_post_height = get_sub_field('equal_post_height');
if($equal_post_height):
	$equal_post_height_class = "js-equal-heights_rows";
else:
	$equal_post_height_class = "";
endif;
// WHAT THE 'READ MORE' LABEL AFTER THE EXCERPT SHOULD SAY
$read_more_label = get_sub_field('read_more_label');
if( $read_more_label == "") {
	$read_more_label = "Read More";
}
// TO INCLUDE A 'SEE ALL' LINK UNDER THE POSTS
$include_blog_link = get_sub_field('include_link_to_blog');

//GET POSTS BASED ON WHAT INCLUSION METHOD TO USE: AUTO OR MANUAL SELECTION
$selection_method = get_sub_field('selection_method');
if( $selection_method == "") {
	$selection_method = "manual";
}
//
$featured_posts_category = "";
$featured_post_data = "";
if( $selection_method == "manual") {
	$featured_post_data = get_sub_field('featured_posts');
} elseif( $selection_method == "automatic") {
	$featured_posts_categories = get_sub_field('featured_posts_category');
	//var_dump($featured_posts_categories);
	//TEMPORARILY CREATE NEW QUERY OBJECT
	$type = 'post';
	$args=array(
		'post_type' => $type,
		'post_status' => 'publish',
		'posts_per_page' => 4,
		'order' =>'DSC',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $featured_posts_categories,
			)
		)
	);
	$my_query = null;
	$my_query = new WP_Query($args);
	$featured_post_data = $my_query->posts;
}
$post_counter = 0;
//
$featured_posts_categories_slugs = array();
if( isset($featured_posts_categories) ):
	foreach ($featured_posts_categories as $category) {
		$featured_posts_categories_slugs[] = get_cat_slug($category);
	}
	//var_dump($featured_posts_categories_slugs);
endif;
?>

<?php if( $featured_post_data ) : ?>
	
	<?php //SET NEWS CATEGORIES SO WE CAN CHECK AGAINST THEM
	$news_categories = array('news', 'press-release');
	if( !in_array($news_categories[0], $featured_posts_categories_slugs) && !in_array($news_categories[1], $featured_posts_categories_slugs) ) :
	?>
		<?php //IF NOT THE NEWS OR PRESS CATEGORIES, USE THIS LAYOUT ?>
		<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section-featured-posts " <?php echo $section_bkg_style; ?>>
			<div class="wrap b-section__wrap-outer b-section__wrap-outer_featured-posts cf">
				<div class="b-section__wrap-inner b-section__wrap-inner_featured-posts cf">
					<?php if($section_title): ?>
						<h2 class="b-headline h2 b-featured-posts__title b-section__title"><span><?php echo $section_title; ?></span></h2>
					<?php endif; ?>
		
					<div class="b-featured-posts">
						<ul class="b-featured-posts__wrap b-featured-posts__wrap<?php echo $section_layout; ?> js-featured-posts<?php echo $section_layout; ?> js-featured-posts_carousel">
						<?php foreach ($featured_post_data as $post) : ?>
						<?php
						$featured_post_title = $featured_post_data[$post_counter]->post_title;
						$featured_post_excerpt = wp_trim_words($featured_post_data[$post_counter]->post_content, 25);
						$featured_post_id = $featured_post_data[$post_counter]->ID;
						$featured_post_url = get_permalink( $featured_post_id );
						$featured_post_image = get_the_post_thumbnail( $featured_post_id, 'full', array( 'class' => 'b-featured-post__thumbnail', 'alt' => $featured_post_title ) );
						$post_counter++;
						?>
						
						
						<li class="b-featured-post b-featured-post<?php echo $section_layout; ?> <?php echo $equal_post_height_class; ?>">
							<div class="b-featured-post__inner">
								
								<div class="b-resp-image b-resp-image_featured-post b-resp-image_featured-post<?php echo $section_layout; ?>">
									<?php if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it. ?>
										
										<?php echo $featured_post_image; ?>
									<?php endif; ?>
								</div>
								
								<div class="b-featured-post__content b-featured-post__content<?php echo $section_layout; ?> js-equal-heights_rows">
									<?php if($featured_post_title): ?><h3 class="b-featured-post__headline b-featured-post__headline<?php echo $section_layout; ?>"><?php echo $featured_post_title; ?></h3><?php endif; ?>
									<?php if($include_excerpt): ?><?php if($featured_post_excerpt): ?> <p class="b-wysiwyg b-featured-post__text b-featured-post__text<?php echo $section_layout; ?>"><?php echo $featured_post_excerpt; ?></p><?php endif; ?><?php endif; ?>
									<a class="b-button_flat b-featured-post__button b-featured-post__button<?php echo $section_layout; ?>" href="<?php echo $featured_post_url; ?>"><?php echo $read_more_label; ?></a>
								</div>
							</div>
						</li>
							
						<?php endforeach; ?>
						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						</ul>
						<?php
						$related_posts_link = get_permalink( get_option('page_for_posts') );
						if( $selection_method == "manual") {
							$related_posts_link = get_permalink( get_option('page_for_posts') );
							$related_stories_label = "View All Stories";
						} elseif( $selection_method == "automatic") {
							$related_posts_link = get_term_link( $featured_posts_categories[0] );
							$related_stories_label = "View All Related Stories";
						}
						?>
						<p class="b-featured-post__bloglink"><a href="<?php echo $related_posts_link; ?>"><?php echo $related_stories_label; ?></a></p>
					</div>
				</div>
			</div>
		</section>
		
	<?php else : ?>
		
	<?php //IF IN THE NEWS OR PRESS CATEGORIES, WE'LL HAVE A DIFFERENT LAYOUT ?>
		<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section-featured-news" <?php echo $section_bkg_style; ?>>
		<div class="wrap b-section__wrap-outer b-section__wrap-outer_featured-news cf">
			<div class="b-section__wrap-inner b-section__wrap-inner_featured-news cf">
				<?php if($section_title): ?>
					<h2 class="b-headline h2 b-featured-news__title b-section__title"><span><?php echo $section_title; ?></span></h2>
				<?php endif; ?>
	
				<div class="b-featured-news-items">
					<ul class="b-featured-news-items__wrap b-columns">
					<?php foreach ($featured_post_data as $post) : ?>
					<?php
					$featured_post_title = $featured_post_data[$post_counter]->post_title;
					$featured_post_excerpt = wp_trim_words($featured_post_data[$post_counter]->post_content, 25);
					$featured_post_id = $featured_post_data[$post_counter]->ID;
					$featured_post_url = get_permalink( $featured_post_id );
					$featured_post_image = get_the_post_thumbnail( $featured_post_id, 'full', array( 'class' => 'b-featured-post__thumbnail', 'alt' => $featured_post_title ) );
					$featured_post_terms = get_the_terms( $post->ID, 'category' );
						$post_cats_list = "";
						//print_r($terms);
						if ( $featured_post_terms && ! is_wp_error( $featured_post_terms ) ) : 
							$post_cats = array();
							foreach ( $featured_post_terms as $term ) :
								if($term->slug == 'news' || $term->slug == 'press-release') {
									$post_cats[] = $term->slug;
								}
							endforeach;
							$post_cats_list = join( " ", $post_cats );
							$post_cats_list_nice = ucwords( str_replace("-", " ", $post_cats_list) );
						endif;
					$post_counter++;
					?>
					
					
					<li class="b-featured-news <?php echo $post_cats_list; ?> <?php echo $equal_post_height_class; ?> p-all m-all t-all d-1of4">
						<div class="b-featured-news__inner">
							<span class="b-featured-news__icon <?php echo $post_cats_list; ?>"></span>
							<div class="b-featured-news__content js-equal-heights_rows">
								<span class="b-featured-news__meta"><?php echo $post_cats_list_nice; ?></span>
								<?php if($featured_post_title): ?><h3 class="b-featured-news__headline"><?php echo $featured_post_title; ?></h3><?php endif; ?>
								<?php /* ?>
								<?php if($include_excerpt): ?>
									<?php if($featured_post_excerpt): ?><p class="b-wysiwyg b-featured-news__text "><?php echo $featured_post_excerpt; ?></p><?php endif; ?>
								<?php endif; ?>
								<?php */ ?>
								
								<a class="b-button_flat b-button_small b-featured-news__button" href="<?php echo $featured_post_url; ?>"><?php echo $read_more_label; ?></a>
							</div>
						</div>
					</li>
						
					<?php endforeach; ?>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					</ul>
					<?php
					$related_posts_link = get_permalink( get_option('page_for_posts') );
					if( $selection_method == "manual") {
						$related_posts_link = get_permalink( get_option('page_for_posts') );
						$related_stories_label = "View All Stories";
					} elseif( $selection_method == "automatic") {
						$related_posts_link = get_term_link( $featured_posts_categories[0] );
						$related_stories_label = "View All News";
					}
					?>
					<p class="b-featured-news__bloglink"><a href="<?php echo $related_posts_link; ?>"><?php echo $related_stories_label; ?></a></p>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
<?php endif; ?>