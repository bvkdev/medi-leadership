<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
// Get values for page intro
$intro_headline = get_sub_field('headline');
$intro_text = get_sub_field('intro');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?> b-section_bodytext <?php echo $custom_classes; ?>">
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_bodytext cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_bodytext cf">
			<?php if($intro_headline): ?> <h2 class="h3 b-bodytext__headline b-intro__headline_bodytext"><?php echo $intro_headline; ?></h2><?php endif; ?>
			<?php if($intro_text): ?> <div class="b-bodytext__text b-wysiwyg js_p_grid b-columns" ><?php echo $intro_text; ?></div><?php endif; ?>
		</div>
	</div>
</section>