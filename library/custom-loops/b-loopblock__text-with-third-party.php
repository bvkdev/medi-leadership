<?php // CURRENT ROW LAYOUT = text_with_social_feed  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
?>

<?php
$third_party_script = get_sub_field('3rd_party_script');
$script_side = get_sub_field('feed_align');
$script_align = "align".$script_side;
$text_column_classes = "p-all m-all t-1of2 d-1of2";
$image_column_classes = "p-all m-all t-1of2 d-1of2";
//
$headline = get_sub_field('headline');
$subheadline = get_sub_field('subheadline');
$intro = get_sub_field('intro');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_text-script" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_text-script cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_text-script cf " >
			<div class="b-text-script__content b-columns">
			
				<div class="b-text-script__copy b-text-script__copy_<?php echo $script_side; ?> <?php echo $text_column_classes; ?>">
					<?php if($headline): ?><h2 class="h2 b-headline b-text-script__headline"><span><?php echo $headline; ?></span></h2><?php endif; ?>
					<?php if($subheadline): ?><p class="h3 b-text-script__subheadline"><?php echo $subheadline; ?></p><?php endif; ?>
					<?php if($intro): ?> <div class="b-wysiwyg b-text-script__text"><?php echo $intro; ?></div><?php endif; ?>
				
					<?php if( $section_button_link ) : ?>
				    	<a class="b-button_flat b-text-script__button" href="<?php echo $section_button_link; ?>"><?php the_sub_field('button_text'); ?></a>
				    <?php endif; ?>
				</div>
				
				<div class="b-text-script__script b-text-script__script_<?php echo $script_side; ?> <?php echo $image_column_classes; ?>">
					<div class="b-third-party-script">
						<?php echo $third_party_script; ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>