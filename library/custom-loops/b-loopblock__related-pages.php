<?php // CURRENT ROW LAYOUT = feature-cards  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);

$section_title = get_sub_field('section_title');
$include_excerpt = get_sub_field('include_excerpt');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> <?php echo $inverse_class; ?> b-section_relpages" <?php echo $section_bkg_style; ?>>
	
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_relpages cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_relpages cf ">
			<?php if($section_title): ?>
				<h2 class="b-headline h2 b-section__title b-section__title_relpages"><span><?php echo $section_title; ?></span></h2>
			<?php endif; ?>
			<?php
			$relpage_grid = "p-all m-1of2 t-1of2 d-1of2";
			$relpage_array = get_sub_field('related_pages');
			
			$relpage_count = count( $relpage_array );
			switch ($relpage_count) {
			    case 1:
			        $relpage_grid = "p-all m-all t-all d-all";
			        break;
			    case 2:
			        $relpage_grid = "p-all m-1of2 t-1of2 d-1of2";
			        break;
			    case 3:
			        $relpage_grid = "p-all m-1of3 t-1of3 d-1of3";
			        break;
				case 4:
			        $relpage_grid = "p-all m-1of2 t-1of4 d-1of4";
			        break;
			}
			?>
			<?php if( have_rows('related_pages') ): ?>
			
		    <ul class="b-relpages b-columns">
		    <?php while( have_rows('related_pages') ): the_row(); ?>
		    	<?php
		    	$post = get_sub_field('related_page');
				setup_postdata( $post );
				//get values from post we're referencing
				$headline = get_the_title();
				$text = excerpt(18);
				$image = get_the_post_thumbnail_url(get_the_ID(),'full');
				$page_url = get_permalink();
				//reset post data to the page's flexible content blocks
				wp_reset_postdata();
				
				//override any values as necessary
				$headline_override = get_sub_field('title');
				if($headline_override != ""){ $headline = $headline_override; }

				$text_override = get_sub_field('text');
				if($text_override != ""){ $text = $text_override; }
				
				$image_override = get_sub_field('image');
				if($image_override != ""){ $image = $image_override; }
				?>
				<li class="b-relpage <?php echo $relpage_grid; ?>">
					<div class="b-relpage__inner">
						<?php if($page_url): ?><a href="<?php echo $page_url; ?>" title="<?php echo $headline; ?>"><?php endif; ?>
						<?php if($image): ?><div class="b-resp-image b-resp-image_relpage">
												<img class="b-image_relpage" alt="<?php echo $headline; ?>" src="<?php echo $image; ?>" />
											</div>
						<?php endif; ?>
						<?php if($page_url): ?></a><?php endif; ?>

						<?php if($headline): ?> <h3 class="h2 b-headline b-relpage__headline"><span><?php echo $headline; ?></span></h3><?php endif; ?>
						<?php if($include_excerpt) : ?>
						<?php if($text): ?> <p class="b-relpage__text"><?php echo $text; ?></p><?php endif; ?>
						<?php endif; ?>
					</div>
				</li>
			<?php endwhile; ?>
		    </ul>
			<?php endif; ?>
		</div>
	</div>
</section>
