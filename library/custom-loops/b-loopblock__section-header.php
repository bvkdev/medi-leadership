<?php // CURRENT ROW LAYOUT = section_header  ?>
<?php
$content_bkg_style = bb_set_content_bkg_style();
$section_bkg_style = set_background_style();
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = inverse_text();
//$anchor_tag = anchors_away();
$id_tag = add_section_id();
$custom_classes = bb_add_section_classes();
?>
<?php
$headline = get_sub_field('headline');
$subheadline = get_sub_field('subheadline');
$intro = get_sub_field('intro');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section__section-header" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer cf"><div class="b-section__wrap-inner cf " >
		<?php if($headline): ?><h2 class="b-headline b-headline__section-header <?php echo $inverse_class; ?>"><?php the_sub_field('headline'); ?></h2><?php endif; ?>
		<?php if($subheadline): ?><h3 class="b-subhead b-subhead__section-header <?php echo $inverse_class; ?>"><?php the_sub_field('subheadline'); ?></h3><?php endif; ?>
		<?php if($intro): ?><div class="b-wysiwyg b-wysiwyg__section-header <?php echo $inverse_class; ?>"><?php the_sub_field('intro'); ?></div><?php endif; ?>
	</div></div>
</section>