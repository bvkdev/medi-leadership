<?php

//
$page_title = get_the_title();
$section_title = $page_title." Stories";
$read_more_label = "Read More";
$page_slug = $post->post_name;

//set up query object
//
$type = 'post';
$args=array(
	'post_type' => $type,
	'post_status' => 'publish',
	'posts_per_page' => 4,
	'order' =>'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => $page_slug,
		)
	)
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {

?>
<section class="b-section  b-section-featured-posts " >
	<div class="wrap b-section__wrap-outer cf">
		<div class="b-section__wrap-inner cf">
			<?php if($section_title): ?>
				<h2 class="b-headline h2 b-featured-posts__title b-section__title"><span><?php echo $section_title; ?></span></h2>
			<?php endif; ?>
			<div class="b-featured-posts">
				<ul class="b-featured-posts__wrap js-featured-posts js-featured-posts_carousel">

<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<?php
				$featured_post_title = get_the_title();
				$featured_post_excerpt = excerpt(10);
				$featured_post_id = get_the_ID();
				$featured_post_url = get_permalink( $featured_post_id );
				$featured_post_image = get_the_post_thumbnail( $featured_post_id, 'full', array( 'class' => 'b-featured-post__thumbnail', 'alt' => $featured_post_title ) );
				?>

  					<li class="b-featured-post">
						<div class="b-featured-post__inner">
							
							<div class="b-resp-image b-resp-image_featured-post">
								<?php if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it. ?>
									
									<?php echo $featured_post_image; ?>
								<?php endif; ?>
							</div>
							
							<div class="b-featured-post__content js-equal-heights_rows">
								<?php if($featured_post_title): ?><h3 class="b-featured-post__headline"><?php echo $featured_post_title; ?></h3><?php endif; ?>
								<?php if($featured_post_excerpt): ?> <p class="b-wysiwyg b-featured-post__text"><?php echo $featured_post_excerpt; ?></p><?php endif; ?>
								<a class="b-button_flat b-featured-post__button" href="<?php echo $featured_post_url; ?>"><?php echo $read_more_label; ?></a>
							</div>
						</div>
					</li>

<?php endwhile; ?>
  				</ul>
  				<?php
				$related_posts_link = get_term_link( $page_slug, $taxonomy = 'category' );
				$related_stories_label = "View All Related Stories";
				//echo $related_posts_link;
				?>
  				<p class="b-featured-post__bloglink"><a href="<?php echo $related_posts_link; ?>"><?php echo $related_stories_label; ?></a></p>

			</div>
		</div>
	</div>
</section>

<?php }
wp_reset_query();  // Restore global post data stomped by the_post().
?>