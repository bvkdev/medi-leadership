<?php
// Get values for panel groups
$money_panel = get_field('money');
$money_panel_a = $money_panel['sentence_1'];
$money_panel_b = $money_panel['sentence_2'];
$money_panel_button_label = $money_panel['button_text'];
$money_panel_button_label = $money_panel['button_text'];
$ro_prefix = 'money_';
$money_panel_button_label = ro_get_button_label($ro_prefix);
//for some reason, the function is returning the page ID when pulling it from a group, such as 'money' in this instance.
$money_panel_button_link = ro_button_link($ro_prefix);
$money_panel_button_link = get_the_permalink($money_panel_button_link);
//
$people_panel = get_field('people');
$people_panel_title = $people_panel['panel_title'];
//print_r($people_panel);
$people_panel_stats = $people_panel['statistics'];
//print_r($people_panel_stats);
$ro_prefix = 'people_';
$people_panel_button_label = ro_get_button_label($ro_prefix);
//for some reason, the function is returning the page ID when pulling it from a group, such as 'people' in this instance.
$people_panel_button_link = ro_button_link($ro_prefix);
$people_panel_button_link = get_the_permalink($people_panel_button_link);
?>

<section class="b-section b-section_money-people">
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_money-people cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_money-people b-columns">
			<div class="b-half-panel b-half-panel_left  b-money-panel js-equal-heights_rows p-all m-all t-1of2 d-1of2">
				<?php if( $money_panel_a ) : ?>
					<p class="b-money-panel__sentence"><?php echo $money_panel_a; ?></p>
			    <?php endif; ?>
			    
			    <?php if( $money_panel_b ) : ?>
			    	<p class="b-money-panel__sentence"><?php echo $money_panel_b; ?></p>
			    <?php endif; ?>
			    
				<?php if( $money_panel_button_link ) : ?>
			    	<a class="b-button_flat b-half-panel__button" href="<?php echo $money_panel_button_link; ?>"><?php echo $money_panel_button_label; ?></a>
			    <?php endif; ?>
			</div>
			<div class="b-half-panel b-half-panel_right b-people-panel js-equal-heights_rows p-all m-all t-1of2 d-1of2">
				<?php if($people_panel_title): echo "<h2 class='b-headline h2 b-people-panel__headline'><span>".$people_panel_title."</span></h2>"; endif; ?>
					<?php if( $people_panel_stats ): ?>
						<ul class="b-people-stats js_li_grid b-columns" data-organizer="b-people-stat">
		    			<?php foreach($people_panel_stats as $stat): ?>
							<li class="b-people-stat">
								<span class="b-people-stat__value"><?php echo $stat['stat_value']; ?></span>
								<span class="b-people-stat__label"><?php echo $stat['stat_label']; ?></span>
							</li>
		    			<?php endforeach; ?>
	    				</ul>
	    			<?php endif; ?>
	    			<?php if( $people_panel_button_link ) : ?>
				    	<a class="b-button_flat b-half-panel__button" href="<?php echo $people_panel_button_link; ?>"><?php echo $people_panel_button_label; ?></a>
				    <?php endif; ?>
			</div>
			
		</div>
	</div>
</section>