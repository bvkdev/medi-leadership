<?php // CURRENT ROW LAYOUT = cta_over_video  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
// Get remaining values
$headline = get_sub_field('headline');
$intro = get_sub_field('subheadline');
$background_video_url = get_sub_field('background_video');
?>



<section <?php echo $id_tag; ?>  class="b-section <?php echo $collapse_class; ?> b-section_outer-pad <?php echo $custom_classes; ?> b-section_cta-video b-cta-video" <?php echo $section_bkg_style; ?>>
	<?php if($background_video_url != "") : ?>
		<video style="object-fit: cover;" autoplay="" loop="" preload="" poster="" class="b-video-background"><source src="<?php echo $background_video_url; ?>" type="video/mp4"></video>
	<?php endif; ?>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_cta-video cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_cta-video cf" >
			<div class="b-cta-bar">
				<h2 class="h2 b-headline b-cta-bar__title"><span><?php echo($headline); ?></span></h2>
				<?php if($intro): ?> <p class="b-wysiwyg b-cta-bar__text"><?php echo $intro; ?></p><?php endif; ?>
				<?php if( $section_button_link ) : ?>
			    	<a class="b-button_flat b-cta-bar__button" href="<?php echo $section_button_link; ?>"><?php echo $button_label; ?></a>
			    <?php endif; ?>
		    </div>
		</div>
	</div>
</section>
