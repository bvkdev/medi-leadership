<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
// Get values for page intro
$headline = get_sub_field('headline');
$button_label = "";
$button_label = ro_get_button_label();
$button_link = ro_button_link();
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?> b-section_intro <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_intro cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_intro cf">
			<?php if($headline): ?> <h2 class="b-intro__headline"><?php echo $headline; ?></h2><?php endif; ?>
			<?php if( $button_link ) : ?>
		    	<a class="b-button_flat b-section-textphoto__button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
		    <?php endif; ?>
		</div>
	</div>
</section>