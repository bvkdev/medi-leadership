<?php // CURRENT ROW LAYOUT = text_over_photo  ?>
<?php
// Get values via common-functions
$ro_prefix = 'community_goals_';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text($ro_prefix);
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();

$collapse_class = ro_collapse_padding($ro_prefix);

?>
<?php
$headline = get_field($ro_prefix.'headline');
$intro = get_field($ro_prefix.'intro');
?>
<?php
$callout_grid = "p-all m-1of2 t-1of2 d-1of2";
$callout_array = get_field($ro_prefix.'icon_callouts');

$callout_count = count( $callout_array );
switch ($callout_count) {
    case 1:
        $callout_grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $callout_grid = "p-all m-1of2 t-1of2 d-1of2";
        break;
    case 3:
        $callout_grid = "p-all m-1of3 t-1of3 d-1of3";
        break;
	case 4:
        $callout_grid = "p-all m-1of2 t-1of4 d-1of4";
        break;
}
?>


<section <?php echo $id_tag; ?>  class="b-section b-section_outer-pad  <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?> b-section-icon-callouts " <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_icon-callouts cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_icon-callouts cf" >
			<?php if($headline): ?> <h2 class="h2 b-headline b-section-icon-callouts__title"><span><?php echo $headline; ?></span></h2><?php endif; ?>
			<?php if($intro): ?> <div class="b-wysiwyg b-section-icon-callouts__text"><?php echo $intro; ?></div><?php endif; ?>
			
			<?php if( have_rows($ro_prefix.'icon_callouts') ): ?>
		
		    <ul class="b-icon-callouts b-columns">
		    <?php while( have_rows($ro_prefix.'icon_callouts') ): the_row(); ?>
		    	<?php
					$image = get_sub_field('icon');
					$text = get_sub_field('headline');
					$button_label = ro_get_button_label();
					$button_link = ro_button_link();
				?>
		    	<li class="b-icon-callout  <?php echo $callout_grid; ?>">
		    		<div class="b-icon-callout__inner js-equal-heights_rows">
					
					<?php if($image): ?>
						<div class="b-resp-image b-icon-callout__icon">
							<img class="b-icon-callout__image" alt="<?php echo $headline; ?>" src="<?php echo $image; ?>" />
						</div>
						<?php endif; ?>
						
					<?php if($text): ?>
						<p class="b-icon-callout__text"><?php echo $text; ?></p>
					<?php endif; ?>
					
					<?php if($button_link && $button_label): ?>
						<a class="b-icon-callout__button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
					<?php endif; ?>	
					</div>
		    	</li>
	    	<?php endwhile; ?>
	    	<?php endif; ?>
			</ul>
		</div>
	</div>
</section>
