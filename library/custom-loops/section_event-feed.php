<?php

//
//$page_title = get_the_title();
$page_title = "";
$section_title = $page_title."Volunteer Opportunities & Events";
$read_more_label = "Read More";
$page_slug = $post->post_name;
$category_slug = $page_slug;
if($page_slug == 'volunteer') {
	$category_slug = 'all';
};

//set up query object
//
$today = date('Ymd');
$type = 'type_event';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => 3,
  'meta_query' => array(
  		'relation' => 'OR',
	  	//is it on-going?
		'ongoing_dates' => array(
			'ongoing_date_value' => array(
					'key' => 'ongoing_status',
					'value' => true,
					'compare' => '==',
					),
		),
		// Or does it have an end date that is today or later
	  	'has_end_date' => array(
	  		'relation' => 'AND',
			'end_date_value' => array(
					'key' => 'end_date',
					'value' => $today,
					'compare' => '>=',
					),
			'end_date_value_2' => array(
					'key' => 'end_date',
					'value' => "",
					'compare' => '!=',
					)
		),
		// or does it have only a start date that is today or later
		'only_start_date' => array(
			'start_date_value' => array(
					'key' => 'start_date',
					'value' => $today,
					'compare' => '>=',
					),
		)
	    
    ),
  'meta_key' => 'start_date',
  'orderby' => 'meta_value',
  'type' => 'DATE',
  'order' =>'ASC',
  'tax_query' => array(
        array(
            'taxonomy' => 'event_categories',
            'field' => 'slug',
            'terms' => $page_slug,
        )
    )
  );
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {

?>
<section class="b-section b-section-featured-events b-section-featured-events_pillar " >
	<div class="wrap b-section__wrap-outer wrap b-section__wrap-outer_featured-events b-section__wrap-outer_featured-events_pillar cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_featured-events b-section__wrap-inner_featured-events_pillar cf">
			<?php if($section_title): ?>
				<h2 class="b-headline h2 b-featured-events__title b-featured-events__title_pillar b-section__title"><span><?php echo $section_title; ?></span></h2>
			<?php endif; ?>
			<div class="b-featured-events b-featured-events_pillar">
				<ul class="b-featured-events__wrap b-featured-events__wrap_pillar b-columns">

<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
	<?php get_template_part('library/custom-loops/b-single__event'); ?>
<?php endwhile; ?>
  				</ul>
  				<?php
		    	$related_posts_link = get_permalink( get_page_by_title( 'Events' ) );
			?>
				<p class="b-featured-event__bloglink"><a href="<?php echo $related_posts_link; ?>">See More Events</a></p>
			</div>
		</div>
	</div>
</section>

<?php }
wp_reset_query();  // Restore global post data stomped by the_post().
?>
