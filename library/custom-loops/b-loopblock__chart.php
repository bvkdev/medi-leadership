<?php

// Get the data here, so it can be used in IF Statements below in case a field isn't needed.
$section_headline = get_sub_field('headline');
$section_sub_headline = get_sub_field('sub_headline');
$section_content = get_sub_field('content');
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
// Get the total number outside of the if statement. It doesn't work otherwise.
$total_number = count(get_sub_field('chart_data'));
// Create a space-stripped version of the headline to use as a unique ID for the jQuery charts.
$my_id = str_replace(' ', '', get_sub_field('headline') );
$my_id = "charty";
$my_id = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
$my_id = "$my_id";
?>

<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
// Get values for page intro
$section_title = get_sub_field('section_title');
$section_headline = get_sub_field('headline');
$section_subheadline = get_sub_field('subheadline');
$section_content = get_sub_field('intro');
$chart_side = "_".get_sub_field('chart_side');

?>

<section <?php echo $id_tag; ?> class="b-section b-section_chart <?php echo $custom_classes; ?> <?php if($section_title): ?>b-section_tucked-title<?php endif; ?>" <?php echo $section_bkg_style; ?>>
	
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_chart cf">
		
		<div class="b-section__wrap-inner b-section__wrap-inner_chart cf">
		<?php if($section_title): ?>
			<h2 class="b-headline h2 b-section__title b-section__title_chart"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>
		
		<div class="b-chart-contents b-columns">
		<?php if( ! ($section_headline == "" && $section_subheadline == "" && $section_content == "")): ?>
		<div class="b-chartblock__text-content">	
			<?php if( $section_headline != ""): ?>
				<h2 class="h2 b-chartblock__headline"><?php echo $section_headline; ?></h2>
			<?php endif; ?>
			<?php if( $section_subheadline != ""): ?>
				<p class="h3 b-subhead b-chartblock__subhead"><?php echo $section_subheadline; ?></p>
			<?php endif; ?>
			<?php if( $section_content != ""): ?>
				<div class="b-wysiwyg b-wysiwyg_chart b-chartblock__text"><?php echo $section_content; ?></div>
			<?php endif; ?>
			<?php if( $section_button_link ) : ?>
		    	<a class="b-button_flat b-chartblock__button" href="<?php echo $section_button_link; ?>"><?php echo $button_label; ?></a>
		    <?php endif; ?>
		</div>
		<?php endif; ?>
		
		<div class="b-chartblock__chart b-chartblock__chart<?php echo $chart_side; ?> b-chart">
			<?php
			$chart_title = get_sub_field('chart_label');
			$chart_title_data = "";
			  if($chart_title) {
			  		
			  	$chart_title_data = "var ".$my_id."_title = ".json_encode( array("display"=> "true","text"=> "'$chart_title'","position"=>"bottom") )."";
			  	
				  //print_r( $chart_js_data);
			  }else {
			  	$chart_title_data = "var ".$my_id."_title = ".json_encode( array("display"=> "false") )."";
			  }
			  ?>
			<?php //Create a data boject for the jQuery chart plugin, using the data point repeater field. ?>
			<?php if( have_rows('chart_data') ): ?>
				<?php $js_data_open = "<script type='text/javascript'> var ".$my_id." = {"; //opening of the data object.
					  $js_data_close = "</script>"; //closing of the data object.
					  $label_obj = array();
					  $data_obj = array();
					  $bkg_color_obj = array();  
				?>
				<?php while( have_rows('chart_data') ) : the_row();
						//add values into appropriate PHP arrays
						array_push($label_obj, get_sub_field('label'));
						array_push($data_obj, get_sub_field('value'));
						array_push($bkg_color_obj, get_sub_field('color'));
				      endwhile;
					  //build text variable that makes up the echoed js script
					  $current_data="labels: ".json_encode($label_obj).",";
					  $current_data = $current_data."datasets: [{";
					  $current_data = $current_data."data: ".json_encode($data_obj).",";
					  $current_data = $current_data."backgroundColor: ".json_encode($bkg_color_obj).",";
					  $current_data = $current_data."borderWidth: 0";
					  $current_data = $current_data."}]";
					  //print_r(json_encode($label_obj));
					  //print_r(json_encode($data_obj));
					  //print_r(json_encode($bkg_color_obj));
					  $new_js_data = $current_data;
					 // print_r($current_data);
				?>
				<?php //Declare canvas for jQuery charts to use. The width and heigth can be altered. See docs for jQuery plugin: http://www.chartjs.org/docs ?>
				<canvas id="<?php echo $my_id; ?>" class="js-chart" width="400" height="400"></canvas> 
				<?php //build javascript tags and data object ?>
				<?php echo $js_data_open; ?>
				<?php echo $new_js_data; ?>
				<?php echo "};"; ?>
				<?php echo $chart_title_data; ?>
				<?php echo $js_data_close; ?>
				
				<ul class="b-chart-key">
				<?php while( have_rows('chart_data') ) : the_row(); ?>
					<li class="b-chart-key__datum">
						<span class="b-datum__color" style="background-color: <?php the_sub_field('color'); ?>"></span>
						<span class="b-datum__label"><?php the_sub_field('label'); ?></span>
						<span class="b-datum__value"><?php the_sub_field('value'); ?></span>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div><?php /*END CHARTBLOCK__CHART*/?>
		
		</div>
		</div>
	</div>
</section>