<?php

//
$page_title = get_the_title();
$section_title = "";
$read_more_label = "Read More";
$page_slug = $post->post_name;

//set up query object
//
$type = 'type_program';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => 4,
  'order' =>'ASC',
  'tax_query' => array(
        array(
            'taxonomy' => 'program_categories',
            'field' => 'slug',
            'terms' => $page_slug,
        )
    )
  );
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {

?>
<section class="b-section b-section-featured-programs b-section-featured-programs_pillar " >
	<div class="wrap b-section__wrap-outer wrap b-section__wrap-outer_featured-programs b-section__wrap-outer_featured-programs_pillar cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_featured-programs b-section__wrap-inner_featured-programs_pillar cf">
			<?php if($section_title): ?>
				<h2 class="b-headline h2 b-featured-programs__title b-featured-programs__title_pillar b-section__title b-section__title_tucked-bottom"><span><?php echo $section_title; ?></span></h2>
			<?php endif; ?>
			<div class="b-featured-programs b-featured-programs_pillar">
				<ul class="b-featured-programs__wrap b-featured-programs__wrap_pillar b-columns">
					<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
						<?php get_template_part('library/custom-loops/b-single__program'); ?>
					<?php endwhile; ?>
  				</ul>
  				
			</div>
		</div>
	</div>
</section>

<?php }
wp_reset_query();  // Restore global post data stomped by the_post().
?>
