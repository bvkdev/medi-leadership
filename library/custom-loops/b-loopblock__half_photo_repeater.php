<?php // CURRENT ROW LAYOUT = HALF PHOTO REPEATER  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);

$collapse_class = ro_collapse_padding($ro_prefix);
$section_button_link = ro_button_link();
$button_label = get_sub_field('button_text');
?>


<?php
$grid = "";
$section_title = get_sub_field('section_title');
?>
<section <?php echo $id_tag; ?> class="b-section b-section_outer-pad b-section_halves b-half-photos <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_halves cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_halves cf">
			<?php if($section_title): ?>
				<h2 class="b-headline h2 b-half-photos__title b-section__title"><span><?php echo $section_title; ?></span></h2>
			<?php endif; ?>	
			<?php if( have_rows('blocks') ): ?>
			
		    <ul class="b-half-tiles">
		    <?php $tile_count = 1; ?>
		    <?php while( have_rows('blocks') ): the_row(); ?>
		    	<?php
				$image = get_sub_field('image');
				$image_url = $image["url"];
				$headline = get_sub_field('headline');
				$subhead = get_sub_field('subheadline');
				$text = get_sub_field('intro');
				$button_label = ro_get_button_label();
				$button_link = ro_button_link();
				
				?>
				<li class="b-half-tile">
					<div class="b-half-tile__photo js-equal-heights_rows">
						
						<div class="b-halfphoto__cover-image" style="background: url(<?php echo $image_url; ?>) center center no-repeat; background-size: cover;"><?php if($button_link && !$button_label): ?><a href="<?php echo $button_link; ?>"></a><?php endif; ?></div>
					</div>
					<div class="b-half-tile__inner  js-equal-heights_rows">
						<?php if($headline): ?> <h3 class="b-half-tile__headline"><?php echo $headline; ?></h3><?php endif; ?>
						<?php if($subhead): ?> <p class="b-subhead b-half-tile__subhead"><?php echo $subhead; ?></p><?php endif; ?>
						<?php if($text): ?> <div class="b-wysiwyg b-half-tile__text"><?php echo $text; ?></div><?php endif; ?>
						<?php if($button_label && $button_link): ?>
							<a class="b-button_flat b-half-tile__button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
						<?php endif; ?>
					</div>
					
				</li>
				<?php $tile_count++; ?>
			<?php endwhile; ?>
		    </ul>
			<?php endif; ?>
		</div>
	</div>
</section>
