<?php // CURRENT ROW LAYOUT = side-image-text-area  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
?>

<?php
$image_url = get_sub_field('image');
$image_align = get_sub_field('image_align'); //defaults to right
$text_side = 'b-text-side_left';
$image_side = "b-image-side_".$image_align;
if( $image_align == "left" ) :
	$text_side = "b-text-side_right";
endif;
//
$text_column_classes = "p-all m-all t-1of3 d-1of3";
$image_column_classes = "p-all m-all t-2of3 d-2of3";
$favor_text = get_sub_field('favor_the_text');
if($favor_text) :
	$text_column_classes = "p-all m-all t-2of3 d-2of3";
	$image_column_classes = "p-all m-all t-1of3 d-1of3";
endif;
//
$headline = get_sub_field('headline');
$subheadline = get_sub_field('subheadline');
$intro = get_sub_field('intro');
$images = get_sub_field('images');
$image_count = sizeof($images);
$column_amount = 2;
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_text-side-image" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_text-side-image cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_text-side-image cf " >
		
		<div class="b-text-side-image__content b-columns">
			
			<?php if( have_rows('images') ): ?>
				<?php $image_num = 1; ?>
			<div class="b-side-photos b-side-photos_<?php echo $image_count; ?> <?php echo $image_side; ?> <?php echo $image_column_classes; ?>">
			    <?php while( have_rows('images') ): the_row();
			    	$image_url = get_sub_field('image');
					$background_style = "style='background: url(".$image_url.") center center no-repeat; background-size: cover;'";
			 	?>
			 		<div class="b-resp-image b-side-photo" <?php echo $background_style; ?>>
			    	</div>
			    	<?php $image_num++; ?>
			    <?php endwhile; ?>
			</div>
			<?php endif; ?>
			
			<div class="b-side-text <?php echo $text_side; ?> <?php echo $text_column_classes; ?>">
				<?php if($headline): ?><h2 class="h2 b-headline b-side-text__headline"><span><?php echo $headline; ?></span></h2><?php endif; ?>
				<?php if($subheadline): ?><p class="h3 b-side-text__subheadline"><?php echo $subheadline; ?></p><?php endif; ?>
				<?php if($intro): ?> <div class="b-wysiwyg b-side-text__text"><?php echo $intro; ?></div><?php endif; ?>
			
				<?php if( $section_button_link ) : ?>
			    	<a class="b-button_flat b-side-text_button" href="<?php echo $section_button_link; ?>"><?php the_sub_field('button_text'); ?></a>
			    <?php endif; ?>
			</div>
			
			
		</div>
	</div></div>
</section>