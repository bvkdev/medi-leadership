
<?php
				// check if the flexible content field has rows of data
				if( have_rows('content_blocks') ):
				
				 	// loop through the rows of data
				    while ( have_rows('content_blocks') ) : the_row();
				
						// // current row layout = CTA BAR
				        if( get_row_layout() == 'call-to-action_bar' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__cta-bar'); ?>
						
						<?php // current row layout = SIMPLE CTA ?>		
						<?php elseif( get_row_layout() == 'simple_cta' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__simple-cta'); ?>
						
						
						<?php // current row layout = CALL-TO-ACTION OVER VIDEO (DONATE CTA) ?>		
						<?php elseif( get_row_layout() == 'donate_cta' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__cta-over-video'); ?>
						
						
						<?php // current row layout = CHART  ?>
							<?php elseif ( get_row_layout() == 'chart' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__chart'); ?>		
					
									
						<?php // current row layout = EMPLOYEE GROUPS ?>		
						<?php elseif( get_row_layout() == 'employee_groups' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__employee-groups'); ?>
					
									
						<?php // current row layout = FAQ ?>		
						<?php elseif( get_row_layout() == 'faqs' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__faqs'); ?>


						<?php // current row layout = FEATURE CARDS ?>
						<?php elseif( get_row_layout() == 'feature_cards' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__feature-cards'); ?>

							
						<?php // current row layout = FEATURED STORIES ?>		
						<?php elseif( get_row_layout() == 'featured_stories' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__featured-posts'); ?>
							
							
						<?php // current row layout = FEATURED PROGRAMS ?>		
						<?php elseif( get_row_layout() == 'featured_programs' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__featured-programs'); ?>
							
							
						<?php // current row layout = FEATURED EVENTS ?>		
						<?php elseif( get_row_layout() == 'featured_events' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__featured-events'); ?>
						
						
						<?php // current row layout = HALF AND HALF ?>		
						<?php elseif( get_row_layout() == 'half_photo_repeater' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__half_photo_repeater'); ?>
						
						
						<?php // current row layout = HOVER VIDEO PANELS ?>		
						<?php elseif( get_row_layout() == 'hover_video_panels' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__hover-video-panels'); ?>
						
						
						<?php // current row layout = HORIZONTAL RULE ?>		
						<?php elseif( get_row_layout() == 'horizontal_rule' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__horizontal-rule'); ?>
						
						
						<?php // current row layout = ICON BUTTONS LIST ?>		
						<?php elseif( get_row_layout() == 'icon_list' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__icon-buttons'); ?>
						
						
						<?php // current row layout = IFRAME AND SCRIPTS ?>		
						<?php elseif( get_row_layout() == 'iframe_and_scripts' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__iframe-and-scripts'); ?>
							
						
						<?php // current row layout = MULTI-COLUMN TEXT ?>		
						<?php elseif( get_row_layout() == 'multi_column_text' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__multicolumn-text'); ?>
							
						
						<?php // current row layout = PAGE HERO ?>		
						<?php elseif( get_row_layout() == 'page_hero' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__page-header'); ?>
						
						
						<?php // current row layout = QUOTE CAROUSEL ?>		
						<?php elseif( get_row_layout() == 'quote_carousel' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__quote-carousel'); ?>
							
						
						<?php // current row layout = REGIONS MENU ?>		
						<?php // elseif( get_row_layout() == 'regions_menu' ): ?>
							<?php // get_template_part('library/custom-loops/b-loopblock__menu-with-photos'); ?>
							
						
						<?php // current row layout = REGIONS MENU 2 ?>		
						<?php elseif( get_row_layout() == 'regions_menu' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__menu-with-map'); ?>
							
						
						<?php // current row layout = RELATED PAGES ?>		
						<?php elseif( get_row_layout() == 'related_pages' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__related-pages'); ?>
								
						<?php // current row layout = SECTIONS WITH MENU/GROUPED CONTENT ?>		
						<?php elseif( get_row_layout() == 'grouped_content' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__grouped-content'); ?>
														
							
						<?php // current row layout = TEXT INTRO ?>		
						<?php elseif( get_row_layout() == 'text_intro' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__text-intro'); ?>
						
						
						<?php // current row layout = TEXT BODY ?>		
						<?php elseif( get_row_layout() == 'text_body' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__text-body'); ?>	
						
						
						<?php // current row layout = TEXT EDIT ?>		
						<?php elseif( get_row_layout() == 'text_editor' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__text-editor'); ?>
							
						
						<?php // current row layout = TEXT OVER PHOTO ?>		
						<?php elseif( get_row_layout() == 'text_over_photo' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__text-over-photo'); ?>
							
							
						<?php // current row layout = TEXT AREA WITH SIDE IMAGE ?>
						<?php elseif( get_row_layout() == 'text_with_side_image' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__text-with-side-image'); ?>
							
							
						<?php // current row layout = TEXT AREA WITH 3RD PARTY SCRIPT ?>
						<?php elseif( get_row_layout() == 'social_media_feed' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__text-with-third-party'); ?>
						
						
						<?php /* END REGIONAL OFFICE BLOCKS */ ?>
						
						<?php // current row layout = VIDEO FEATURE ?>		
						<?php elseif( get_row_layout() == 'video_feature' ): ?>
							<?php get_template_part('library/custom-loops/b-loopblock__video-feature'); ?>

						
						
						<?php  
						endif; // END current row layout check
				
				    endwhile;
				
				else :
				
				    // no layouts found
				
				endif;
				
				?>
