<?php // CURRENT ROW LAYOUT = feature-cards  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);
$section_title = get_sub_field('section_title');
?>
<?php
// WHAT THE 'READ MORE' LABEL AFTER THE EXCERPT SHOULD SAY
$read_more_label = get_sub_field('read_more_label');
if( $read_more_label == "") {
	$read_more_label = "Event Details";
}
?>
<?php
//GET POSTS BASED ON WHAT INCLUSION METHOD TO USE: AUTO OR MANUAL SELECTION
$selection_method = get_sub_field('selection_method');
if( $selection_method == "") {
	$selection_method = "manual";
}
//
$featured_posts_category = "";
$featured_post_data = "";
if( $selection_method == "manual") {
	$featured_post_data = get_sub_field('featured_posts');
	//var_dump($featured_post_data);
} elseif( $selection_method == "automatic") {
	$featured_posts_categories = get_sub_field('featured_posts_category');
	//var_dump($featured_posts_categories);
	//TEMPORARILY CREATE NEW QUERY OBJECT
	$today = date('Ymd');
	$type = 'type_event';
	$args=array(
	  'post_type' => $type,
	  'post_status' => 'publish',
	  'posts_per_page' => 3,
	  'tax_query' => array(
				array(
					'taxonomy' => 'event_categories',
					'field' => 'term_id',
					'terms' => $featured_posts_categories,
				)
		),
		'meta_query' => array(
	  		'relation' => 'OR',
		  	
			'ongoing_dates' => array(
				'ongoing_date_value' => array(
						'key' => 'ongoing_status',
						'value' => true,
						'compare' => '==',
						),
			),
			
		  	'has_end_date' => array(
		  		'relation' => 'AND',
				'end_date_value' => array(
						'key' => 'end_date',
						'value' => $today,
						'compare' => '>=',
						),
				'end_date_value_2' => array(
						'key' => 'end_date',
						'value' => "",
						'compare' => '!=',
						)
			),
			
			'only_start_date' => array(
				'start_date_value' => array(
						'key' => 'start_date',
						'value' => $today,
						'compare' => '>=',
						),
			),
			
			
		    
		),
	  'meta_key' => 'start_date',
	  'orderby' => 'meta_value',
	  'type' => 'DATE',
	  'order' =>'ASC',
	);

	$my_query = null;
	$my_query = new WP_Query($args);
	$featured_post_data = $my_query->posts;
}
$post_counter = 0;
//
//$programs_amount = sizeof($featured_posts_categories);

?>
<?php if( $featured_post_data ) : ?>
<section class="b-section <?php echo $collapse_class; ?> b-section-featured-events" >
	<div class="wrap b-section__wrap-outer wrap b-section__wrap-outer_featured-events cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_featured-events cf">
			
		<?php if($section_title): ?>
			<h2 class="b-headline h2 b-section__title b-section__title_featured-programs"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>	
			
		<?php if( count($featured_post_data) > 0 ) { ?>
		    <div class="b-featured-events">
					<ul class="b-featured-events__wrap b-columns">
		    <?php foreach ($featured_post_data as $post) : ?>
		    	<?php get_template_part('library/custom-loops/b-single__event'); ?>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				
		    		</ul>
		    <?php
		    	$related_posts_link = get_permalink( get_page_by_title( 'Events' ) );
			?>
				<p class="b-featured-event__bloglink"><a href="<?php echo $related_posts_link; ?>">See More Events</a></p>
			</div>
		<?php } else { ?>
			<p>There are currently no upcoming events.</p>
		<?php } //end check for events ?>
		</div>
	</div>
</section>
<?php endif; ?>