<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
// Get values for page intro
$headline = get_sub_field('headline');
$subheadline = get_sub_field('subheadline');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_photo-menu <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_photo-menu cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_photo-menu cf">
			<?php if($headline): ?> <h2 class="h2 b-headline b-photo-menu__headline"><span><?php echo $headline; ?></span></h2><?php endif; ?>
			<?php if($subheadline): ?> <p class="b-photo-menu__text"><?php echo $subheadline; ?></p><?php endif; ?>
			<?php if( have_rows('menu_items') ): ?>
				<?php while( have_rows('menu_items') ): the_row(); ?>
					<?php
					$image = get_sub_field('image');
					$image_url = get_sub_field('image');
					/*?>
					<div class="b-photo-menu__photo">
						<div class="b-resp-image">
							<img class="b-photo-menu__img" src="<?php echo $image_url; ?>" />
						</div>
					</div>
				<?php*/ endwhile; ?>
			    <ul class="b-photo-menu">
			    <?php while( have_rows('menu_items') ): the_row(); ?>
			    	<?php
					
					$headline = get_sub_field('headline');
					$subhead = get_sub_field('subheadline');
					$text = get_sub_field('intro');
					$button_label = ro_get_button_label();
					$button_link = ro_button_link();
					
					?>
					<li class="b-photo-menu__item">
						<?php if($button_label && $button_link): ?>
							<a class="b-photo-menu__button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
						<?php endif; ?>					
					</li>
				<?php endwhile; ?>
			    </ul>
			<?php endif; ?>
		</div>
</section>