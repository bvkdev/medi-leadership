<?php // CURRENT ROW LAYOUT = horizontal_rule  ?>
<?php
$ro_prefix = '';
$collapse_class = ro_collapse_padding($ro_prefix);
//
$rule_color = get_sub_field('color');
$rule_size = get_sub_field('size_option');
?>
<section class="<?php echo $collapse_class; ?>">
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_hr">
		<div class="b-section__wrap-inner b-section__wrap-inner_hr" >
			<hr class="b-rule b-rule_<?php echo $rule_size; ?>" style='border-top: 1px solid <?php echo $rule_color; ?>;' />
		</div>
	</div>
</section>