<?php
// check if the flexible content field has rows of data
if( have_rows('third_party_seals', 'options') ):
?>
	<div class="b-charity-seals">
		
		<ul class="b-menu__list b-charity-seals__list">
	<?php
	 	// loop through the rows of data
	    while ( have_rows('third_party_seals', 'options') ) : the_row();
			$title = get_sub_field('title');
			$icon = get_sub_field('icon');
			$url = get_sub_field('url');
			?>
			<li class='b-charity-seal b-menu__list-item'>
				<a class='b-charity-seal__link' href="<?php echo $url; ?>" title="<?php echo $title; ?>" target="_blank">
					<img class='b-charity-seal__image' src="<?php echo $icon; ?>" alt="<?php echo $title; ?>" />
				</a>
			</li>
			<?php
		endwhile;
	?>
		</ul>
	</div>
<?php				
	else :
	
	    // no layouts found
	
	endif;
?>