<?php // CURRENT ROW LAYOUT = text_over_photo  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text($ro_prefix);
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();

$collapse_class = ro_collapse_padding($ro_prefix);

?>
<?php
$headline = get_sub_field('headline');
$intro = get_sub_field('intro');
$font_size = get_sub_field('text_size');
$display_size = get_sub_field('display_size');
?>



<section <?php echo $id_tag; ?>  class="b-section b-section_outer-pad b-section_<?php echo $display_size; ?> <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?> b-section-textphoto " <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_textphoto cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_textphoto cf" >
			<h2 class="h2 b-headline b-section-textphoto__title"><span><?php echo($headline); ?></span></h2>
			<?php if($intro): ?> <div class="b-wysiwyg b-section-textphoto__text"><?php echo $intro; ?></div><?php endif; ?>
			<?php if( $section_button_link ) : ?>
		    	<a class="b-button_flat b-section-textphoto__button" href="<?php echo $section_button_link; ?>"><?php echo $button_label; ?></a>
		    <?php endif; ?>
		</div>
	</div>
</section>
