<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//

?>

<?php if( have_rows($ro_prefix.'board_groups') ): ?>
<section <?php echo $id_tag; ?> class="b-section b-section-boards <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_boards cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_boards cf">

			<div class="b-boards">
				<?php while( have_rows($ro_prefix.'board_groups') ): the_row(); ?>
				<?php
					$board_name = get_sub_field('board_name');
				?>
				<div class="b-board">
					<h3 class="b-board__name"><?php echo $board_name; ?></h3>
					<div class="b-board__members b-columns">
						<?php if( have_rows('board_members') ): ?>
							<?php while( have_rows('board_members') ): the_row(); ?>
								<?php
									$board_member_name = get_sub_field('name');
									$board_member_position = get_sub_field('position');
									$board_member_extra_lines = get_sub_field('open_line');
								?>
								<div class="b-board-member p-all m-1of2 t-1of4 d-1of4">
									<?php if($board_member_name): ?><h4 class="b-board-member__name"><?php echo $board_member_name; ?></h4><?php endif; ?>
									<?php if($board_member_position): ?><p class="b-board-member__position"><?php echo $board_member_position; ?></p><?php endif; ?>
									<?php if( have_rows('custom_line')): ?>
										<ul class="b-board-member__bylines">
										<?php while( have_rows('custom_line') ): the_row(); ?>
											<li class="b-board-member__byline">
												<?php
												// current row layout = Text
										        if( get_row_layout() == 'text' ):
													$board_member_text = get_sub_field('text'); 
													echo $board_member_text;
												?>
												
												<?php // current row layout = email 	
												elseif( get_row_layout() == 'email' ):
													$board_member_email = get_sub_field('email_address'); 
												?>
													<a href="<?php echo 'mailto://'.$board_member_email; ?>"><?php echo $board_member_email; ?></a>
												
												<?php // current row layout = link 	
												 elseif( get_row_layout() == 'link' ):
													$board_member_link = get_sub_field('link_label');
													$board_member_url = get_sub_field('url'); 
												?>
													<a href="<?php echo $board_member_url; ?>"><?php echo $board_member_link; ?></a>
												
												<?php endif; ?>
											</li>
										<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
					
				</div>
				
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>