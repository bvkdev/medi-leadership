<?php // CURRENT ROW LAYOUT = video_feature  ?>
<?php
$content_bkg_style = bb_set_content_bkg_style();
$section_bkg_style = set_background_style();
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = inverse_text();
//$anchor_tag = anchors_away();
$id_tag = add_section_id();
$custom_classes = bb_add_section_classes();
?>
<?php
$video_headline = get_sub_field('headline');
$video_text = get_sub_field('text');
$video_poster = get_sub_field('poster_image');
$video_link = get_sub_field('video_link');
?>
<?php
if($video_headline || $video_text){
	$video_grid = "t-1of2 d-1of2";
} else {
	$video_grid = "";
}

?>


<section <?php echo $id_tag; ?> class="b-section b-section_video-feature <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> <?php echo $inverse_class; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer cf">
		<div class="b-section__wrap-inner cf " >
			<?php if($video_headline || $video_text): ?>
			<div class="b-section_video-feature__text">
				<h2 class="h2 b-headline b-section_video-feature__headline b-section__headline"><?php echo $video_headline; ?></h2>
				<p class="b-section_video-feature__intro b-section__intro b-wysiwyg"><?php echo $video_text; ?></p>
			</div>
			<?php endif; ?>
			<div class="b-video-poster b-resp-image">
				<a href="<?php echo $video_link; ?>" class="b-video-poster__link cboxElement_video">
					<img src="<?php echo $video_poster; ?>" />
					<span class="b-video-poster__play-button"></span>
				</a>
			</div>
		</div>
	</div>
</section>			