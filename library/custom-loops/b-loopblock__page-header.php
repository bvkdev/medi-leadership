<?php // CURRENT ROW LAYOUT = page-header  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
// Get values for page header
$page_header_headline = get_sub_field('headline');
$background_video_url = get_sub_field('background_video');
$scroll_indicator = get_sub_field('include_scroll_indicator');
$scroll_indicator_label = get_sub_field('scroll_indicator_label');
?>


<section <?php echo $id_tag; ?> class="b-section b-section_hero <?php echo $custom_classes; ?> <?php echo $inverse_class; ?> <?php if($background_video_url != "") { echo 'b-section_hero_video'; } ?>" <?php echo $section_bkg_style; ?>">
	<?php if($background_video_url != "") : ?>
		<video style="object-fit: cover;" autoplay="" loop="" preload="" poster="" class="b-video-background"><source src="<?php echo $background_video_url; ?>" type="video/mp4"></video>
	<?php endif; ?>
	
		
		<div class="wrap b-section__wrap-outer b-section__wrap-outer_hero cf">
			<div class="b-section__wrap-inner b-section__wrap-inner_hero cf">
				<?php if($page_header_headline): ?><h1 class="b-headline b-hero__headline <?php if($background_video_url != "") { echo 'b-hero__headline_video'; } ?>"><span><?php echo $page_header_headline; ?></span></h1><?php endif; ?>
				<?php if($scroll_indicator): ?>
					<p class='b-scroll-more'><?php echo $scroll_indicator_label; ?><span></span></p>
				<?php endif; ?>
			</div>
		</div>
		
</section>