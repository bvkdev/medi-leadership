<?php // CURRENT ROW LAYOUT = photo_break  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
// Get other values
$display_size = get_sub_field('display_size');
?>

<section <?php echo $id_tag; ?>  class="b-section <?php echo $collapse_class; ?> b-section_outer-pad b-photo-break_<?php echo $display_size; ?> b-section_photo-break b-photo-break <?php echo $collapse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer cf">
		<div class="b-section__wrap-inner cf">
		</div>
	</div>
</section>
