<?php // CURRENT ROW LAYOUT = big_title  ?>
<?php
$content_bkg_style = bb_set_content_bkg_style();
$section_bkg_style = set_background_style();
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = inverse_text();
//$anchor_tag = anchors_away();
$id_tag = add_section_id();
$custom_classes = bb_add_section_classes();
?>
<?php
	$cta_url = get_sub_field('cta_button_alternate_url');
	if(!$cta_url) :
		$cta_url = '#pageform';
	endif;
?>	


<section <?php echo $id_tag; ?> class="b-section b-intro <?php echo $collapse_class; ?> <?php echo $custom_classes; ?>">
	<div class="wrap b-section__wrap-outer cf"><div class="b-section__wrap-inner cf " >
		<div class="b-intro__text t-2of3 d-2of3">
			<div class="b-wysiwyg"><?php the_sub_field('text_editor'); ?></div>
		</div>
		<div class="b-intro-cta t-1of3 d-1of3">
			<h3 class="b-intro-cta__headline"><?php the_sub_field('cta_headline'); ?></h3>
			<p class="b-intro-cta__subhead"><?php the_sub_field('cta_subheadline'); ?></p>
			<a class="b-intro-cta__button slide" href=<?php echo $cta_url; ?>><img class="b-inline-icon" src="<?php echo get_template_directory_uri(); ?>/library/images/email-icon-teal-60x60.png" /><?php the_sub_field('cta_button_text'); ?></a>
		</div>
	</div></div>
</section>

								