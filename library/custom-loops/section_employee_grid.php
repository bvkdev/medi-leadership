<?php
//get page values
$ro_prefix = 'page_link_';
$button_label = ro_get_button_label($ro_prefix);
$section_button_link = ro_button_link($ro_prefix);
//set up query object
//
$type = 'type_employee';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'meta_key' => 'last_name',
  'orderby' => 'meta_value',
  'order' =>'ASC',
  );
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) :

?>


<section class="b-section b-section-coaches">
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_coaches cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_coaches cf">
			<div class="b-columns b-coaches">
				<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<?php
					// Get values for employee
					$name = get_the_title();
					$title = get_field('job_title');
					$employee_page_link = get_the_permalink();
					//$phone = get_field('phone_number');
					//$email = get_field('email');
					//$social_array = get_field('social_media_links');
					//$bio = get_field('bio');
					$post_image = get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'b-coach__image', 'alt' => $name." Photograph" ) );
				?>
				<?php
					//create an id-ready lowercase version of the employee name
					$team_member_id = strtolower($name);
					//Make alphanumeric (removes all other characters)
				    $team_member_id = preg_replace("/[^a-z0-9_\s-]/", "", $team_member_id);
				    //Clean up multiple dashes or whitespaces
				    $team_member_id = preg_replace("/[\s-]+/", " ", $team_member_id);
				    //Convert whitespaces and underscore to dash
				    $team_member_id = preg_replace("/[\s_]/", "-", $team_member_id);
				?>
				<div id="<?php echo $team_member_id; ?>" class="b-coach p-1of2 m-1of2 t-1of3 d-1of3 js-equal-heights_rows">
					<div class="b-resp-image b-coach__photo">
						<a class="b-coach__photo-link" href="<?php echo $employee_page_link; ?>"><?php echo $post_image; ?></a>
					</div>
					<div class="b-coach__details">
						<h4 class="b-coach__name"><?php echo $name; ?></h4>
						<p class="b-coach__title"><?php echo $title; ?></p>
						<a class="b-coach__link" href="<?php echo $employee_page_link; ?>"><span></span></a>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
			<?php if( $section_button_link ) : ?>
			<div class="b-section-coaches__related">
		    	<a class="b-button_flat b-section-textphoto__button" href="<?php echo $section_button_link; ?>"><?php echo $button_label; ?></a>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>