<?php
$featured_event_title = get_the_title();
$featured_event_excerpt = get_field('description');
$featured_event_location = get_field('location');
$featured_event_id = get_the_ID();
$featured_event_url = get_field('form_link');
// START DATE
$featured_event_date_start = get_field('start_date');
//echo $featured_event_date_start;
if($featured_event_date_start != ""):
	$featured_event_date_start_display = date("j M", strtotime($featured_event_date_start));
else:
	$featured_event_date_start_display = "";
endif;
//echo " ".$featured_event_date_start_display;
// END DATE
$featured_event_date_end = get_field('end_date');
//echo "END: ".$featured_event_date_end;
if($featured_event_date_end != ""):
	$featured_event_date_end_display = date("j M", strtotime($featured_event_date_end));
else:
	$featured_event_date_end_display = "";
endif;
//echo " ".$featured_event_date_end_display;
// START AND END TIMES
$featured_event_time_start = get_field('start_time');
if($featured_event_time_start != ""):
	$featured_event_time_start_display = date("g:i a", strtotime($featured_event_time_start));
else:
	$featured_event_time_start_display = "";
endif;
$featured_event_time_end = get_field('end_time');
if($featured_event_time_end != ""):
	$featured_event_time_end_display = date("g:i a", strtotime($featured_event_time_end));
else:
	$featured_event_time_end_display = "";
endif;
//
//For ongoing events
$featured_event_ongoing = get_field('ongoing_status');
if( $featured_event_ongoing == true ) {
	$featured_event_date_start_display = get_field('ongoing_label');
}
?>
<li class="b-feature-event <?php //echo $post_cats_list; ?> js-sortable-object p-all m-all t-1of3 d-1of3">
	<div class="b-feature-event__inner js-equal-heights">
			<div class="b-feature-event__dates">
				<span class="b-feature-event__date b-feature-event__date_start">
					<?php echo $featured_event_date_start_display; ?>
				</span>
				<?php if( $featured_event_date_end != "" && $featured_event_date_end_display != $featured_event_date_start_display ): ?>
				<span class="b-feature-event__date-splitter">-</span>
				<span class="b-feature-event__date b-feature-event__date_end">
					<?php echo $featured_event_date_end_display; ?>
				</span>
				<?php endif; ?>
			</div>
			<div class="b-feature-event__content">						
			<?php if($featured_event_title): ?><h3 class="b-feature-event__headline"><?php echo $featured_event_title; ?></h3><?php endif; ?>																
			<?php if($featured_event_time_start != "" || $featured_event_location != ""): ?>
				<div class="b-feature-event__time-loc">
					
					<?php if($featured_event_time_start != "") : ?>
					<div class="b-feature-event__times">
						<span class="b-feature-event__time b-feature-event__time_start">
							<?php echo $featured_event_time_start_display; ?>
						</span>
						<?php if( $featured_event_time_end != "" && $featured_event_time_end != $featured_event_time_start ): ?>
							<span class="b-feature-event__time-splitter">-</span>
							<span class="b-feature-event__time b-feature-event__time_end">
								<?php echo $featured_event_time_end_display; ?>
							</span>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				
				<?php if($featured_event_time_start && $featured_event_location): ?>
					<span class="b-featured-event__location-splitter"> - </span>
				<?php endif; ?>
				
				<?php if($featured_event_location): ?>
					<span class="b-featured-event__location"><?php echo $featured_event_location; ?></span>
				<?php endif; ?>
			</div>
			<?php endif; ?>	
			<?php if($featured_event_excerpt): ?><div class="b-feature-event__text b-wysiwyg"><?php echo $featured_event_excerpt; ?></div><?php endif; ?>																
			<?php if($featured_event_url): ?>
				<?php if( !isset($read_more_label) ) { $read_more_label = "Event Details"; } ?>
			<a href="<?php echo $featured_event_url; ?>" title="<?php echo $featured_event_title; ?>" class="b-feature-event__link"><?php echo $read_more_label; ?></a>
			<?php endif; ?>	
			</div>															
	</div>
</li>