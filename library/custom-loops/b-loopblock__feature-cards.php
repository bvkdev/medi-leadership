<?php // CURRENT ROW LAYOUT = feature-cards  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);
$section_title = get_sub_field('section_title');
?>
<?php
$section_style = "";
$layout_choice = get_sub_field('card_style');
if($layout_choice == 'text_under'):
	$section_style = '_default';
elseif($layout_choice == 'text_overlay'):
	$section_style = '_overlay';
else :
	$section_style = '_default';
endif;
?>
<?php
$grid = "p-all m-all t-1of3 d-1of3";
$row_amount = get_sub_field('how_many_across');
switch ($row_amount) {
    case 1:
        $grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $grid = "p-all m-1of2 t-1of2 d-1of2";
        break;
    case 3:
        $grid = "p-all m-1of3 t-1of3 d-1of3";
        break;
	case 4:
        $grid = "p-all m-all t-1of4 d-1of4";
        break;
	case 5:
        $grid = "p-all m-all t-1of3 d-1of5";
        break;
	case 6:
        $grid = "p-all m-all t-1of3 d-1of6";
        break;
	case 7:
        $grid = "p-all m-all t-1of4 d-1of7";
        break;
	case 8:
        $grid = "p-all m-all t-1of4 d-1of8";
        break;
}
?>
<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> <?php echo $inverse_class; ?> b-section_feature-cards b-section_feature-cards<?php echo $section_style; ?> <?php if($section_title): ?>b-section_tucked-title<?php endif; ?>" <?php echo $section_bkg_style; ?>>
	
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_feature-cards<?php echo $section_style; ?> cf">
		
		<div class="b-section__wrap-inner b-section__wrap-inner_feature-cards<?php echo $section_style; ?> cf ">
		<?php if($section_title): ?>
			<h2 class="b-headline h2 b-feature-cards__title b-section__title"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>	
			
		<?php if( have_rows('feature_cards') ): ?>
		
	    <ul class="b-feature-cards b-feature-cards<?php echo $section_style; ?> b-columns">
	    <?php while( have_rows('feature_cards') ): the_row(); ?>
	    	<?php
			$image = get_sub_field('image');
			$headline = get_sub_field('headline');
			$text = get_sub_field('text');
			$button_label = "";
			$button_label = ro_get_button_label();
			$button_link = ro_button_link();
			//$inverse_class = inverse_text();
			?>
			<li class="b-feature-card b-feature-card<?php echo $section_style; ?> <?php echo $grid; ?>">
				<div class="b-feature-card__inner b-feature-card__inner<?php echo $section_style; ?> js-equal-heights_rows <?php if($button_label && $button_link): ?>b-feature-card__inner_has-btn<?php endif; ?>">
					<?php if($section_style == '_default'): ?>
						<?php if($button_link): ?><a href="<?php echo $button_link; ?>" title="<?php echo $headline; ?>" class="b-feature-card__image-link"><?php endif; ?>
						<?php if($image): ?><div class="b-feature-card__image b-resp-image b-resp-image_feature-card b-resp-image_feature-card<?php echo $section_style; ?>">
												<img class="b-image_feature-card" alt="<?php echo $headline; ?>" src="<?php echo $image; ?>" />
											</div>
											<div class="b-feature-card__image_hover">
											<p ><?php echo $button_label; ?></p>
											</div>
						<?php endif; ?>
													
						<?php if($button_link): ?></a><?php endif; ?>
					<?php else: ?>
						<div class="b-feature-card__cover-image" style="background: url(<?php echo $image; ?>) center center no-repeat; background-size: cover;"><?php if($button_link && !$button_label): ?><a href="<?php echo $button_link; ?>"></a><?php endif; ?></div>
					<?php endif; ?>
					<?php if($headline): ?> <h3 class="b-feature-card__headline b-feature-card__headline<?php echo $section_style; ?>"><?php echo $headline; ?></h3><?php endif; ?>
					<?php if($text): ?> <div class="b-wysiwyg b-feature-card__text b-feature-card__text<?php echo $section_style; ?>"><?php echo $text; ?></div><?php endif; ?>
					<?php /* if($button_label && $button_link): ?>
						<a class="b-feature-card__button b-feature-card__button<?php echo $section_style; ?>" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
					<?php endif;*/ ?>
				</div>
			</li>
		<?php endwhile; ?>
	    </ul>
		<?php endif; ?>
	</div></div>
</section>
