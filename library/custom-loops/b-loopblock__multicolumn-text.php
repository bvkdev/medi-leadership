<?php // CURRENT ROW LAYOUT = multi-column-text  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
?>
<?php
$section_columns_class = "";
$column_amount = get_sub_field('columns');
if( $column_amount == 1) :
	$column_classes = "";
elseif( $column_amount == 2 ) :
	$column_classes = "t-1of2 d-1of2";
	$section_columns_class = "b-section_multi-column_twocol";
endif;
?>


<section <?php echo $id_tag; ?> class="b-section b-section_multi-column <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> <?php echo $section_columns_class; ?> <?php echo $inverse_class; ?>" <?php echo $section_bkg_style; ?>>
<div class="wrap b-section__wrap-outer b-section__wrap-outer_multi-column cf">
	<div class="b-section__wrap-inner b-section__wrap-inner_multi-column b-columns">
		<div class="b-wysiwyg b-multi-column__text <?php echo $column_classes; ?>">
			<?php the_sub_field('text'); ?>
		</div>
	<?php if( $column_amount == 2) : ?>
		<div class="b-wysiwyg b-multi-column__text b-multi-column__text_two <?php echo $column_classes; ?>">
			<?php the_sub_field('text_2'); ?>
		</div>
	<?php endif; ?>
	</div>
</div>
</section>