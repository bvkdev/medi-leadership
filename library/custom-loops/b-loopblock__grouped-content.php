<?php // CURRENT ROW LAYOUT = grouped_content  ?>
<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = ro_inverse_text();
?>


<?php if( get_sub_field('add_group') != "" ):
	if( have_rows('add_group') ):
		$group_names = array();
		$group_options = array();
    while( have_rows('add_group') ): the_row();
		$group_name = get_sub_field('group_title');
		array_push($group_names, $group_name);
		$group_name = str_replace(".", "", $group_name);
		$group_name = strtolower($group_name);
		$group_option = "<option value='".str_replace(" ", "-", $group_name)."'>".$group_name."</option>";
		array_push($group_options, $group_option);
		
	endwhile;
	endif;
?>
<?php
//Section Content
$sortbar_label = get_sub_field('sort_bar_label');
$sortbar_detail = get_sub_field('optional_detail_text');
$sortbar_back_up = get_sub_field('back_to_top_label');
?>
<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?>  b-section_grouped-content <?php echo $inverse_class; ?>"  <?php echo $section_bkg_style; ?>>

	<div class="wrap b-section__wrap-outer b-section__wrap-outer_grouped cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_grouped cf " >
			<a id="b-group-sortbar" name="b-group-sortbar"></a>
			<?php //Select Box (drop down sorting) ?>
			<div class="b-group-sortbar b-group-sortbar_dropdown">
				<div class="b-select b-select_sortbar">
					<?php if( $sortbar_label ): ?>
					<label class="b-select__label b-select__label_inline"><?php echo $sortbar_label; ?></label>
					<?php endif; ?>
					<select class="b-select__input b-select__input_inline js-jumpto-select">
						<?php
						foreach($group_options as $option):
							echo $option;
						endforeach;
						?>
					</select>
				</div>
				<?php if($sortbar_detail != "") : ?>
				<p class="b-group-sortbar__detail b-group-sortbar__detail_dropdown"><?php echo $sortbar_detail; ?></p>
				<?php endif; ?>
			</div>
			<?php //List Menu  ?>
			<div class="b-group-sortbar b-group-sortbar_menu">
				<?php if( $sortbar_label ): ?>
				<label class="b-label b-label__sortbar b-label__sortbar_menu"><?php echo $sortbar_label; ?></label>
				<?php endif; ?>
				<ul class="b-group-sortbar__list">
					<?php
					foreach($group_names as $name):
						$group_name_escaped = str_replace(" ", "-", $name);
						$group_name_escaped = str_replace(".", "", $group_name_escaped);
			    		$group_name_escaped = strtolower($group_name_escaped);
						echo "<li class='b-group-sortbar__list-item'><a href='#".$group_name_escaped."'>".$name."</a></li>";
					endforeach;
					?>
				</ul>
				<?php
				$sortbar_detail = get_sub_field('optional_detail_text');
				if($sortbar_detail != "") : ?>
				<p class="b-group-sortbar__detail b-group-sortbar__detail_list"><?php echo $sortbar_detail; ?></p>
				<?php endif; ?>
			</div>
			
			<?php if( have_rows('add_group') ): ?>
				<div class="b-grouped-groups">
	    	<?php while( have_rows('add_group') ): the_row(); ?>
	    		<?php //Group Content
	    		$group_content_data = "";
	    		$group_title = "";
	    		$group_title = get_sub_field('group_title');
	    		
	    		$group_title_escaped = str_replace(" ", "-", $group_title);
				$group_title_escaped = str_replace(".", "", $group_title_escaped);
	    		$group_title_escaped = strtolower($group_title_escaped);
				$group_content = "";
	    		$group_content = get_sub_field('group_content');
				//legacy group content prior to flexible layout option
				if( !is_array($group_content) ) :
					$group_content_data = $group_content;
				else :
					$group_content_data = "holding";
					?>
				<?php	
				endif; //END IF ARRAY CHECK
	    		?>
			    <div class="b-grouped-content">
				    <p class="b-grouped-content__title"><a id="<?php echo $group_title_escaped; ?>" name="<?php echo $group_title_escaped; ?>"><?php echo $group_title; ?></a><a class="b-grouped-content__title__back" href="#b-group-sortbar"><?php echo $sortbar_back_up; ?></a></p>
				    
					<?php if( !is_array($group_content) ) : //legacy group content prior to flexible layout option ?>
						<?php
					    if( get_sub_field('organize_into_columns') ):
							$make_grid = "js_li_grid";
							$elem_overide = "data-organizer='".get_sub_field('class_name_to_organize')."'";
						else : 
							$make_grid = '';
							$elem_overide = '';
						endif;
						?>
					    <div class="b-grouped-content__content  b-wysiwyg <?php echo $make_grid; ?>" <?php echo $elem_overide; ?>>
					    	<?php echo $group_content; ?>
					    </div>
			    	<?php else : ?>
				    <div class="b-grouped-content__content" >
				    	<?php
				    	if( have_rows('group_content') ):
					 	// loop through the rows of data
					    while ( have_rows('group_content') ) : the_row(); ?>
					<?php
							// current row layout = WYSIWYG
					        if( get_row_layout() == 'text_editor' ){
					        	$group_content_data = get_sub_field('text_block');
								echo "<div class='b-wysiwyg b-wysiwyg_grouped'>".$group_content_data."</div>";

							// current row layout = FAQS 		
							}elseif( get_row_layout() == 'faq' ){ ?>
								
								<?php if( have_rows('add_question') ){ ?>
								<ul class="b-faqs b-faqs_grouped">
									<?php while( have_rows('add_question') ) : the_row(); ?>
										<li class="b-faq b-faq_grouped">
											<div class="b-faq__question b-faq__question_grouped js-reveal-sibling" data-faq="<?php the_sub_field('question'); ?>">
												<span class="b-faq__question_sprite b-faq__question_sprite_grouped" data-faq="<?php the_sub_field('question'); ?>"></span>
												<?php the_sub_field('question'); ?>
											</div>
											<div class="b-faq__answer b-faq__answer_grouped js-hide-load">
												<?php the_sub_field('answer'); ?>
											</div>
										</li>
									<?php endwhile; //END WHILE HAVE FAQ QUESTIONS ?>
								</ul>
								<?php }; //END IF HAVE FAQ QUESTIONS?>
							
							<?php }; //END LAYOUT CHECKING?>
						<?php
						endwhile;
						?>
						
						<?php
						endif; //END HAVE ROWS (GROUP CONTENT)
						?>
				    	
				    </div>
			    	<?php endif; ?>
			    </div>
			<?php endwhile; ?>
			</div>
	    <?php endif; ?>
	</div></div>
</section>
<?php endif; ?>