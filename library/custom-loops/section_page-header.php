<?php
// Check if parent of if subpage
$sub_level;
if($post->post_parent == 0) {
	$sub_level = false;
} else {
	$sub_level = true;
}
?>

<?php
// Get values for page header via common-functions
$ro_prefix = 'page_header_';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$page_header_button_label = ro_get_button_label($ro_prefix);
$section_button_link = ro_button_link($ro_prefix);

// Get values for page header
$include_page_header = get_field('include_page_header');
$page_header_headline = get_field('page_header_headline');
$header_layout_override = get_field('page_header_layout_override');
$background_video_url = get_field('page_header_background_video');
?>
<?php
//set variable to use for main v. subpages
$page_level = "";
if( (!$sub_level || $header_layout_override == "force_headline") && $header_layout_override != "force_subpage"  ) { 
	$page_level = "_hero";
} elseif( ($sub_level || $header_layout_override == "force_subpage")&& $header_layout_override != "force_headline" ) {
	$page_level = "_subpage";
}
?>
<?php if($include_page_header) : ?>
<section <?php echo $id_tag; ?> class="b-section b-section_hero <?php if( ($sub_level || $header_layout_override == "force_subpage")&& $header_layout_override != "force_headline"){ echo 'b-section_hero_subpage'; } ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<?php if($background_video_url != "") : ?>
		<video style="object-fit: cover;" autoplay="" loop="" preload="" poster="" class="b-video-background"><source src="<?php echo $background_video_url; ?>" type="video/mp4"></video>
	<?php endif; ?>
	
		<?php if( (!$sub_level || $header_layout_override == "force_headline") && $header_layout_override != "force_subpage" ) { ?>
		<div class="wrap b-section__wrap-outer b-section__wrap-outer_hero cf">
			<div class="b-section__wrap-inner b-section__wrap-inner_hero cf">
				<?php if($page_header_headline): ?><h1 class="b-headline b-hero__headline"><span><?php echo $page_header_headline; ?></span></h1><?php endif; ?>
				<?php if( $section_button_link ) : ?>
			    	<a class="b-button_flat b-hero__button" href="<?php echo $section_button_link; ?>"><?php echo $page_header_button_label; ?></a>
			    <?php endif; ?>
			</div>
		</div>
		<?php } elseif( ($sub_level || $header_layout_override == "force_subpage")&& $header_layout_override != "force_headline" ) { ?>
		<div class="wrap b-section__wrap-outer b-section__wrap-outer_subpage cf">
			<div class="b-section__wrap-inner_subpage cf">	
				<div class="b-hero_headline-holder cf">
					<h1 class="b-headline b-hero__headline b-hero__headline_subpage"><span><?php the_title(); ?></span></h1>
				</div>
			</div>
		</div>
		<?php } ?>
</section>
<?php endif; ?>
<?php
if(!is_page_template("page-pillar.php")):

// Get values for page intro via common-functions
$ro_prefix = 'introduction_text_';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
// Get values for page intro
$intro_headline = get_field('introduction_text_headline');
$intro_text = get_field('introduction_text_intro');
$intro_stats = get_field("page_stats_statistics");
//print_r($intro_stats);
?>
<?php if($intro_headline || $intro_text) : ?>
<section <?php echo $id_tag; ?> class="b-section b-section_intro b-section_intro<?php echo $page_level; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_intro cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_intro cf">
			<?php if($intro_headline): ?> <h2 class="b-intro__headline"><?php echo $intro_headline; ?></h2><?php endif; ?>
			<?php if($intro_text): ?> <div class="b-intro__text b-wysiwyg"><?php echo $intro_text; ?></div><?php endif; ?>
			<?php if( $intro_stats ): ?>
				<div class="b-stats">
				<?php while( have_rows('page_stats_statistics') ): the_row(); ?>
					<?php
					$stat_value = get_sub_field('stat_value');
					$stat_label = get_sub_field('stat_label');
					?>
					<div class="b-stat">
						<div class="b-stat__value"><?php echo $stat_value; ?></div>
						<div class="b-stat__label"><?php echo $stat_label; ?></div>
					</div>
				<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php endif; ?>