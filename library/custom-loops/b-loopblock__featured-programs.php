<?php // CURRENT ROW LAYOUT = feature-cards  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);
$section_title = get_sub_field('section_title');
?>
<?php

?>
<?php
$featured_programs_data = get_sub_field('featured_posts');
$programs_amount = sizeof($featured_programs_data);


$grid = "p-all m-all t-1of3 d-1of3";

switch ($programs_amount) {
    case 1:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
    case 2:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
    case 3:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
	case 4:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
	case 5:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
	case 6:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
	case 7:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
	case 8:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
}
//

?>
<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_featured-programs" <?php echo $section_bkg_style; ?>>
	
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_featured-programs cf">
		
		<div class="b-section__wrap-inner b-section__wrap-inner_featured-programs cf ">
		<?php if($section_title): ?>
			<h2 class="b-headline h2 b-section__title b-section__title_featured-programs"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>	
			
		
	    <ul class="b-featured-programs b-columns">
	    <?php foreach ($featured_programs_data as $post) : ?>
	    	<?php
				$featured_program_title = get_the_title();
				$featured_program_excerpt = excerpt(10);
				$featured_program_id = get_the_ID();
				$featured_program_url = get_permalink( $featured_program_id );
				$featured_program_image = get_the_post_thumbnail( $featured_program_id, 'full', array( 'class' => 'b-featured-program__thumbnail', 'alt' => $featured_program_title ) );
			?>
			<li class="b-feature-card b-feature-card_programs <?php echo $grid; ?>">
				<div class="b-feature-card__inner b-feature-card__inner_programs js-equal-heights_rows">
						<a href="<?php echo $featured_program_url; ?>" title="<?php echo $featured_program_title; ?>" class="b-feature-card__image-link">
						<div class="b-feature-card__image b-feature-card__image_programs b-resp-image b-resp-image_feature-card b-resp-image_feature-card_programs">
							<?php if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it. ?>
								<?php echo $featured_program_image; ?>
							<?php endif; ?>
						</div>
						<div class="b-feature-card__image_hover b-feature-card__image_hover_programs">
						<p>View Program</p>
						</div>
																			
						</a>
						<?php if($featured_program_title): ?><h3 class="b-feature-card__headline b-feature-card__headline_default"><?php echo $featured_program_title; ?></h3><?php endif; ?>																
				</div>
			</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			
	    </ul>

	</div></div>
</section>
