<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
//
$script_field = get_sub_field('script_field');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_scripts <?php echo $custom_classes; ?> <?php echo $collapse_class; ?>">
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_scripts cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_scripts cf">
			<?php if($script_field): ?> <div class="b-script-holder js-script-holder"><?php echo $script_field; ?></div><?php endif; ?>
		</div>
</section>