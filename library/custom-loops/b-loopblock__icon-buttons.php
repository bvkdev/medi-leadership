<?php // CURRENT ROW LAYOUT = icon-list  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
$section_intro = get_sub_field('section_intro');
?>
<?php
$carousel_display = get_sub_field('display_as_carousel');
$carousel_grid_width = get_sub_field('slide_grid_width');
$carousel_class = "";
if( $carousel_display == true ):
	$carousel_class = "js_icon_carousel cf";
else:
	$carousel_class = "b-columns";
endif;
$grid_width = get_sub_field('grid_width');
$grid = "";
if($grid_width == ""):
	$grid_width = 1;
	$grid = "p-all p-all t-all d-all";
elseif($grid_width == 1):
	$grid = "p-all p-all t-all d-all";
elseif($grid_width>1 && $grid_width <= 4):
	$grid = "p-1of".$grid_width." m-1of".$grid_width." t-1of".$grid_width." d-1of".$grid_width;
elseif($grid_width == 5 || $grid_width == 6):
	$grid = "p-1of3 m-1of3 t-1of".$grid_width." d-1of".$grid_width;
elseif($grid_width == 7 || $grid_width == 8):
	$grid = "p-1of4 m-1of4 t-1of".$grid_width." d-1of".$grid_width;
endif;	
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_icon-list">
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_icon-list cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_icon-list cf " >
		<?php if($section_title): ?>
			<h2 class="b-headline h2 b-section__title b-section__title_icon-list"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>
		<?php if($section_intro): ?>
			<p class="b-section__intro b-section__intro_icon-list"><?php echo $section_intro; ?></p>
		<?php endif; ?>
		 <?php if( have_rows('icon_callouts') ): ?>
		    <ul class="b-icon-list <?php echo $carousel_class; ?>" data-grid=<?php echo $carousel_grid_width; ?>>
		    <?php while( have_rows('icon_callouts') ): the_row(); ?>
		    	<?php
					$image = get_sub_field('icon');
					$text = get_sub_field('headline');
					$button_label = ro_get_button_label();
					$button_link = ro_button_link();
				?>
		        <li class="b-icon <?php echo $grid; ?>">
		        	<div class="b-icon__inner js-equal-heights_rows">
		        	<?php if($button_link && !$button_label): ?>
		        		<a class="b-icon__link" href="<?php echo $button_link; ?>" title="<?php echo $button_label; ?>">
		        	<?php endif; ?>
		        			<img class="b-icon__img" src="<?php echo $image; ?>" />
		        	<?php if($button_link && !$button_label): ?>
		        		</a>
		        	<?php endif; ?>
		        	
		        	<?php if($text): ?>
						<p class="b-icon__text"><?php echo $text; ?></p>
					<?php endif; ?>
		        	
		        	<?php if($button_link && $button_label): ?>
						<a class="b-icon__button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
					<?php endif; ?>
					</div>
		        </li>
		        
		    <?php endwhile; ?>
		    </ul>
	    <?php endif; ?>
	</div></div>
</section>