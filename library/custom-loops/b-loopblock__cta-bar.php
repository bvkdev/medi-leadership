<?php // CURRENT ROW LAYOUT = cta_bar  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);

$section_title = get_sub_field('section_title');
?>
<?php
// Get Social Media Usernames
if( have_rows('social_icons', 'options') ):
	$socialAccount = array();
 	// loop through the rows of data
    while ( have_rows('social_icons', 'options') ) : the_row();
		$title = get_sub_field('title');
		$icon = get_sub_field('icon');
		$url = get_sub_field('url');
		$username = get_sub_field('username');
		
		$newAccount = array(
			"website" => $title,
			"username" => $username
		);
		$socialAccount[strtolower($title)] = $newAccount;
	endwhile;
	//get Twitter Username
	if (array_key_exists('twitter', $socialAccount)) {
		$twitter_array = $socialAccount['twitter'];
		if (array_key_exists('username', $twitter_array)) {
			$twitter_user = $twitter_array["username"];
		}
	}
endif;
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_ctabar" <?php echo $section_bkg_style; ?>>
	<?php if($section_title): ?>
		<h2 class="b-headline h2 b-section_ctabar__title b-section__title"><span><?php echo $section_title; ?></span></h2>
	<?php endif; ?>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_ctabar cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_ctabar cf ">
			<?php
			$cta_box_grid = "p-all m-1of2 t-1of2 d-1of2";
			$cta_box_array = get_sub_field('cards');
			
			$cta_box_count = count( $cta_box_array );
			switch ($cta_box_count) {
			    case 1:
			        $cta_box_grid = "p-all m-all t-all d-all";
			        break;
			    case 2:
			        $cta_box_grid = "p-all m-1of2 t-1of2 d-1of2";
			        break;
			    case 3:
			        $cta_box_grid = "p-all m-1of3 t-1of3 d-1of3";
			        break;
				case 4:
			        $cta_box_grid = "p-all m-1of2 t-1of4 d-1of4";
			        break;
			}
			?>
			<?php if( have_rows('cards') ): ?>
		    <ul class="b-ctabar b-columns">
		    <?php while( have_rows('cards') ): the_row(); ?>
		    	<?php
		    	
				$button_label = ro_get_button_label();
				$button_link = ro_button_link();
				$image = get_sub_field('image');				
				
				//set up sharing if active
				$is_sharing = get_sub_field("add_share_capabilities");
				if($is_sharing){ $share_message = get_sub_field("default_share_message"); }
				?>
				<li class="b-ctabox <?php echo $cta_box_grid; ?>" <?php if($is_sharing): ?>onclick="void(0)"<?php endif; ?>>
					<?php if($button_link): ?><a class="b-ctabox__link" href="<?php echo $button_link; ?>" title="<?php echo $button_label; ?>"> <?php endif; ?>
					<div class="b-ctabox__inner js-equal-heights_rows">
						
						<div class="b-ctabox__cover-image" style="background: url(<?php echo $image; ?>) center center no-repeat; background-size: cover;"><?php if($button_link && !$button_label): ?><a href="<?php echo $button_link; ?>"></a><?php endif; ?></div>
						<div class="b-ctabox__content">
							<?php if($button_label): ?> <h3 class="h2 b-headline b-ctabox__headline"><span><?php echo $button_label; ?></span></h3><?php endif; ?>
							<?php if($is_sharing): ?>
								<div class="b-ctabox__share">
								<p class="b-ctabox__text">"<?php echo $share_message; ?>"</p>
								<ul class="b-ctabox__share-btns b-share-links">
									<li class="b-share-link b-share-link_facebook">
										<a href="<?php the_permalink();?>" title="Share on Facebook" target="_blank">
											<img class="b-share-link__image" src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/share-icon-fb.png" alt="Facebook"  data-social-network="facebook.com" data-social-action="share" data-social-target="<?php the_permalink(); ?>">
										</a>
									</li>
									<li class="b-share-link b-share-link_twitter">
										<a href="http://twitter.com/intent/tweet?text=<?php echo $share_message; ?>&amp;url=<?php the_permalink(); ?>&amp;via=<?php echo $twitter_user; ?>" class="xtwitter-share-button" title="Share on Twitter" target="_blank" data-text="<?php the_title(); ?>" <?php if( $twitter_user != "" ){ echo 'data-via="'.$twitter_user.'"'; } ?> >
											<img class="b-share-link__image" src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/share-icon-tw.png" alt="Twitter" data-social-network="twitter.com" data-social-action="share" data-social-target="<?php the_permalink(); ?>" >
										</a>
									</li>
								</ul>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<?php if($button_link): ?></a><?php endif; ?>
					
				</li>
			<?php endwhile; ?>
		    </ul>
			<?php endif; ?>
		</div>
	</div>
</section>
