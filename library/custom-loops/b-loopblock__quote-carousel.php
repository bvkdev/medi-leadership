<?php // CURRENT ROW LAYOUT = quote-carousel  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = "";//ro_set_background_style($ro_prefix);

$collapse_class = ro_collapse_padding($ro_prefix);
?>


<section <?php echo $id_tag; ?> class="b-section b-section_outer-pad <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> b-section_quotes" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_quotes cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_quotes cf " >
		<?php if( have_rows('quotes') ): ?>
			<div class="b-quotes-carousel js-carousel_quote">
		<?php while( have_rows('quotes') ): the_row(); ?>
			<?php
			$quote = get_sub_field('quote');
			$attribution = get_sub_field('attribution');
			$attribution_2 = get_sub_field('attribution_2');
			$attribution_3 = get_sub_field('attribution_3');
			$attribution_image = get_sub_field('attribution_image');
			?>
		<div class="b-quote js-equal-heights_row">
			<?php if($attribution_image): ?> <div class="b-resp-image b-resp-image_quote b-quote__image"><img src="<?php echo $attribution_image; ?>"/></div> <?php endif; ?>
			<div class="b-quote__words">
			<?php if($quote): ?> <p class="b-quote__body"><?php echo $quote; ?></p> <?php endif; ?>
			<?php if($attribution): ?>
				<p class="b-quote__attribution">
					<span class="b-quote__attribution_1"><?php echo $attribution; ?></span><?php if($attribution_2): ?><span class="b-quote__attribution_2"><?php echo $attribution_2; ?></span><?php endif; ?><?php if($attribution_3): ?><span class="b-quote__attribution_3"><?php echo $attribution_3; ?></span><?php endif; ?>
				</p>
			<?php endif; ?>
			</div>
		</div>
		<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div></div>
</section>