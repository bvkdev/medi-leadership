<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
// Get values for page intro
$intro_headline = get_sub_field('headline');
$intro_text = get_sub_field('intro');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_intro <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_intro cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_intro cf">
			<?php if($intro_headline): ?> <h2 class="b-intro__headline"><?php echo $intro_headline; ?></h2><?php endif; ?>
			<?php if($intro_text): ?> <div class="b-intro__text b-wysiwyg"><?php echo $intro_text; ?></div><?php endif; ?>
		</div>
	</div>
</section>