<?php
// BACKGROUND COLOR RESET AND GET
function bb_get_background_color() {
	$background_color = "";
	$background_color = get_sub_field('section_background_color');
	//fallback for older labeling
	if($background_color == "") :
		$background_color = get_sub_field('background_color');
	endif;
	if($background_color != ""):
		return $background_color;
	endif;
}
function bb_get_background_image() {
	$background_image = "";
	$background_image = get_sub_field('background_image');
	if( !is_array($background_image) ) {
		//fallback for older labeling
		if($background_image == ""):
			$background_image = get_sub_field('background_photo');
		endif;
		if($background_image != ""):
			return $background_image;
		endif;
	} else {
		if($background_image != ""):
			return $background_image['url'];
		endif;
	}
}
function bb_get_background_image_obj() {
	$background_image = get_sub_field('background_image');
	if($background_image != ""):
		return $background_image;
	endif;
}

function set_background_style() {
	$background_style = "";
	$background_color = bb_get_background_color();
	
	$background_image = bb_get_background_image();
	if($background_image != "") { //has bkg image
		$background_image_vertical = get_sub_field('background_image_vertical_position');
		if(!$background_image_vertical) {
			$background_image_vertical = get_sub_field('background_photo_vertical_position');
		}
		$background_image_horizontal = get_sub_field('background_image_horizontal_position');
		if(!$background_image_horizontal) {
			$background_image_vertical = get_sub_field('background_photo_horizontal_position');
		}
		$background_display_type = get_sub_field('background_image_display_type');
		if( !$background_display_type ){
			$background_display_type = 'cover';
		}
		//set different background display options
		if( $background_display_type == 'cover' ){
			$background_style = "style='background:".$background_color."  url(".$background_image.") ".$background_image_horizontal." ".$background_image_vertical." no-repeat; background-size: cover;'";
		} elseif( $background_display_type == 'repeat' ) {
			$background_style = "style='background:".$background_color."  url(".$background_image.") ".$background_image_horizontal." ".$background_image_vertical."; background-repeat: repeat;'";
		} elseif( $background_display_type == 'repeat_horizontal' ) {
			$background_style = "style='background:".$background_color."  url(".$background_image.") ".$background_image_horizontal." ".$background_image_vertical."; background-repeat: repeat-x;'";
		} elseif( $background_display_type == 'repeat_vertical' ) {
			$background_style = "style='background:".$background_color."  url(".$background_image.") ".$background_image_horizontal." ".$background_image_vertical."; background-repeat: repeat-y;'";
		}
	
	
	} else { //no bkg image, so use color
		$background_style = "style='background-color:".$background_color.";'";
	};
	return $background_style;
}
//RO Background Color
function ro_get_background_color($ro_prefix="") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
	
	$background_color = "";
	$background_color = $acf_getter($ro_prefix.'section_background_color');
	//fallback for older labeling
	if($background_color == "") :
		$background_color = "#fff";
	endif;
	if($background_color != ""):
		return $background_color;
	endif;
}
// RO Background Style
function ro_set_background_style($ro_prefix="") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
	
	$background_style = "";
	$background_color = ro_get_background_color($ro_prefix);
	$background_image = $acf_getter($ro_prefix.'background_image');
	if($background_image != "") { //has bkg image
		
		$background_image_url = $background_image['url'];
		
		$background_image_vertical = $acf_getter($ro_prefix.'background_image_vertical_position');
		if(!$background_image_vertical) {
			$background_image_vertical = $acf_getter($ro_prefix.'background_photo_vertical_position');
		}
		$background_image_horizontal = $acf_getter($ro_prefix.'background_image_horizontal_position');
		if(!$background_image_horizontal) {
			$background_image_vertical = $acf_getter($ro_prefix.'background_photo_horizontal_position');
		}

		$background_style = "style='background:".$background_color."  url(".$background_image_url.") ".$background_image_horizontal." ".$background_image_vertical." no-repeat; background-size: cover;'";
	} else { //no bkg image, so use color
		$background_style = "style='background-color:".$background_color.";'";
	};
	return $background_style;
}

// CONTENT BACKGROUND COLOR
function bb_get_content_bkg_color() {
	$content_bkg_color = "";
	$content_bkg_color = get_sub_field('content_background_color');
	if($content_bkg_color != ""):
		return $content_bkg_color;
	endif;
}
function bb_set_content_bkg_style() {
	$content_bkg_style = "";
	$content_bkg_color = bb_get_content_bkg_color();
	if( $content_bkg_color != "" ) {
		$content_bkg_style = "style='background-color:".$content_bkg_color.";'";
	}
	return $content_bkg_style;
}

// SHOULD PADDING BE COLLAPSED?
function ro_collapse_padding($ro_prefix = "") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
	$collapse_class = '';
	$collapse_option = $acf_getter($ro_prefix.'collapse_padding');
	switch( $collapse_option ) :
		case "none":
			$collapse_class = '';
			break;
		case "top":
			$collapse_class = 'b-section_collapsed-top';
			break;
		case "bottom":
			$collapse_class = 'b-section_collapsed-bottom';
			break;
		case "both":
			$collapse_class = 'b-section_collapsed-both';
			break;
		case "negtop":
			$collapse_class = 'b-section_collapsed-negtop';
	endswitch;
	return $collapse_class;
}
// SHOULD THE TEXT BE INVERSED?
function ro_inverse_text($ro_prefix = "") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
		
	$inverse_class = "";
	if( $inverse_text_option = $acf_getter($ro_prefix.'inverse_text' ) ):
		$inverse_class = 'inverse_text';
	endif;
	return $inverse_class;
}

// USE AN ID FOR THE SECTION
function ro_add_section_id($ro_prefix = "") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
	$id_tag = "";
	$id_tag_option = $acf_getter($ro_prefix.'custom_id');
	if( $id_tag_option != "") :
		$id_tag = $id_tag_option;
		//return $anchor_tag;
		return "id=".$id_tag;
	endif;
}
// ADD CLASSES TO THE SECTION
function ro_add_section_classes($ro_prefix = "") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
	$custom_class_list = "";
	$custom_class_list_values = $acf_getter($ro_prefix.'custom_classes');
	if( $custom_class_list_values != "") :
		$custom_class_list = $custom_class_list_values;
		return $custom_class_list;
	endif;
}
// GET BUTTON LINK OR CHECK FOR MANUAL ALTERNATE
function ro_get_button_label($ro_prefix = ""){
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}
	// reset variables
	$button_text = false;
	$button_text = $acf_getter($ro_prefix.'button_text');
	return $button_text;
}
function ro_button_link($ro_prefix = "") {
	if($ro_prefix == "") {
		$acf_getter = 'get_sub_field';
	} else {
		$acf_getter = 'get_field';
	}

	// reset variables
	$button_link = false;
	// get current options, with allowances for legacy input names
	if ( $acf_getter($ro_prefix.'button_custom_url') ) {
		$button_link = $acf_getter($ro_prefix.'button_custom_url');
	} elseif ( $acf_getter($ro_prefix.'feature_custom_link') ) {
		$button_link = $acf_getter($ro_prefix.'feature_custom_link');
	} elseif ( $acf_getter($ro_prefix.'card_button_link') ) {
		$button_link = $acf_getter($ro_prefix.'card_button_link');
	} elseif ( $acf_getter($ro_prefix.'feature_link') ) {
		$button_link = $acf_getter($ro_prefix.'feature_link');
	} elseif ( $acf_getter($ro_prefix.'button_link') ) {
		$button_link = $acf_getter($ro_prefix.'button_link');
	}
	return $button_link;
}
function button_position() {
	// reset variables
	$button_position = false;
	if ( get_sub_field('button_position') ) :
		$button_position = get_sub_field('button_position');
	endif;
	return $button_position;
}
//
//
	function ro_add_zero_to_display_numbers($counter = 0) {
		if($counter < 10){
			return "0".$counter;
		} else {
			return $counter;
		}
	}
?>