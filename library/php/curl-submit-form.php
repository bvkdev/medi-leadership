<?php
namespace bvk\http;

header('Content-Type: application/json');
error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__ . '/CurlRequest.php';
require __DIR__ . '/Messenger.php';
require __DIR__ . '/helpers.php';

$response = new Messenger();
$data = [];
$url = null;
$http_origin = $_SERVER['HTTP_ORIGIN'];
$local_origins = ['https://unitedwaynca.org', 'https://www.unitedwaynca.org'];
$external_origins = ['https://donate.unitedwaynca.org', 'https://volunteer.unitedwaynca.org'];
$is_external_origin = in_array($http_origin, $external_origins);
$is_safe_origin = (in_array($http_origin, $external_origins) || $is_external_origin);

if ( isset($_POST) && !empty($_POST) )
{
	if ( $is_safe_origin )
	{
		header("Access-Control-Allow-Origin: $http_origin");
	}

	if ( (!isset($_POST['token']) || $_POST['token'] !== get_csrf_token()) && !$is_external_origin ) 
	{
		$response->error("Token failed validation")->emit();
		return false; 
	}

	foreach ( ['email', 'firstname', 'lastname'] as $val )
	{
		if ( !isset($_POST[$val]) || empty($_POST[$val]) )
		{
			$response->error("Required field not found.")->emit();
			return false;
		}
		if ( $val === 'email' )
		{
			if ( !filter_var($_POST[$val], FILTER_VALIDATE_EMAIL) )
			{
				$response->error("Email failed validation.")->emit();
				return false;
			}
		}
		$data[$val] = $_POST[$val];
	}

	if ( isset($_POST['phone']) && !empty($_POST['phone']) )
	{
		$data['phone'] = $_POST['phone'];
	}

	if ( !isset($_POST['url']) || empty($_POST['url']) )
	{
		$response->error("URL not provided.")->emit();
		return false;
	}
	$url = $_POST['url'];

	$cr = (new CurlRequest())->post($url, $data);
	
	switch ( strtolower($cr->status) )
	{
		case 'success':
			$response->success("Success")->emit();
			break;

		case 'fail':
			if ( strtolower($cr->code) === 'validation' )
			{
				$fields = array_pop($cr->field_errors);
				$response->error($fields->field . ': ' . $fields->message)->emit();
			}
			else
			{
				$response->error($cr->code)->emit();
			}
			break;
	}
}
else
{
	$response->error("POST not set.")->emit();
	return false;
}