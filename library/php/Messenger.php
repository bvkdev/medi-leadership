<?php
namespace bvk\http;

class Messenger
{
	public function __construct()
	{
		$this->data = new \stdClass();

		foreach ( ['error', 'msg', 'success'] as $val )
		{
			$this->data->{$val} = null;
		}	
		return $this;
	}

	public function error($message)
	{
		$this->message($message);
		$this->data->error = true;
		return $this;
	}

	public function success($message)
	{
		$this->message($message);
		$this->data->success = true;
		return $this;
	}

	public function emit($type = 'json')
	{
		echo json_encode($this->data);
	}

	private function message($message) 
	{
		$this->data->msg = $message;
	}
}