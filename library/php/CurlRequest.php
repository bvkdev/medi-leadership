<?php
namespace bvk\http;

class CurlRequest 
{
    protected $data; 
    protected $user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:52.0) Gecko/20100101 Firefox/52.0';
    private $options; 

    public function __construct()
    {
        $this->set_options();   
        return $this;
    }

    /**
     * Perform an HTTP GET request.
     * @param  String $url
     * @return void
     */
	public function get(String $url) // : void
	{
        // $start = time();
        $this->options[CURLOPT_CUSTOMREQUEST] = 'GET';
        $this->options[CURLOPT_POST]          = false;

        $this->data = $this->transaction($url);
        // $end = time();
        // dump('timer: ' . ($end - $start));
        // dump('endpoint: ' . $url);
        return json_decode($this->data); 
	}

    /**
     * Perform an HTTP POST request.
     * @param  String $url
     * @param  Array  $data
     * @return void
     */
    public function post(String $url, Array $data = array()) // : void
    {
        $this->options[CURLOPT_CUSTOMREQUEST] = 'POST';
        $this->options[CURLOPT_POST]          = true;
        $this->options[CURLOPT_POSTFIELDS]    = $data;

        $this->data = $this->transaction($url);
        return json_decode($this->data); 
    }

    /**
     * Perform an HTTP DELETE request.
     * @param  String $url
     * @param  Array  $data
     * @return void
     */
    public function delete(String $url, Array $data = array()) // : void
    {
        $this->options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        // $this->options[CURLOPT_POST]          = false;
        $this->options[CURLOPT_POSTFIELDS]    = $data;

        $this->data = $this->transaction($url);
        return json_decode($this->data); 
    }

    public function add_option($name, $value)
    {
        $this->options[$name] = $value;
        return $this;
    }

    /**
     * Handle the cURL request.
     * @param  String $url
     * @param  Array  $options
     * @return String
     */
    private function transaction(String $url) : String
    {
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $this->options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;

        return $header['content'];
    }

    /**
     * Setup basic, shared options amongst all request types.
     * @return Void
     */
    private function set_options() // : Void
    {
        $this->options = [
            CURLOPT_USERAGENT      => $this->user_agent,    //set user agent
            CURLOPT_COOKIEFILE     => "cookie.txt",         //set cookie file
            CURLOPT_COOKIEJAR      => "cookie.txt",         //set cookie jar
            CURLOPT_RETURNTRANSFER => true,                 // return web page
            CURLOPT_HEADER         => false,                // don't return headers
            CURLOPT_FOLLOWLOCATION => true,                 // follow redirects
            CURLOPT_ENCODING       => "",                   // handle all encodings
            CURLOPT_AUTOREFERER    => true,                 // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 30,                   // timeout on connect
            CURLOPT_TIMEOUT        => 30,                   // timeout on response
            CURLOPT_MAXREDIRS      => 10,                   // stop after 10 redirects
        ];
    }
}