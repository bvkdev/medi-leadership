<?php

/**
 * Generate a CSRF token to help prevent forms or server requests 
 * from being hijacked by eyepatch wearing internet pirates.
 *
 * TO USE: 
 * Add hidden field to form or request that contains the returned 
 * value. On the receiving script, check the provided token 
 * against this function's return value to validate.
 *
 * i.e., if ( $_POST['token'] !== get_csrf_token() ) { return false; }
 * 
 * @return String
 */
function get_csrf_token()
{
	if ( !session_id() ) { session_start(); }
	return hash("sha512", 'peradventurethebedlamwashebetude' . session_id());
}