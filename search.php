<?php get_header(); ?>

			<div id="content">
				<div class="b-search-title">
					<h1 class="archive-title b-headline b-headline_search-results"><?php _e( 'Search Results for: ', 'mediLeadershipTheme' ); ?> <span><?php echo esc_attr(get_search_query()); ?></span></h1>
				</div>
				
				<div id="inner-content" class="wrap b-section__wrap-outer b-columns cf">

					<main id="main" class="cf" role="main">

						<?php if (have_posts()) :
								// CREATE LIST OF POST TYPES THAT HAVE COME BACK
								$result_types = array();
								while (have_posts()) : the_post();
									$type = get_post_type();
									if( ! in_array($type, $result_types ) ):
										$result_types[] = $type;
									endif;
								endwhile;
								//print_r($result_types);
								
								//RUN THE LOOP FOR EACH POST_TYPE SO THAT THEY CAN BE GROUPED
								foreach( $result_types as $type ): ?>
								<?php
								$section_title = str_replace("type_", "", $type);
								$section_title = ucwords($section_title);
								if( $section_title == "post" ) {$section_title = "Blog Post"; }
								?>
								<h2 class="b-headline b-search-results_group-name"><?php echo $section_title."s"; ?></h2>
								<div class="b-search-results_group b-columns">
									
								<?php
									// RUN THE LOOP ON EVERYTHING AGAIN.
									while (have_posts()) : the_post();
										// CHECK IF THE CURRENT POST IN THE LOOP MATCHES THE POST_TYPE SECTION WE'RE BUILDING
										if( $type == get_post_type() ) {
											
											if( get_post_type() == 'type_employee' ) { 
												// THE TEMPLATE FOR EMPLOYEES ?>
												<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf b-regular-post__item b-regular-post-item js-sortable-object ' ); ?> role="article">

													<p class="h3 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
													<?php $excerpt_text = get_the_excerpt(); ?>
													<a class="b-result__link" href="<?php the_permalink(); ?>">View Profile</a>
												</article>
											<?php
											 } elseif ( get_post_type() == 'type_event'  ) {
												 // THE TEMPLATE FOR EVENTS
												get_template_part('library/custom-loops/b-single__event');
												
											} elseif ( get_post_type() == 'page' ) {
												// THE TEMPLATE FOR PAGES
											?>
											<?php
											$excerpt_text = get_the_content();
											if( $excerpt_text == "") {
												$excerpt_text = get_the_excerpt();
											}
											if( $excerpt_text == "") {
												$excerpt_text = get_field('introduction_text_intro');
											}
											if( $excerpt_text == "") {
												$excerpt_text = get_field('goal_subheadline');
											}
											?>
												<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf b-regular-post__item b-regular-post-item js-sortable-object ' ); ?> role="article">
													<?php /*<div class="b-resp-image">
														<?php echo get_the_post_thumbnail( get_the_ID(), 'large', array( 'class' => 'b-regular-post-item__image' ) ); ?>
													</div>*/ ?>
													<p class="h3 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
													<p><?php echo shorten_content($excerpt_text, 20); ?></p>
													<a class="b-result__link" href="<?php the_permalink(); ?>">View Page</a>
												</article>
											<?php } elseif ( get_post_type() == 'post' ) {
												// THE TEMPLATE FOR POSTS
												?>
												<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf b-regular-post__item b-regular-post-item js-sortable-object ' ); ?> role="article">
													<h1 class="h3 entry-title b-regular-post-item__title"><a href="<?php get_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
													<a class="b-result__link"  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read Story</a>
													
												</article>
											<?php } // END CHECK FOR CURRENT POST'S POST_TYPE ?>
											
										<?php } // END CHECK FOR CURRENT LOOP'S POST_TYPE ?>
									
										<?php endwhile; //END WHILE HAVE POSTS ?>
										<?php
										// REWIND THE WP LOOP SO THAT WE CAN GO THROUGH IT AGAIN WITH THE NEXT POST_TYPE
										rewind_posts();
										?>
									</div> <?php // END EACH GROUP'S OUTER WRAPPING ?>
									
								<?php endforeach; //END LOOPING THROUGH EACH POST_TYPE?>
								
								

							<?php else : ?>
							<?php // THERE ARE NO RESULTS OF ANY KIND ?>
									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Sorry, No Results.', 'mediLeadershipTheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Try your search again.', 'mediLeadershipTheme' ); ?></p>
											<?php get_search_form() ?>
										</section>
										
									</article>

							<?php endif; ?>

						</main>


					</div>

			</div>

<?php get_footer(); ?>
